function showMultiModal(no, type, token, id, url){
	//alert("show");
	//alert(no);
	//alert(type);
	//alert(id);
	//alert(url);
	var holder = "#multiModal"+ no;
	if(!$(holder).length){
		$("body").append('<div id="multiModal'+ no +'" class="modal fade" role="dialog" aria-labelledby="multiModalLabel'+ no +'" aria-hidden="true" style="z-index:'+ (1050 + Number(no)) +'"><div class="modal-dialog modal-sm"><div class="modal-content data"></div></div></div>');
	}
	
	$(holder).toggle();
	$(holder).modal({backdrop:"static", keyboard:false});
	$(holder +" .data").html('<div style="text-align:center;padding:15px 0"><i class="ace-icon fa fa-spinner fa-spin blue" style="font-size:30px"></i></div>');
	$.ajax({
		type: "POST",
		//url: url +"/modal",
		url: url,
		data: "id="+ id +"&type="+ type +"&_token="+ token,
		success: function(response){
			$(holder +" .data").html(response);
			$("form").submit(function(e){
				e.preventDefault();
			});
			centerMultiModal(no);
		},
		error: function(xhr, ajaxOptions, thrownError){
			swal("Modal", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
		}
	});
	
	$(".modal-backdrop.show").last().css('z-index', 1049 + Number(no));
}

function showMultiModal2id(no, type, token, id, id2, url){
	//alert("show");
	//alert(no);
	//alert(type);
	//alert(id);
	//alert(url);
	var holder = "#multiModal"+ no;
	if(!$(holder).length){
		$("body").append('<div id="multiModal'+ no +'" class="modal fade" role="dialog" aria-labelledby="multiModalLabel'+ no +'" aria-hidden="true" style="z-index:'+ (1050 + Number(no)) +'"><div class="modal-dialog modal-sm"><div class="modal-content data"></div></div></div>');
	}
	
	$(holder).toggle();
	$(holder).modal({backdrop:"static", keyboard:false});
	$(holder +" .data").html('<div style="text-align:center;padding:15px 0"><i class="ace-icon fa fa-spinner fa-spin blue" style="font-size:30px"></i></div>');
	$.ajax({
		type: "POST",
		//url: url +"/modal",
		url: url,
		data: "id="+ id +"&id2="+ id2 +"&type="+ type +"&_token="+ token,
		success: function(response){
			$(holder +" .data").html(response);
			$("form").submit(function(e){
				e.preventDefault();
			});
			centerMultiModal(no);
		},
		error: function(xhr, ajaxOptions, thrownError){
			swal("Modal", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
		}
	});
	
	$(".modal-backdrop.show").last().css('z-index', 1049 + Number(no));
}

function showMultiModal3id(no, type, id, id2, id3, url){
	var holder = "#multiModal"+ no;
	if(!$(holder).length){
		$("body").append('<div id="multiModal'+ no +'" class="modal fade" role="dialog" aria-labelledby="multiModalLabel'+ no +'" aria-hidden="true" style="z-index:'+ (1050 + Number(no)) +'"><div class="modal-dialog modal-sm"><div class="modal-content data"></div></div></div>');
	}
	
	$(holder).toggle();
	$(holder).modal({backdrop:"static", keyboard:false});
	$(holder +" .data").html('<div style="text-align:center;padding:15px 0"><i class="ace-icon fa fa-spinner fa-spin blue" style="font-size:30px"></i></div>');
	$.ajax({
		type: "POST",
		//url: url +"/modal",
		url: url,
		data: "id="+ id +"&id2="+ id2 +"&id3="+ id3 +"&type="+ type,
		success: function(response){
			$(holder +" .data").html(response);
			$("form").submit(function(e){
				e.preventDefault();
			});
			centerMultiModal(no);
		},
		error: function(xhr, ajaxOptions, thrownError){
			swal("Modal", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
		}
	});
	
	$(".modal-backdrop.show").last().css('z-index', 1049 + Number(no));
}

function sizeMultiModal(no, type){
	//alert("size");
	$("#multiModal"+ no +" .modal-dialog").removeClass("modal-full modal-lg modal-sm");
	if(type != "default"){
		$("#multiModal"+ no +" .modal-dialog").addClass("modal-"+ type);
	}
}

function centerMultiModal(no){
	//alert("center");
	var holder = "#multiModal"+ no;
	var winH = $(window).height();
	var hdrH = ($(holder +" .modal-body").height() + 210);				
	if(hdrH < winH){
		$(holder +" .modal-dialog").css({"margin-top":((winH - hdrH) / 2) +"px", "margin-bottom":10});
	}
	else $(holder +" .modal-dialog").css({"margin-top":10, "margin-bottom":10});
}

function closeMultiModal(no){
	//alert("close");
	$("#multiModal"+ no).remove();
	$(".modal-backdrop.show").last().remove();
}

function showPage(page, tab){
	populate(page, tab);
}
function popPage(page, tab){
	popSearch(page, tab);
}
function doSearch(tab){
	populate("", tab);
}
function showOrder(page, tab, field, order){
	populateOrder(page, tab, field, order);
}
function showDetail(page, tab, detail, menu){
	populateDetail(page, tab, detail, menu);
}

function populate(page, tab){
	if(page) page = "/"+ page;
	if(tab === undefined) tab = "";
	var form = $("#form-search"+ tab);
	$.ajax({
		type: "POST",
		data: form.serialize(),
		url: form.attr("action") + page,
		cache: false,
		success: function(data){
			$("#panel-data"+ tab).html(data);
		},
		beforeSend: function(response){
			loader("#panel-data"+ tab);
		},
		error: function(response){
			$("#panel-data"+ tab).html('<div class="alert alert-danger alert-dismissable text-center"><strong>Error!</strong> '+ response.status +' - '+ response.statusText +'</div>');
		}
	});
}

// custom double populate
function doSearch2(tab, type) {
	populate2("", tab, type);
}

function populate2(page, tab, type) {
	if (page) page = "/" + page;
	if (tab === undefined) tab = "";
	var form = $("#form-search" + tab);
	$.ajax({
		type: "POST",
		// data: "id=" + id + "&type=" + type,
		data: form.serialize()+"&type="+type,
		url: form.attr("action") + page,
		cache: false,
		success: function (data) {
			$("#panel-data" + tab).html(data);
		},
		beforeSend: function (response) {
			loader("#panel-data" + tab);
		},
		error: function (response) {
			$("#panel-data" + tab).html('<div class="alert alert-danger alert-dismissable text-center"><strong>Error!</strong> ' + response.status + ' - ' + response.statusText + '</div>');
		}
	});
}
// End custom double populate

// cutom populate order
function populateOrder(page, tab, field, order) {
	if (page) page = "/" + page;
	if (tab === undefined) tab = "";
	var form = $("#form-search" + tab);
	$.ajax({
		type: "POST",
		// data: "id=" + id + "&type=" + type,
		data: form.serialize()+"&field="+field+"&order="+order,
		url: form.attr("action") + page,
		cache: false,
		success: function (data) {
			$("#panel-data" + tab).html(data);
		},
		beforeSend: function (response) {
			loader("#panel-data" + tab);
		},
		error: function (response) {
			$("#panel-data" + tab).html('<div class="alert alert-danger alert-dismissable text-center"><strong>Error!</strong> ' + response.status + ' - ' + response.statusText + '</div>');
		}
	});
}
function populateDetail(page, tab, detail, menu) {
	if (page) page = "/" + page;
	if (tab === undefined) tab = "";
	var form = $("#form-search" + tab);
	$.ajax({
		type: "POST",
		// data: "id=" + id + "&type=" + type,
		data: form.serialize()+"&detail="+detail+"&menu="+menu,
		url: form.attr("action") + page,
		cache: false,
		success: function (data) {
			$("#panel-data" + tab).html(data);
		},
		beforeSend: function (response) {
			loader("#panel-data" + tab);
		},
		error: function (response) {
			$("#panel-data" + tab).html('<div class="alert alert-danger alert-dismissable text-center"><strong>Error!</strong> ' + response.status + ' - ' + response.statusText + '</div>');
		}
	});
}
// End custom populate order

function popSearch(page, id){
	if(page) page = "/"+ page;
	var form = $("#form-"+ id);
	$.ajax({
		type: "POST",
		data: form.serialize(),
		url: form.attr("action") + page,
		cache: false,
		success: function(response){
			$("#result-"+ id).html(response);
		},
		beforeSend: function(response){
			if(!$("#result-"+ id).length){
				$('<div id="result-'+ id +'"></div>').insertBefore("#form-"+ id);
			}
			loader("#result-"+ id);
		},
		error: function(xhr, ajaxOptions, thrownError){
			swal("Search", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
		}
	});
}

function reloadPopulate(tab){
	if(tab === undefined) tab = "";
	var currentPage = $("#populated"+ tab).attr("current-page");
	console.log($("#populated"+ tab));
	if(currentPage === undefined){
		doSearch(tab);
	}
	else showPage(currentPage, tab);
}

function doReset(tab){
	if(tab === undefined) tab = "";
	$("#form-search"+ tab)[0].reset();
	$('#form-search'+ tab +' input[type="hidden"]').val('');
	setTimeout(function(){doSearch(tab)}, 500);
}

function doSubmit(id){
	//$("#form-"+ id).validate({ 
		//submitHandler: function(form) {
			swal({
				title: "Simpan Data?",
				text: "Apakah Anda yakin ingin menyimpan perubahan data?.",
				type: "warning",
				cancelButtonText: "Tidak",
				confirmButtonText: "Ya",
				showCancelButton: true
			}).then(function(result){
				if(result.value){
					setTimeout(function(){
						var form = $("#form-"+ id);
						$.ajax({
							type: "POST",
							data: new FormData(form[0]),
							url: form.attr("action"),
							async: false,
							cache: false,
							contentType: false,
							processData: false,
							success: function(response){
								swal.close();
								setTimeout(function(){
									if(!$("#result-"+ id).length){
										$('<div id="result-'+ id +'"></div>').insertBefore("#form-"+ id);
									}
									$("#result-"+ id).html(response);
								}, 250);
							},
							beforeSend: function(){
								swalLoader();
							},
							error: function(xhr, ajaxOptions, thrownError){
								swal("Form", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
							}
						});
					}, 250);
				}
			});
		//}
	//});
	
	/*$("#form-"+ id).validate();
	swal({
		title: "Are you sure?",
		text: "You will submit this form.",
		type: "warning",
		cancelButtonText: "Cancel",
		confirmButtonText: "Submit",
		showCancelButton: true
	}).then(function(result){
		if(result.value){
			setTimeout(function(){
				var form = $("#form-"+ id);
				$.ajax({
					type: "POST",
					data: new FormData(form[0]),
					url: form.attr("action"),
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					success: function(response){
						swal.close();
						setTimeout(function(){
							if(!$("#result-"+ id).length){
								$('<div id="result-'+ id +'"></div>').insertBefore("#form-"+ id);
							}
							$("#result-"+ id).html(response);
						}, 250);
					},
					beforeSend: function(){
						swalLoader();
					},
					error: function(xhr, ajaxOptions, thrownError){
						swal("Form", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
					}
				});
			}, 250);
		}
	});*/
}
// function doDelete(id, type, token, url){
// 	swal({
// 		title: "Are you sure?",
// 		text: "You will delete this data.",
// 		type: "warning",
// 		cancelButtonText: "Cancel",
// 		confirmButtonText: "Submit",
// 		showCancelButton: true
// 	}).then(function(result){
// 		if(result.value){
// 			setTimeout(function(){
// 				$.ajax({
// 					type: "POST",
// 					url: url,
// 		      data: "id="+ id +"&type="+ type +"&_token="+ token,
// 					cache: false,
// 					success: function(response){
// 						if(response){
// 							eval(response);
// 						}
// 					},
// 					error: function(xhr, ajaxOptions, thrownError){
// 						swal("Delete", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
// 					}
// 				});
// 			}, 250);
// 		}
// 	});
// }
function doDelete(id, token, url){
  swal({
      title: "Hapus Data?",
      text: "Apakah Anda yakin ingin menghapus data ini?.",
      type: "warning",
      cancelButtonText: "Tidak",
      confirmButtonText: "Ya",
      showCancelButton: true
  }).then(function(result){
      if(result.value){
          $.ajax({
              type: "POST",
              url: url,
              data: {
                  id: id,
                  type: "delete",
                  _token: token
              },
              cache: false,
              success: function(response){
                  handleDeleteResponse(response);
              },
              error: function(xhr, ajaxOptions, thrownError){
                  swal("Delete", ajaxOptions.toUpperCase() + ': ' + thrownError, "error");
              }
          });
      }
  });
}

function handleDeleteResponse(response) {
  if (response.success) {
      swal("Success!", "Data berhasil dihapus", "success").then(function() {
          // Reload or update the content as needed
          doSearch("Populate");
      });
  } else {
      swal("Failed!", "Data gagal dihapus", "error");
  }
}


function doAction(type, id, url){
	//alert(type);
	//alert(id);
	//alert(url);
	swal({
		title: "Are you sure?",
		text: "You will "+ type +" this data.",
		type: "warning",
		cancelButtonText: "Cancel",
		confirmButtonText: "Submit",
		showCancelButton: true
	}).then(function(result){
		if(result.value){
			setTimeout(function(){
				$.ajax({
					type: "POST",
					data: "id="+ id,
					url: url,
					cache: false,
					success: function(response){
						swal.close();
						if(response){
							eval(response);
						}
					},
					beforeSend: function(){
						swalLoader();
					},
					error: function(xhr, ajaxOptions, thrownError){
						swal("Action", ajaxOptions.toUpperCase() +': '+ thrownError, "error");
					}
				});
			}, 250);
		}
	});
}

function loader(holder){
	$(holder).html('<div class="alert alert-info text-center"><i class="fa fa-spin fa-spinner"></i> Please wait &hellip;</div>');
}

function swalLoader(){
	swal({
		html: '<i class="fa fa-spin fa-spinner mloader"></i>',
		width: 1,
		padding: 1,
		background: 'none',
		allowEscapeKey: false,
		allowOutsideClick: false,
		showConfirmButton: false
	});
}

function kirimDataDownload(data1, data2, url) {
	// alert(url);
	// alert(data1);
	// alert(data2);
	$.ajax({
		url: url,
		type: 'POST',
		data: {
			'data1': data1,
			'data2': data2
		},
		success: function (data) {
			// console.log(data);
		},
		error: function (msg) {
			console.log(msg);
		}
	});
}
