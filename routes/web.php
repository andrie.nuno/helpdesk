<?php

use App\Http\Controllers\ForgotPasswordController;
use Illuminate\Support\Facades\Route;

// Auth::routes();

Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');
Route::get('/2fa', 'AuthController@authform')->name('2fa');
Route::post('/2fa', 'AuthController@authenticated')->name('2fa.verify');

// 2fa
// Route::get('protected-route', 'ProtectedController@index')->middleware('2fa');
Route::get('setup-2fa', 'TwoFactorAuthController@showSetupForm')->name('setup-2fa');
Route::post('enable-2fa', 'TwoFactorAuthController@enableTwoFactorAuth')->name('enable-2fa');

// Show the password reset request form
Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');

// Handle the password reset request
Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

// Password reset routes
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

//change password
Route::get('/update-password', 'AuthController@showUpdatePasswordForm')->name('update.password.form');
Route::post('/update-password', 'AuthController@updatePassword')->name('update.password');

Route::get('cronmutasiinternalunit', 'CronMutasiInternalUnit@calculate');
Route::get('cronpencairangadai', 'CronPencairanGadai@calculate');
Route::get('cronpencairangadaijf', 'CronPencairanGadaiJf@calculate');

Route::group(['middleware' => ['auth', '2fa']], function () {

    Route::get('home', 'HomeController@index')->name('home');

    Route::get('picexcel',[
        'uses' => 'HomeController@excelpic',
        'as' => 'picexcel'
    ]);

    Route::get('logout', 'AuthController@logout')->name('logout');

    // ==== Start Dashboard ====
    Route::get('/dashboard',[
        'uses' => 'login\Dashboard@index',
        'as' => 'login.dashboard'
    ]);
    Route::post('/dashboardpopulate',[
        'uses' => 'login\Dashboard@populate',
        'as' => 'login.dashboardpopulate'
    ]);
    Route::post('/dashboardpopulatemodal',[
        'uses' => 'login\Dashboard@modal',
        'as' => 'login.dashboardmodal'
    ]);
    Route::post('/dashboardpopulateajax',[
        'uses' => 'login\Dashboard@ajax',
        'as' => 'login.dashboardajax'
    ]);
    Route::post('/dashboardpopulatepost',[
        'uses' => 'login\Dashboard@proses',
        'as' => 'login.dashboardpost'
    ]);

    // ==== Start Master User ====
    Route::get('/masteruser',[
        'uses' => 'login\MasterUser@index',
        'as' => 'login.masteruser'
    ]);
    // Route::get('/masteruserpopulate',[
    //     'uses' => 'login\MasterUser@populate',
    //     'as' => 'login.masteruserpopulate'
    // ]);
    Route::post('/masteruserpopulate',[
        'uses' => 'login\MasterUser@populate',
        'as' => 'login.masteruserpopulate'
    ]);
    Route::post('/masteruserpopulatemodal',[
        'uses' => 'login\MasterUser@modal',
        'as' => 'login.masterusermodal'
    ]);
    Route::post('/masteruserpopulateajax',[
        'uses' => 'login\MasterUser@ajax',
        'as' => 'login.masteruserajax'
    ]);
    Route::post('/masteruserpopulatepost',[
        'uses' => 'login\MasterUser@proses',
        'as' => 'login.masteruserpost'
    ]);

    // ==== Start Master User ====
    Route::get('/masteruser',[
        'uses' => 'login\MasterUser@index',
        'as' => 'login.masteruser'
    ]);
    Route::post('/masteruserpopulate',[
        'uses' => 'login\MasterUser@populate',
        'as' => 'login.masteruserpopulate'
    ]);
    Route::post('/masteruserpopulatemodal',[
        'uses' => 'login\MasterUser@modal',
        'as' => 'login.masterusermodal'
    ]);
    Route::post('/masteruserpopulateajax',[
        'uses' => 'login\MasterUser@ajax',
        'as' => 'login.masteruserajax'
    ]);
    Route::post('/masteruserpopulatepost',[
        'uses' => 'login\MasterUser@proses',
        'as' => 'login.masteruserpost'
    ]);

    // ==== Start Master Kategori ====
    Route::get('/masterkategori',[
        'uses' => 'login\MasterKategori@index',
        'as' => 'login.masterkategori'
    ]);
    Route::post('/masterkategoripopulate',[
        'uses' => 'login\MasterKategori@populate',
        'as' => 'login.masterkategoripopulate'
    ]);
    Route::post('/masterkategoripopulatemodal',[
        'uses' => 'login\MasterKategori@modal',
        'as' => 'login.masterkategorimodal'
    ]);
    Route::post('/masterkategoripopulateajax',[
        'uses' => 'login\MasterKategori@ajax',
        'as' => 'login.masterkategoriajax'
    ]);
    Route::post('/masterkategoripopulatepost',[
        'uses' => 'login\MasterKategori@proses',
        'as' => 'login.masterkategoripost'
    ]);

    // ==== Start Master Task ====
    Route::get('/mastertask',[
        'uses' => 'login\MasterTask@index',
        'as' => 'login.mastertask'
    ]);
    Route::post('/mastertaskpopulate',[
        'uses' => 'login\MasterTask@populate',
        'as' => 'login.mastertaskpopulate'
    ]);
    Route::post('/mastertaskpopulatemodal',[
        'uses' => 'login\MasterTask@modal',
        'as' => 'login.mastertaskmodal'
    ]);
    Route::post('/mastertaskpopulateajax',[
        'uses' => 'login\MasterTask@ajax',
        'as' => 'login.mastertaskajax'
    ]);
    Route::post('/mastertaskpopulatepost',[
        'uses' => 'login\MasterTask@proses',
        'as' => 'login.mastertaskpost'
    ]);

    // ==== Start Master Subjek ====
    Route::get('/mastersubjek',[
        'uses' => 'login\MasterSubjek@index',
        'as' => 'login.mastersubjek'
    ]);
    Route::post('/mastersubjekpopulate',[
        'uses' => 'login\MasterSubjek@populate',
        'as' => 'login.mastersubjekpopulate'
    ]);
    Route::post('/mastersubjekpopulatemodal',[
        'uses' => 'login\MasterSubjek@modal',
        'as' => 'login.mastersubjekmodal'
    ]);
    Route::post('/mastersubjekpopulateajax',[
        'uses' => 'login\MasterSubjek@ajax',
        'as' => 'login.mastersubjekajax'
    ]);
    Route::post('/mastersubjekpopulatepost',[
        'uses' => 'login\MasterSubjek@proses',
        'as' => 'login.mastersubjekpost'
    ]);

    // ==== Start Master Sub Subjek ====
    Route::get('/mastersubsubjek',[
        'uses' => 'login\MasterSubSubjek@index',
        'as' => 'login.mastersubsubjek'
    ]);
    Route::post('/mastersubsubjekpopulate',[
        'uses' => 'login\MasterSubSubjek@populate',
        'as' => 'login.mastersubsubjekpopulate'
    ]);
    Route::post('/mastersubsubjekpopulatemodal',[
        'uses' => 'login\MasterSubSubjek@modal',
        'as' => 'login.mastersubsubjekmodal'
    ]);
    Route::post('/mastersubsubjekpopulateajax',[
        'uses' => 'login\MasterSubSubjek@ajax',
        'as' => 'login.mastersubsubjekajax'
    ]);
    Route::post('/mastersubsubjekpopulatepost',[
        'uses' => 'login\MasterSubSubjek@proses',
        'as' => 'login.mastersubsubjekpost'
    ]);

    // ==== Start Task Problem ====
    Route::get('/problem',[
        'uses' => 'login\Problem@index',
        'as' => 'login.problem'
    ]);
    Route::post('/problempopulate',[
        'uses' => 'login\Problem@populate',
        'as' => 'login.problempopulate'
    ]);
    Route::post('/problempopulatemodal',[
        'uses' => 'login\Problem@modal',
        'as' => 'login.problemmodal'
    ]);
    Route::post('/problempopulateajax',[
        'uses' => 'login\Problem@ajax',
        'as' => 'login.problemajax'
    ]);
    Route::post('/problempopulatepost',[
        'uses' => 'login\Problem@proses',
        'as' => 'login.problempost'
    ]);

    Route::get('problemexcel',[
        'uses' => 'login\Problem@excel',
        'as' => 'problemexcel'
    ]);

    Route::get('taskexcel',[
        'uses' => 'login\Problem@taskexcel',
        'as' => 'taskexcel'
    ]);

    // ==== Start Task Incident / Bug ====
    Route::get('/incident',[
        'uses' => 'login\Incident@index',
        'as' => 'login.incident'
    ]);
    Route::post('/incidentpopulate',[
        'uses' => 'login\Incident@populate',
        'as' => 'login.incidentpopulate'
    ]);
    Route::post('/incidentpopulatemodal',[
        'uses' => 'login\Incident@modal',
        'as' => 'login.incidentmodal'
    ]);
    Route::post('/incidentpopulateajax',[
        'uses' => 'login\Incident@ajax',
        'as' => 'login.incidentajax'
    ]);
    Route::post('/incidentpopulatepost',[
        'uses' => 'login\Incident@proses',
        'as' => 'login.incidentpost'
    ]);

    Route::get('incidentexcel',[
        'uses' => 'login\Incident@excel',
        'as' => 'incidentexcel'
    ]);

    // ==== Start Task Pelaporan ====
    Route::get('/pelaporan',[
        'uses' => 'login\Pelaporan@index',
        'as' => 'login.pelaporan'
    ]);
    Route::post('/pelaporanpopulate',[
        'uses' => 'login\Pelaporan@populate',
        'as' => 'login.pelaporanpopulate'
    ]);
    Route::post('/pelaporanpopulatemodal',[
        'uses' => 'login\Pelaporan@modal',
        'as' => 'login.pelaporanmodal'
    ]);
    Route::post('/pelaporanpopulateajax',[
        'uses' => 'login\Pelaporan@ajax',
        'as' => 'login.pelaporanajax'
    ]);
    Route::post('/pelaporanpopulatepost',[
        'uses' => 'login\Pelaporan@proses',
        'as' => 'login.pelaporanpost'
    ]);

    Route::get('pelaporanexcel',[
        'uses' => 'login\Pelaporan@excel',
        'as' => 'pelaporanexcel'
    ]);


    // ==== Start Menu ====
    Route::get('/master-akses/menu',[
        'uses' => 'login\Menu@index',
        'as' => 'login.menu'
    ]);
    Route::post('/master-akses/menupopulate',[
        'uses' => 'login\Menu@populate',
        'as' => 'login.menupopulate'
    ]);
    Route::post('/master-akses/menupopulatemodal',[
        'uses' => 'login\Menu@modal',
        'as' => 'login.menumodal'
    ]);
    Route::post('/master-akses/menupopulatepost',[
        'uses' => 'login\Menu@proses',
        'as' => 'login.menupost'
    ]);

    // ==== Start Modul ====
    Route::get('/master-akses/modul',[
        'uses' => 'login\Modul@index',
        'as' => 'login.modul'
    ]);
    Route::post('/master-akses/modulpopulate',[
        'uses' => 'login\Modul@populate',
        'as' => 'login.modulpopulate'
    ]);
    Route::post('/master-akses/modulpopulatemodal',[
        'uses' => 'login\Modul@modal',
        'as' => 'login.modulmodal'
    ]);
    Route::post('/master-akses/modulpopulatepost',[
        'uses' => 'login\Modul@proses',
        'as' => 'login.modulpost'
    ]);
    Route::post('/master-akses/modulpopulatepostakses',[
        'uses' => 'login\Modul@prosesakses',
        'as' => 'login.modulpostakses'
    ]);

    // ==== Start Mitra User ====
    Route::get('/master-akses/mitrauser',[
        'uses' => 'login\MitraUser@index',
        'as' => 'login.mitrauser'
    ]);
    Route::post('/master-akses/mitrauserpopulate',[
        'uses' => 'login\MitraUser@populate',
        'as' => 'login.mitrauserpopulate'
    ]);
    Route::post('/master-akses/mitrauserpopulatemodal',[
        'uses' => 'login\MitraUser@modal',
        'as' => 'login.mitrausermodal'
    ]);
    Route::post('/master-akses/mitrauserpopulateajax',[
        'uses' => 'login\MitraUser@ajax',
        'as' => 'login.mitrauserajax'
    ]);
    Route::post('/master-akses/mitrauserpopulatepost',[
        'uses' => 'login\MitraUser@proses',
        'as' => 'login.mitrauserpost'
    ]);

});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
