<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CabangController;
use App\Http\Controllers\API\GoogleSheetsApi;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cabang', [CabangController::class, 'index']);
Route::get('/cabang/{id}', [CabangController::class, 'show']);
Route::post('/cabang', [CabangController::class, 'store']);
Route::put('/cabang/{id}', [CabangController::class, 'update']);
Route::delete('/cabang/{id}', [CabangController::class, 'destroy']);

Route::get('/googlesheets', [GoogleSheetsApi::class, 'index']);
