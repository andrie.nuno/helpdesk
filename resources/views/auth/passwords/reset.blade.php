<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Helpdesk | Ubah Password</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="assets/css/custom.css">
    <style>
            /* Custom CSS for the modal */
        .modal-body {
            text-align: center;
        }

        .modal-body img {
            max-width: 100px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body class="hold-transition layout-fixed">
    <header class="header">
        <div class="container">
            <div class="user-panel ml-3 mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <img style="width: 150px" src="{{ asset('assets/MasHelpdesk.png') }}" alt="Logo">
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row justify-content-center mt-6">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header bg-white text-black text-center">
                        <h3 class="card-title">Password Baru</h3>
                        <p class="card-text">Silakan buat password baru Anda untuk menjaga keamanan akun.</p>
                    </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <!-- Email field hidden -->
                        <div class="mb-3 hidden">
                            <input hidden id="emailUser" type="email" class="form-control @error('emailUser') is-invalid @enderror" name="emailUser" value="{{ $emailUser ?? old('emailUser') }}" required autocomplete="emailUser" autofocus>
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label text-bold">{{ __('Password') }}</label>
                            <div class="input-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <button class="btn btn-outline-secondary" type="button" id="togglePassword">
                                    <i class="fas fa-eye"></i>
                                </button>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>

                        </div>

                        <div class="mb-3">
                            <label for="password-confirm" class="form-label text-bold">{{ __('Konfirmasi Password') }}</label>
                            <div class="input-group">
                                <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
                                <button class="btn btn-outline-secondary" type="button" id="toggleConfirmPassword">
                                    <i class="fas fa-eye"></i>
                                </button>
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>


                        <div class="text-center">
                            <button id="submitButton" type="submit" class="btn btn-secondary" disabled>{{ __('Lanjutkan') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content row justify-content-center mt-5">
            <div class="col-md-12">
                <div class="modal-body justify-content-center align-items-center">
                    <div class="row">
                        <div class="col text-center">
                            <img style="width: 400px;" class="img-fluid" src="{{ asset('assets/kunciMas.png') }}" alt="Success Image" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center">
                            <h5 class="card-title" id="successModalLabel">Password Berhasil Dibuat</h5>
                            <p>Silakan masuk kembali dengan password baru Anda.</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center align-items-center">
                    <a href="{{ route('login') }}" class="btn btn-primary btn-block">Cek Email</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<!-- Bootstrap 4 JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<script>
    // Add an event listener for form submission
    document.querySelector('form').addEventListener('submit', function(event) {
        event.preventDefault(); // Prevent the form from submitting normally

        // Get the form data
        var formData = new FormData(event.target);

        // Make an AJAX request to the backend
        fetch(event.target.action, {
            method: 'POST',
            body: formData
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            if (data.status === 'success') {
                $('#successModal').modal('show'); // Show the success modal
            } else {
                // Handle other cases if needed
                event.target.submit(); // Submit the form normally
            }
        })
        .catch(error => {
            console.error('Error:', error);
            event.target.submit(); // Submit the form normally
        });
    });

    // Function to toggle password visibility
    function togglePasswordVisibility(inputId) {
        const passwordInput = document.getElementById(inputId);
        const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInput.setAttribute('type', type);
    }

    // Add event listeners to toggle password visibility when the buttons are clicked
    document.getElementById('togglePassword').addEventListener('click', function() {
        togglePasswordVisibility('password');
    });

    document.getElementById('toggleConfirmPassword').addEventListener('click', function() {
        togglePasswordVisibility('password-confirm');
    });

    document.addEventListener('input', function() {
        const password = document.getElementById('password').value;
        const confirmPassword = document.getElementById('password-confirm').value;
        const submitButton = document.getElementById('submitButton');
        if (password && confirmPassword) {
            submitButton.removeAttribute('disabled');
            submitButton.classList.remove('btn-secondary');
            submitButton.classList.add('btn-primary');
        } else {
            submitButton.setAttribute('disabled', 'disabled');
            submitButton.classList.remove('btn-primary');
            submitButton.classList.add('btn-secondary');
        }
    });
</script>
@include('layouts.footer')
</body>
</html>


