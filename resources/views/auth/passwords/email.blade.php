<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Helpdesk | Forgot Password</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="assets/css/custom.css">
</head>
<body class="hold-transition layout-fixed">
    <header class="header">
        <div class="container">
            <div class="user-panel ml-3 mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <img style="width: 150px" src="{{ asset('assets/MasHelpdesk.png') }}" alt="Logo">
                </div>
            </div>
        </div>
    </header>
<div class="container">
  <div class="row justify-content-center mt-5">
    <div class="col-md-5">
      <div class="card">
        <div class="card-header bg-white text-black text-center">
          <h3 class="card-title">Lupa Password</h3>
          <p>Silakan masukkan email yang terdaftar untuk memperbarui password Anda</p>
        </div>
        <div class="card-body">
          @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
          @endif

          <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group">
              <label for="email" class="col-form-label">Email</label>
              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Masukkan Email" autofocus oninput="toggleSubmitButton()">
              @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="form-group">
              <button id="submitButton" type="submit" class="btn btn-secondary btn-block" disabled>Verifikasi Email</button>
            </div>
          </form>
          <hr>
            <div class="form-group bg-white text-black text-center">
            <p>Kembali ke Halaman <a href="{{ route('login') }}">Login</a></p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content row justify-content-center mt-5">
            <div class="col-md-12">
                <div class="modal-body justify-content-center align-items-center">
                    <div class="row">
                        <div class="col text-center">
                            <img style="width: 400px;" class="img-fluid" src="{{ asset('assets/Email.png') }}" alt="Success Image" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center">
                            <h5 class="card-title" id="successModalLabel">Cek Email Anda</h5>
                            <p>Permintaan perubahan password berhasil dikirim ke email Anda. Cek email dan klik tombol “Buat Password Baru” untuk memperbarui kata sandi.</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center align-items-center">
                    <a href="https://mail.google.com/" class="btn btn-primary btn-block">Cek Email</a>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<!-- Bootstrap 4 JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
</body>
@include('layouts.footer')
<script>
    function toggleSubmitButton() {
      // Get the email input field
      var emailInput = document.getElementById('email');
      // Get the submit button
      var submitButton = document.getElementById('submitButton');

      // Check if the email input field value is empty or not
      if (emailInput.value.trim() !== '') {
        // If the email input field is not empty, enable the submit button and set its class to btn-primary
        submitButton.disabled = false;
        submitButton.classList.remove('btn-secondary');
        submitButton.classList.add('btn-primary');
      } else {
        // If the email input field is empty, disable the submit button and set its class to btn-secondary
        submitButton.disabled = true;
        submitButton.classList.remove('btn-primary');
        submitButton.classList.add('btn-secondary');
      }
    }

    // Add an event listener for form submission
document.querySelector('form').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent the form from submitting normally

    // Get the form data
    var formData = new FormData(event.target);

    // Make an AJAX request to the backend
    fetch(event.target.action, {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        if (data.status === 'success') {
            $('#successModal').modal('show'); // Show the success modal
        } else {
            // Handle other cases if needed
        }
    })
    .catch(error => {
        console.error('Error:', error);
    });
});

</script>
</html>
