{{-- @extends('layouts.app')

@section('content') --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Set Up Two Factor Authentication') }}</div>

                                        <img src="{{ $qrCode }}" alt="QR Code">
                    <p>Scan the QR code with your authenticator app and enter the code to complete the setup process.</p>
                    <p>Secret Key: {{ $secret }}</p>
                </div>
            </div>
        </div>
    </div>
{{-- @endsection --}}
