{{-- @extends('layouts.app') --}}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Set Up Two Factor Authentication') }}</div>

                    <div class="card-body">
                        <p>{{ __('Set up your 2fa.') }}</p>

                        <!-- resources/views/auth/setup-2fa.blade.php -->

                            <form method="POST" action="{{ route('enable-2fa') }}">
                                @csrf

                                <button type="submit">Enable Two-Factor Authentication</button>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
