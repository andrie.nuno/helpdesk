@extends('layouts.app')


@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-lg-12">
                <h1 class="m-0 text-bold text-dark">Overview</h1>
            </div>
        </div>
    </div>
</div>

    <style>
        .card {
    border: 1px solid #ccc;
    border-radius: 8px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    transition: box-shadow 0.3s ease;
}

.card:hover {
    box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
}

.card-title {
    color: #333;
    font-size: 18px;
    font-weight: bold;
}

.card-text {
    font-size: 16px;
}

    </style>

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
                <!-- Task Cards -->
<div class="row">
    <div class="col-md-3 mb-4">
        <div class="card bg-light">
            <div class="card-body">
                <img class="float-left mr-2" style="width: 18px" src="{{ asset('assets/Pending.png') }}" alt="Logo">
                <h5 class="card-title text-muted">Task Pending</h5>
                <p class="card-text">{{ $pending }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card bg-light">
            <div class="card-body">
                <img class="float-left mr-2" style="width: 18px" src="{{ asset('assets/file-text.png') }}" alt="Logo">
                <h5 class="card-title text-muted">Task Open</h5>
                <p class="card-text">{{ $open }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card bg-light">
            <div class="card-body">
                <img class="float-left mr-2" style="width: 18px" src="{{ asset('assets/check-circle.png') }}" alt="Logo">
                <h5 class="card-title text-muted">Task Done</h5>
                <p class="card-text">{{ $done }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card bg-light">
            <div class="card-body">
                <img class="float-left mr-2" style="width: 18px" src="{{ asset('assets/clipboard.png') }}" alt="Logo">
                <h5 class="card-title text-muted">Total Task</h5>
                <p class="card-text">{{ $total }}</p>
            </div>
        </div>
    </div>
</div>
<!-- End Task Cards -->


            <!-- Workspace Point -->
            <div class="row mt-4">
                <div class="col-lg-12">
                    <div class="col-md-12 p-0 pb-3 d-flex justify-content-between align-items-center">
                        <h2 class="text-bold">Workspace Point</h2>
                        <span data-href="{{ route('picexcel') }}" id="export" class="btn btn-outline-primary btn-sm theme-button-colour" onclick="exportTasks(event.target);"><i class="bi bi-download pr-1"></i>  Report</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama PIC</th>
                                    <th>Total Task</th>
                                    <th>Task Pending</th>
                                    <th>Task Done</th>
                                    <th data-toggle="tooltip" data-placement="right" title="Waktu rata-rata dalam menyelesaikan task">Rerata Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($pics)
                                @php($nomor=1)
                                @foreach($pics as $dita)
                                <tr>
                                    <td>{{ $nomor++ }}</td>
                                    <td>{{ $dita->namaUser }}</td>
                                    <td>{{ $dita->totalTask }}</td>
                                    <td>{{ $dita->pendingTask }}</td>
                                    <td>{{ $dita->doneTask }}</td>
                                    <td>{{ $dita->averageDuration }}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End Workspace Point -->
        </div>

        <!-- Content Header (Page header) -->
    <div class="content-header mt-4">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-bold text-dark">Task Overdue</h1>
                </div>
            </div>
            <nav class="nav nav-pills nav-fill">
                <a class="nav-link navbut active" data-section="problem" href="#">Problem</a>
                <a class="nav-link navbut" data-section="incident" href="#">Incident/Bug System</a>
                <a class="nav-link navbut" data-section="pelaporan" href="#">Pelaporan</a>
            </nav>
        </div>

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.problempopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="nama_task" placeholder="Search Task..." name="nama_task" value="" autocomplete="off">
                                                    </div>

                                                    @if (Auth::user()->idLevel != 2)
                                                        <div class="col-sm-3">
                                                            <select class="form-control" id="id_pic" name="id_pic">
                                                                <option value="">Semua PIC</option>
                                                                @foreach($pic as $dita)
                                                                    <option value="{{ $dita->idUser }}">{{ $dita->namaUser }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggalAwal">Tanggal Awal</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAwal" name="tanggalAwal" value="">
                                                    </div>
                                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                                    <label class="col-sm-2 col-form-label" for="tanggalAkhir">Tanggal Akhir</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAkhir" name="tanggalAkhir" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="filterwilayah" class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="nama_wilayah">Nama Wilayah</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_wilayah" name="nama_wilayah">
                                                        <option value="">Semua Wilayah</option>
                                                        @foreach($wilayahs as $wilayah)
                                                            <option value="{{ $wilayah->idWilayah }}">{{ $wilayah->namaWilayah }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-2 col-form-label" for="nama_cabang">Nama Cabang</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_cabang" name="nama_cabang">
                                                        <option value="">Semua Cabang</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="nama_unit">Nama Unit</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_unit" name="nama_unit">
                                                        <option value="">Semua Unit</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-10 pl-3">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="dash" id="dash" value="{{ $dash }}">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="bi bi-search pr-1"></i> Search</button></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->


@endsection
@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });


    document.addEventListener('DOMContentLoaded', function() {
    var navLinks = document.querySelectorAll('.navbut');

    navLinks.forEach(function(link) {
        link.addEventListener('click', function(event) {
            event.preventDefault(); // Prevent the default link behavior

             // Remove the 'active' class from all nav links
             navLinks.forEach(function(navLink) {
                    navLink.classList.remove('active');
                });

                // Add the 'active' class to the clicked nav link
                this.classList.add('active');

            var section = this.getAttribute('data-section');
            var formAction = '';

            // Determine the appropriate form action based on the section
            switch (section) {
                case 'problem':
                    formAction = "{{ route('login.problempopulate') }}";
                    enableFilters();
                    break;
                case 'incident':
                    formAction = "{{ route('login.incidentpopulate') }}";
                    enableFilters();
                    break;
                case 'pelaporan':
                    formAction = "{{ route('login.pelaporanpopulate') }}";
                    // Disable and hide the filters for nama wilayah, nama cabang, and nama unit
                    disableFilters();
                    break;
                default:
                    // Default to the problem section if no match found
                    enableFilters();
                    formAction = "{{ route('login.problempopulate') }}";
            }

            // Update the form action
            document.getElementById('form-searchPopulate').setAttribute('action', formAction);
            setTimeout(function(){
                doSearch('Populate');
            }, 500);
        });
    });
});

// Function to disable and hide filters for nama wilayah, nama cabang, and nama unit
function disableFilters() {
    document.getElementById('nama_wilayah').disabled = true;
    document.getElementById('nama_wilayah').style.display = 'none';

    document.getElementById('nama_cabang').disabled = true;
    document.getElementById('nama_cabang').style.display = 'none';

    document.getElementById('nama_unit').disabled = true;
    document.getElementById('nama_unit').style.display = 'none';

    document.getElementById('filterwilayah').style.display = 'none';
}

function enableFilters() {
    document.getElementById('nama_wilayah').disabled = false;
    document.getElementById('nama_wilayah').style.display = 'block';

    document.getElementById('nama_cabang').disabled = false;
    document.getElementById('nama_cabang').style.display = 'block';

    document.getElementById('nama_unit').disabled = false;
    document.getElementById('nama_unit').style.display = 'block';

    document.getElementById('filterwilayah').style.display = 'block';
}


    $("#nama_wilayah").change(function() {
    var idWilayah = $('select[name="nama_wilayah"]').val();
    $("#nama_cabang").html('');
    $("#nama_unit").html('<option value="">== Pilih Unit ==</option>');
    // alert(idWilayah);
    $.ajax({
        type: "POST",
        url: "problempopulateajax?type=cabang&_token={{ csrf_token() }}",
        data: {
            idWilayah: idWilayah
        },
        success: function (result1) {
            $return1 = result1;
            $('#nama_cabang').append($return1);
        }
    });
});

$("#nama_cabang").change(function() {
    var idCabang = $('select[name="nama_cabang"]').val();
    $("#nama_unit").html('');
    // alert(idCabang);
    $.ajax({
        type: "POST",
        url: "problempopulateajax?type=unit&_token={{ csrf_token() }}",
        data: {
            idCabang: idCabang
        },
        success: function (result2) {
            $return2 = result2;
            $('#nama_unit').append($return2);
        }
    });
});

function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }

    document.addEventListener('DOMContentLoaded', function() {
    var navLinks = document.querySelectorAll('.navbut');

    navLinks.forEach(function(link) {
        link.addEventListener('click', function(event) {
            event.preventDefault(); // Prevent the default link behavior

            // Reset filter fields and enable/disable them as needed
            resetFilters();

            // Remove the 'active' class from all nav links
            navLinks.forEach(function(navLink) {
                navLink.classList.remove('active');
            });

            // Add the 'active' class to the clicked nav link
            this.classList.add('active');

            var section = this.getAttribute('data-section');
            var formAction = '';

            // Determine the appropriate form action based on the section
            switch (section) {
                case 'problem':
                    formAction = "{{ route('login.problempopulate') }}";
                    enableFilters();
                    break;
                case 'incident':
                    formAction = "{{ route('login.incidentpopulate') }}";
                    enableFilters();
                    break;
                case 'pelaporan':
                    formAction = "{{ route('login.pelaporanpopulate') }}";
                    // Disable and hide the filters for nama wilayah, nama cabang, and nama unit
                    disableFilters();
                    break;
                default:
                    // Default to the problem section if no match found
                    enableFilters();
                    formAction = "{{ route('login.problempopulate') }}";
            }

            // Update the form action
            document.getElementById('form-searchPopulate').setAttribute('action', formAction);
            setTimeout(function(){
                doSearch('Populate');
            }, 500);
        });
    });
});

// Function to reset filter fields and enable/disable them as needed
function resetFilters() {
    document.getElementById('nama_task').value = ''; // Reset the task name input field

    // Reset the date input fields
    document.getElementById('tanggalAwal').value = '';
    document.getElementById('tanggalAkhir').value = '';

    // Reset and enable nama wilayah filter
    var namaWilayahSelect = document.getElementById('nama_wilayah');
    namaWilayahSelect.value = '';
    namaWilayahSelect.disabled = false;
    namaWilayahSelect.style.display = 'block';

    // Clear and enable nama cabang filter
    var namaCabangSelect = document.getElementById('nama_cabang');
    namaCabangSelect.innerHTML = '<option value="">Semua Cabang</option>';
    namaCabangSelect.disabled = false;
    namaCabangSelect.style.display = 'block';

    // Clear and enable nama unit filter
    var namaUnitSelect = document.getElementById('nama_unit');
    namaUnitSelect.innerHTML = '<option value="">Semua Unit</option>';
    namaUnitSelect.disabled = false;
    namaUnitSelect.style.display = 'block';

    // Show and enable the filterwilayah section
    document.getElementById('filterwilayah').style.display = 'block';
}

</script>
@endsection
