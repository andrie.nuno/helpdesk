<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th colspan="2">Urut</th>
                <th>Menu</th>
                <th>Segmen</th>
                <th>URL</th>
                <th>Navigasi</th>
                <th><center>Status</center></th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
            @if($data)
                @foreach($data as $dita)
                <tr>
                    <td>{{ $dita->menuUrut }}</td>
                    <td></td>
                    <td>
                        @if($dita->menuNav == "Header")
                            <a onclick="showMultiModal(1, 'createchild', '{{ csrf_token() }}', {{ $dita->menuId }}, '{{ route('login.menumodal') }}')" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                        @else
                            ----
                        @endif
                        {{ $dita->menuNama }}
                    </td>
                    <td>{{ $dita->menuSegmen }}</td>
                    <td>{{ $dita->menuUrl }}</td>
                    <td>{{ $dita->menuNav }}</td>
                    <td><center>{{ $dita->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                    <td><center><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', {{ $dita->menuId }}, '{{ route('login.menumodal') }}')"><img src="{{ asset('assets/edit.svg') }}" alt=""></a></center></td>
                </tr>
                @if($dita->sub)
                    @foreach($dita->sub as $sub)
                    <tr>
                        <td></td>
                        <td>{{ $sub->menuUrut }}</td>
                        <td>---- {{ $sub->menuNama }}</td>
                        <td>{{ $sub->menuSegmen }}</td>
                        <td>{{ $sub->menuUrl }}</td>
                        <td>{{ $sub->menuNav }}</td>
                        <td><center>{{ $sub->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                        <td><center><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', {{ $sub->menuId }}, '{{ route('login.menumodal') }}')"><img src="{{ asset('assets/edit.svg') }}" alt=""></a></center></td>
                    </tr>
                    @endforeach
                @endif
                @endforeach
            @endif
        </tbody>
    </table>
</div>
