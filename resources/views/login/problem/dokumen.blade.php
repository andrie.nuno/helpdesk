<style>
    .modal-backdrop {
        opacity: 0.5; /* Adjust the opacity value as needed */
        background-color: rgba(0, 0, 0, 0.5); /* Adjust the RGBA values for the desired color and opacity */
        /* z-index: 1055; */
    }

    #imageModal {
            /* display: flex; */
            align-items: center;
            justify-content: center;
            margin: 0;
            z-index: 1095;
        }
</style>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title bold">Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="card">
            <img id="modalImage" src="{{ $data }}" style="width: 100%;" alt="Modal Image">
        </div>
    </div>
    <!-- Add a modal dialog for the image -->
    {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(2)"><i class="fa fa-remove"></i> Close</button>
    </div> --}}
</div>
<div id="imageModal" class="modal fade" role="dialog" style="z-index: 1080;">
    <div class="modal-dialog modal-lg">
        <!-- Modal content -->
        <div class="modal-content">

        </div>
    </div>
</div>




<script type="text/javascript">
$(document).ready(function(e){
    sizeMultiModal(2, "lg"); // large
    centerMultiModal(2);

});
</script>

