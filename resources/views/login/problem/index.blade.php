@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            {{-- <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Master Kategori</h1>
            </div> --}}
            <nav class="nav nav-pills nav-fill">
                <a class="nav-link active" aria-current="page" href="{{ route("login.problem") }}">Problem</a>
                <a class="nav-link" href="{{ route("login.incident") }}">Incident/Bug System</a>
                <a class="nav-link" href="{{ route("login.pelaporan") }}">Pelaporan</a>
            </nav>
        </div>


    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.problempopulate') }}" class="row" id="form-searchPopulate">

                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="nama_task" placeholder="Search Task..." name="nama_task" value="" autocomplete="off">
                                                    </div>

                                                    @if (Auth::user()->idLevel != 2)
                                                        <div class="col-sm-3">
                                                            <select class="form-control" id="id_pic" name="id_pic">
                                                                <option value="">Semua PIC</option>
                                                                @foreach($pics as $pic)
                                                                    <option value="{{ $pic->idUser }}">{{ $pic->namaUser }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @endif
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggalAwal">Tanggal Awal</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAwal" name="tanggalAwal" value="">
                                                    </div>
                                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                                    <label class="col-sm-2 col-form-label" for="tanggalAkhir">Tanggal Akhir</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAkhir" name="tanggalAkhir" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="nama_wilayah">Nama Wilayah</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_wilayah" name="nama_wilayah">
                                                        <option value="">Semua Wilayah</option>
                                                        @foreach($wilayahs as $wilayah)
                                                            <option value="{{ $wilayah->idWilayah }}">{{ $wilayah->namaWilayah }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-2 col-form-label" for="nama_cabang">Nama Cabang</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_cabang" name="nama_cabang">
                                                        <option value="">Semua Cabang</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="nama_unit">Nama Unit</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_unit" name="nama_unit">
                                                        <option value="">Semua Unit</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-10 pl-3">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="bi bi-search pr-1"></i> Search</button></a>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <a onclick="showMultiModal(1, 'create', '{{ csrf_token() }}', '', '{{ route('login.problemmodal') }}')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour float-right"><i class="bi bi-plus pr-1"></i> Tambah Task</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    @endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });

$("#nama_wilayah").change(function() {
    var idWilayah = $('select[name="nama_wilayah"]').val();
    $("#nama_cabang").html('');
    $("#nama_unit").html('<option value="">== Pilih Unit ==</option>');
    // alert(idWilayah);
    $.ajax({
        type: "POST",
        url: "problempopulateajax?type=cabang&_token={{ csrf_token() }}",
        data: {
            idWilayah: idWilayah
        },
        success: function (result1) {
            $return1 = result1;
            $('#nama_cabang').append($return1);
        }
    });
});

$("#nama_cabang").change(function() {
    var idCabang = $('select[name="nama_cabang"]').val();
    $("#nama_unit").html('');
    // alert(idCabang);
    $.ajax({
        type: "POST",
        url: "problempopulateajax?type=unit&_token={{ csrf_token() }}",
        data: {
            idCabang: idCabang
        },
        success: function (result2) {
            $return2 = result2;
            $('#nama_unit').append($return2);
        }
    });
});

document.getElementById('toggleFilters').addEventListener('click', function() {
        var filterOptions = document.getElementById('filterOptions');
        if (filterOptions.style.display === 'none') {
            filterOptions.style.display = 'block';
        } else {
            filterOptions.style.display = 'none';
        }
    });
</script>
@endsection
