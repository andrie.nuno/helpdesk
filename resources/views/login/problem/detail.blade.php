<style>
    .modal-backdrop {
        opacity: 0.5; /* Adjust the opacity value as needed */
        background-color: rgba(0, 0, 0, 0.5); /* Adjust the RGBA values for the desired color and opacity */
        /* z-index: 1055; */
    }

    #imageModal {
            /* display: flex; */
            align-items: center;
            justify-content: center;
            margin: 0;
            z-index: 1095;
        }
</style>
<div id="detail" class="modal-content">
    <div id="detail-header" class="modal-header">
        <h4 class="modal-title bold">Detail Task Problem</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
        <div class="modal-body">
            <div class="card">
                {{-- @dd($data) --}}
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Task :</b> {{ $data->namaTask }}</li>
                            <li class="list-group-item"><b>Wilayah :</b> {{ $data->namaWilayah }}</li>
                            <li class="list-group-item"><b>Cabang :</b> {{ $data->namaCabang }}</li>
                            <li class="list-group-item"><b>Unit :</b> {{ $data->namaUnit }}</li>
                            <li class="list-group-item"><b>Subjek :</b> {{ $data->namaSubjek }}</li>
                            <li class="list-group-item"><b>Sub Subjek :</b> {{ $data->namaSubSubjek }}</li>
                            <li class="list-group-item">
                                <b>Dokumen :</b>
                                @if (pathinfo($data->dokumen, PATHINFO_EXTENSION) == 'pdf')
                                    <a href="{{ $data->dokumen }}" class="btn btn-primary" download>Download PDF</a>
                                @else
                                    <a onclick="showMultiModal(2, 'dokumen', '{{ csrf_token() }}', '{{ $data->dokumen }}', '{{ route('login.problemmodal') }}')" href="#" class="modal-trigger" data-image="{{ $data->dokumen }}">
                                        <img style="width: 300px" src="{{ $data->dokumen }}">
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>PIC :</b> {{ $data->namaUser }}</li>
                            <li class="list-group-item"><b>Start Date :</b> {{ $data->startDate }}</li>
                            <li class="list-group-item"><b>Due Date :</b> {{ $data->dueDate }}</li>
                            <li class="list-group-item"><b>Status :</b> {{ $data->status == 1 ? 'Open' : ($data->status == 2 ? 'Pending' : 'Done') }}</li>
                            <li class="list-group-item"><b>Overdue :</b> {{ abs($data->overdueDays) }} Hari</li>
                            <li class="list-group-item"><b>Keterangan :</b> {{ $data->keterangan }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add a modal dialog for the image -->
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
    </div>
</div>




<script type="text/javascript">
$(document).ready(function(e){
    sizeMultiModal(1, "lg"); // large
    centerMultiModal(1);

    // $('.modal-trigger').on('click', function() {
    //     var imageSrc = $(this).data('image');
    //     $('#modalImage').attr('src', imageSrc);

    //     $('#detail').css('z-index', 10); // Set lower z-index for #detail modal
    // });

    // // When the image modal is closed
    // $('#imageModal').on('hidden.bs.modal', function () {
    //     $('#detail').css('z-index', 1050); // Restore original z-index for #detail modal
    // });
});
</script>

