    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Tambah Problem</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div id="modal_form" current="1">
        <div class="modal-body">
            <form action="{{ route('login.problempost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idWilayah">Wilayah *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idWilayah" name="idWilayah">
                                    <option value="">== Pilih Wilayah ==</option>
                                    @foreach($wilayahs as $wilayah)
                                        <option value="{{ $wilayah->idWilayah }}">{{ $wilayah->namaWilayah }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idCabang">Cabang *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idCabang" name="idCabang">
                                    <option value="">== Pilih Cabang ==</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idUnit">Unit *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idUnit" name="idUnit">
                                    <option value="">== Pilih Unit ==</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idTask">Task *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idTask" name="idTask" onchange="toggleUploadDokumen()">
                                    <option value="">== Pilih Task ==</option>
                                    @foreach($tasks as $task)
                                        <option value="{{ $task->idTask }}" data-is-upload="{{ $task->isUpload }}">{{ $task->namaTask }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idSubjek">Subjek *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idSubjek" name="idSubjek">
                                    <option value="">== Pilih Subjek ==</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="subSubjekGroup" style="display: none;">
                            <label class="col-sm-3 col-form-label" for="idSubSubjek">Sub Subjek *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idSubSubjek" name="idSubSubjek">
                                    <option value="">== Pilih Sub Subjek ==</option>
                                </select>
                            </div>
                        </div>

                        <!-- Add style="display: none;" to hide the Upload Dokumen input initially -->
                        <div class="form-group row" id="uploadDokumenGroup" style="display: none;">
                            <label class="col-sm-3 col-form-label" for="dokumen">Upload Dokumen</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control-file" id="dokumen" name="dokumen" value="" autocomplete="off" required>
                            </div>
                        </div>

                    </div>


                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="idPic">PIC *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="idPic" name="idPic">
                                    @if (Auth::user()->idLevel != 2)
                                        <option value="">== Pilih PIC ==</option>
                                        @foreach($pics as $pic)
                                            <option value="{{ $pic->idUser }}">{{ $pic->namaUser }}</option>
                                        @endforeach
                                        @else
                                        <option value="{{ Auth::user()->idUser }}">{{ Auth::user()->namaUser }}</option>
                                    @endif
                                </select>
                            </div>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                            <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="startDate">Start Date *</label>
                            <div class="col-sm-9">
                                <input type="datetime-local" class="form-control" id="startDate" name="startDate" value="{{ date('Y-m-d h:i:s') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="dueDate">Due Date *</label>
                            <div class="col-sm-9">
                                <input type="datetime-local" class="form-control" id="dueDate" name="dueDate" value="{{ date('Y-m-d h:i:s') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="keterangan">Keterangan</label>
                            <div class="col-sm-9">
                                <textarea class="ckeditor form-control" id="keterangan" name="keterangan" autocomplete="off" required rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="status">Status *</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="status" name="status">
                                    <option value="1">OPEN</option>
                                    <option value="2">PENDING</option>
                                    <option value="3">DONE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
        <div class="modal-footer">
            <button type="submit" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Simpan</button>
            <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(e){
        sizeMultiModal(1, "lg"); // large
        // sizeMultiModal(1, "xl"); // extra large
        // sizeMultiModal(1, "sm"); // small
        centerMultiModal(1);

        toggleSubSubjekVisibility();
        setDefaultDates();
    });

    // Function to show or hide Sub Subjek based on conditions
    function toggleSubSubjekVisibility() {
        var idSubjek = $('select[name="idSubjek"]').val();
        var subSubjekGroup = $('#subSubjekGroup');

        // Check if Subjek is chosen and has options available
        if (idSubjek && $('#idSubjek option[value="' + idSubjek + '"]').length > 0) {
            subSubjekGroup.show();
        } else {
            subSubjekGroup.hide();
        }
    }

    function setDefaultDates() {
        // Get today's date
        var start = new Date();
        var due = new Date();

        start.setHours(start.getHours() + 7);
        due.setHours(25, 30, 0, 0);


        // Format the date to match the input field's value format
        var formattedStartDate = start.toISOString().slice(0, 16);
        var formattedDueDate = due.toISOString().slice(0, 16);



        // Set the default value for the dueDate input field
        $('#startDate').val(formattedStartDate);
        $('#dueDate').val(formattedDueDate);
    }

    function toggleUploadDokumen() {
            // Get the selected value of idTask
            var selectedTask = $('#idTask option:selected');

            // Check the condition (e.g., isUpload equals 1)
            var isUpload = selectedTask.data('is-upload');

            // Show or hide the Upload Dokumen input based on the condition
            if (isUpload == 1) {
                $('#uploadDokumenGroup').show();
            } else {
                $('#uploadDokumenGroup').hide();
            }
        }

    $("#idWilayah").change(function() {
        var idWilayah = $('select[name="idWilayah"]').val();
        $("#idCabang").html('');
        $("#idUnit").html('<option value="">== Pilih Unit ==</option>');
        // alert(idWilayah);
        $.ajax({
            type: "POST",
            url: "problempopulateajax?type=cabang&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah
            },
            success: function (result) {
                $return = result;
                $('#idCabang').append($return);
            }
        });
    });

    $("#idCabang").change(function() {
        var idCabang = $('select[name="idCabang"]').val();
        $("#idUnit").html('');

        // alert(idCabang);
        $.ajax({
            type: "POST",
            url: "problempopulateajax?type=unit&_token={{ csrf_token() }}",
            data: {
                idCabang: idCabang
            },
            success: function (result) {
                $return = result;
                $('#idUnit').append($return);
            }
        });
    });

    $("#idTask").change(function() {
        var idTask = $('select[name="idTask"]').val();
        $("#idSubjek").html('');
        $("#idSubSubjek").html('<option value="">== Pilih Sub Subjek ==</option>');

        toggleSubSubjekVisibility();

        // alert(idTask);
        $.ajax({
            type: "POST",
            url: "problempopulateajax?type=subjek&_token={{ csrf_token() }}",
            data: {
                idTask: idTask
            },
            success: function (result) {
                $return = result;
                $('#idSubjek').append($return);
            }
        });
    });

    $("#idSubjek").change(function() {
        var idSubjek = $('select[name="idSubjek"]').val();
        $("#idSubSubjek").html('');

        // Update the visibility of Sub Subjek based on the selected Subjek
        toggleSubSubjekVisibility();

        // alert(idSubjek);
        $.ajax({
            type: "POST",
            url: "problempopulateajax?type=subsubjek&_token={{ csrf_token() }}",
            data: {
                idSubjek: idSubjek
            },
            success: function (result) {
                $return = result;
                $('#idSubSubjek').append($return);

                // Show the Sub Subjek dropdown only if there are results
                var subSubjekGroup = $('#subSubjekGroup'); // Define subSubjekGroup here
                if ($('#idSubSubjek option').length > 1) {
                    subSubjekGroup.show();
                } else {
                    subSubjekGroup.hide(); // Hide if no options
                }
            }
        });
    });

    </script>
