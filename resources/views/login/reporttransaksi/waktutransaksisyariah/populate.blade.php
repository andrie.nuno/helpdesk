<div class="table-responsive">
    
    <div class="col-md-12">
        <span data-href="{{ route('waktutransaksisyariahexcel') }}?nama_wilayah={{ $nama_wilayah }}&tgl_awal={{ $tgl_awal }}&tgl_sampai={{ $tgl_sampai }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span>
    </div>
    
    <p>&nbsp;</p>
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Outlet</th>
                <th>Nama Outlet</th>
                <th>Nama Cabang</th>
                <th>Nama Wilayah</th>
                <th>Tanggal Pencairan</th>
                <th>No SBG</th>
                <th>Jenis Pembayaran Final</th>
                <th>CIF</th>
                <th>Nama Customer</th>
                <th>No KTP</th>
                <th>Waktu Input FAPG</th>
                <th>Waktu Taksir Penaksir</th>
                <th>Waktu Taksir Ka Unit</th>
                <th>Waktu Input Gadai</th>
                <th>Tanggal Approval Unit</th>
                <th>Tanggal Approval Kacab</th>
                <th>Tanggal Approval Kawil</th>
                <th>Tanggal Approval Direktur</th>
                <th>Tanggal Approval Dirut</th>
                <th>Waktu Cetak Kwitansi</th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->kodeOutlet }}</td>
                <td>{{ $dita->namaOutlet }}</td>
                <td>{{ $dita->namaCabang }}</td>
                <td>{{ $dita->namaWilayah }}</td>
                <td>{{ $dita->tanggalPencairan }}</td>
                <td>{{ $dita->noSbg }}</td>
                <td>{{ $dita->jenisPembayaranFinal }}</td>
                <td>{{ $dita->cif }}</td>
                <td>{{ $dita->namaCustomer }}</td>
                <td>{{ $dita->noKtp }}</td>
                <td>{{ $dita->timeInputFAPG }}</td>
                <td>{{ $dita->timeTaksirPenaksir }}</td>
                <td>{{ $dita->timeTaksirKaUnit }}</td>
                <td>{{ $dita->timeInputGadai }}</td>
                <td>{{ $dita->tglApprovalKaunit }}</td>
                <td>{{ $dita->tglApprovalKacab }}</td>
                <td>{{ $dita->tglApprovalKawil }}</td>
                <td>{{ $dita->tglApprovalDirektur }}</td>
                <td>{{ $dita->tglApprovalDirut }}</td>
                <td>{{ $dita->timeCetakKwitansi }}</td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
