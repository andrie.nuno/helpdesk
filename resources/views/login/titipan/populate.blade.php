<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Cabang</th>
                <th>No SBG</th>
                <th>Nominal</th>
                <th>Tanggal</th>
                @if(Auth::user()->idLevel == 1)
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td nowrap>{{ $nomor++ }}</td>
                <td nowrap>{{ $dita->kodeCabang }} - {{ $dita->namaCabang }}</td>
                @if($dita->isJurnal == 0)
                    <td nowrap>{{ $dita->noSbg }}</td>
                @else
                    <td nowrap><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.titipanmodal') }}')"><span style="color:blue">{{ $dita->noSbg }}</span></a></td>
                @endif
                <td align="right" nowrap>{{ number_format($dita->nominalTitipan) }}</td>
                <td nowrap>{{ $dita->tanggal }}</td>
                <td>
                @if(Auth::user()->idLevel == 1 && $dita->isJurnal == 0)
                    <center><a onclick="showMultiModal(1, 'jurnal', '{{ csrf_token() }}', '{{ $dita->idTitipan }}', '{{ route('login.titipanmodal') }}')"><span style="color:blue">PROSES</span></a></center>
                @endif
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });

    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
