<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Menu Coa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.dashboardpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="UTF-8">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coa">Coa Promas *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coa" name="coa" value="{{ $data->coa }}" autocomplete="off" required>
                    <input type="hidden" name="idCoa" id="idCoa" value="{{ $data->idCoa }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coaOracle">Coa Oracle *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coaOracle" name="coaOracle" value="{{ $data->coaOracle }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="description">Deskripsi *</label>
                <div class="col-sm-9">
                    <textarea class="ckeditor form-control" id="description" name="description" autocomplete="off" required rows="3">{{ $data->description }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="headAccount">Head Account *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="headAccount" name="headAccount" value="{{ $data->headAccount }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="usedFor">Used For *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="usedFor" name="usedFor">
                        <option value="">== Pilih Used For ==</option>
                        @foreach($usedfors as $usedFor)
                            <option value="{{ $usedFor->idUsedfor }}" {{ $data->idUsedfor == $usedFor->idUsedfor ? "selected":"" }}>{{ $usedFor->namaUsedfor }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="cashFlow">Cash Flow *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="cashFlow" name="cashFlow">
                        <option value="">== Pilih Cash Flow ==</option>
                        @foreach($cashflows as $cashflow)
                            <option value="{{ $cashflow->idCashflow }}" {{ $data->idCashflow == $cashflow->idCashflow ? "selected":"" }}>{{ $cashflow->kodeCashflow }}-{{ $cashflow->namaCashflow }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="accountType">Account Type *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="accountType" name="accountType">
                        <option value="Asset" {{ $data->accountType == "Asset" ? 'selected="selected"' : '' }}>Asset</option>
                        <option value="Bank" {{ $data->accountType == "Bank" ? 'selected="selected"' : '' }}>Bank</option>
                        <option value="Expense" {{ $data->accountType == "Expense" ? 'selected="selected"' : '' }}>Expense</option>
                        <option value="Fixed Asset" {{ $data->accountType == "Fixed Asset" ? 'selected="selected"' : '' }}>Fixed Asset</option>
                        <option value="Liability" {{ $data->accountType == "Liability" ? 'selected="selected"' : '' }}>Liability</option>
                        <option value="Owners' equity" {{ $data->accountType == "Owners' equity" ? 'selected="selected"' : '' }}>Owners' equity</option>
                        <option value="Revenue" {{ $data->accountType == "Revenue" ? 'selected="selected"' : '' }}>Revenue</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="qualifiers">Qualifiers *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="qualifiers" name="qualifiers">
                        <option value="Asset" {{ $data->qualifiers == "Asset" ? 'selected="selected"' : '' }}>Asset</option>
                        <option value="Expense" {{ $data->qualifiers == "Expense" ? 'selected="selected"' : '' }}>Expense</option>
                        <option value="Liability" {{ $data->qualifiers == "Liability" ? 'selected="selected"' : '' }}>Liability</option>
                        <option value="Owners' equity" {{ $data->qualifiers == "Owners' equity" ? 'selected="selected"' : '' }}>Owners' equity</option>
                        <option value="Revenue" {{ $data->qualifiers == "Revenue" ? 'selected="selected"' : '' }}>Revenue</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="jenisCoa">Jenis Coa *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="jenisCoa" name="jenisCoa">
                        <option value="0" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>Konven & Umum</option>
                        <option value="1" {{ $data->isActiveSyariah == 1 ? 'selected="selected"' : '' }}>SYARIAH</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="rekonBank">Rekon Bank *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="rekonBank" name="rekonBank">
                        <option value="0" {{ $data->statusRekonBank == 0 ? 'selected="selected"' : '' }}>COA Transaksi</option>
                        <option value="1" {{ $data->statusRekonBank == 1 ? 'selected="selected"' : '' }}>Bank Konven</option>
                        <option value="2" {{ $data->statusRekonBank == 2 ? 'selected="selected"' : '' }}>Bank Syariah</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(1, "lg"); // large
    sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});

</script>
