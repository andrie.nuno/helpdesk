<div class="table-responsive">
    <div class="col-md-12 p-0">
        {{-- {{ dd($data)->toSql() }} --}}
        <span data-href="{{ route('taskexcel') }}?tanggal_awal={{ $tanggal_awal }}&tanggal_akhir={{ $tanggal_akhir }}&pic={{ $pic }}&nama_wilayah={{ $nama_wilayah }}&nama_cabang={{ $nama_cabang }}&nama_unit={{ $nama_unit }}&nama_task={{ $nama_task }}&dash={{ $dash }}" id="export" class="btn btn-outline-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="bi bi-download pr-1"></i>  Report</span>
    </div>
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Task</th>
                <th>Subjek</th>
                <th>Unit</th>
                <th>PIC</th>
                <th>Due Date</th>
                <th><center>Status</center></th>
                <th>Overdue</th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
        {{-- @dd($data) --}}
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->namaTask }}</td>
                <td>{{ $dita->namaSubjek }}</td>
                <td>{{ $dita->namaUnit }}</td>
                <td>{{ $dita->namaUser }}</td>
                <td>{{ $dita->dueDate }}</td>
                <td><center>{{ $dita->status == 1 ? 'Open' : ($dita->status == 2 ? 'Pending' : 'Done') }}</center></td>
                @if ($dita->overdueDays)
                <td><center>{{ abs($dita->overdueDays) }} Hari</center></td>
                @else
                <td><center> - </center></td>
                @endif
                <td>
                    <center>
                        @if($dita->status == 3)
                            <a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idTaskIncident }}', '{{ route('login.incidentmodal') }}')"><span class="btn btn-sm btn-outline-primary theme-button-colour">Detail</span></a>

                        @else
                            <a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idTaskIncident }}', '{{ route('login.incidentmodal') }}')"><span class="btn btn-sm btn-outline-primary theme-button-colour">Detail</span></a>
                        @endif
                    </center>
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
