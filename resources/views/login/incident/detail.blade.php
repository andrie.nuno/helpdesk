<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title bold">Detail Task Incident</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
        <div class="modal-body">
            <div class="card">
                {{-- @dd($data) --}}
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Task :</b> {{ $data->namaTask }}</li>
                            <li class="list-group-item"><b>Wilayah :</b> {{ $data->namaWilayah }}</li>
                            <li class="list-group-item"><b>Cabang :</b> {{ $data->namaCabang }}</li>
                            <li class="list-group-item"><b>Unit :</b> {{ $data->namaUnit }}</li>
                            <li class="list-group-item"><b>Subjek :</b> {{ $data->namaSubjek }}</li>
                            <li class="list-group-item"><b>Sub Subjek :</b> {{ $data->namaSubSubjek }}</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>PIC :</b> {{ $data->namaUser }}</li>
                            <li class="list-group-item"><b>Start Date :</b> {{ $data->startDate }}</li>
                            <li class="list-group-item"><b>Due Date :</b> {{ $data->dueDate }}</li>
                            <li class="list-group-item"><b>Status :</b> {{ $data->status == 1 ? 'Open' : ($data->status == 2 ? 'Pending' : 'Done') }}</li>
                            <li class="list-group-item"><b>Overdue :</b> {{ abs($data->overdueDays) }} Hari</li>
                            <li class="list-group-item"><b>Keterangan :</b> {{ $data->keterangan }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
    sizeMultiModal(1, "lg"); // large
    centerMultiModal(1);
});
</script>
