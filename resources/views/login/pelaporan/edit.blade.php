<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Pelaporan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.pelaporanpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="idTask">Task *</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="idTask" name="idTask">
                                <option value="">== Pilih Cabang ==</option>
                                @foreach($tasks as $task)
                                    <option value="{{ $task->idTask }}" data-is-upload="{{ $task->isUpload }}" {{ $data->idTask == $task->idTask ? "selected":"" }}>{{ $task->namaTask }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" id="idTaskPelaporan" name="idTaskPelaporan" value="{{ $data->idTaskPelaporan }}" readonly="readonly">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                        <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="idSubjek">Subjek *</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="idSubjek" name="idSubjek">
                                @php
                                    $selected = false;
                                @endphp
                                @foreach($subjeks as $subjek)
                                    @if ($data->idSubjek == $subjek->idSubjek && !$selected)
                                        @php
                                            $selected = true;
                                        @endphp
                                        <option value="{{ $subjek->idSubjek }}" selected>{{ $subjek->namaSubjek }}</option>
                                    @else
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="startDate">Start Date *</label>
                        <div class="col-sm-9">
                            <input type="datetime-local" class="form-control" id="startDate" name="startDate" value="{{ $data->startDate }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="dueDate">Due Date *</label>
                        <div class="col-sm-9">
                            <input type="datetime-local" class="form-control" id="dueDate" name="dueDate" value="{{ $data->dueDate }}">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="idPic">PIC *</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="idPic" name="idPic">
                                @if (Auth::user()->idLevel != 2)
                                    <option value="">== Pilih PIC ==</option>
                                    @foreach($pics as $pic)
                                        <option value="{{ $pic->idUser }}" {{ $data->idPic == $pic->idUser ? "selected":"" }}>{{ $pic->namaUser }}</option>
                                    @endforeach
                                    @else
                                    <option value="{{ Auth::user()->idUser }}">{{ Auth::user()->namaUser }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="keterangan">Keterangan</label>
                        <div class="col-sm-9">
                            <textarea class="ckeditor form-control" id="keterangan" name="keterangan" autocomplete="off" required rows="3">{{ $data->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="status">Status *</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="status" name="status">
                                <option value="1" {{ $data->status == 1 ? 'selected="selected"' : '' }}>OPEN</option>`
                                <option value="2" {{ $data->status == 2 ? 'selected="selected"' : '' }}>PENDING</option>
                                <option value="3" {{ $data->status == 3 ? 'selected="selected"' : '' }}>DONE</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Simpan</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);

});

$("#idTask").change(function() {
    var idTask = $('select[name="idTask"]').val();
    $("#idSubjek").html('');
    // alert(idTask);
    $.ajax({
        type: "POST",
        url: "pelaporanpopulateajax?type=subjek&_token={{ csrf_token() }}",
        data: {
            idTask: idTask
        },
        success: function (result) {
            $return = result;
            $('#idSubjek').append($return);
        }
    });
});
</script>
