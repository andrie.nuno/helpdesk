<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Modul</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.modulpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaLevel">Nama Modul *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="namaLevel" name="namaLevel" value="{{ $data->namaLevel }}" autocomplete="off" required>
                    <input type="hidden" id="idLevel" name="idLevel" value="{{ $data->idLevel }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg");
	centerMultiModal(1);
});
</script>
