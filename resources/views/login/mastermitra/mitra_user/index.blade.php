@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Master User</h1>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.mitrauserpopulate') }}" class="row" id="form-searchPopulate">
                                            {{-- <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="nama_company">Nama Company</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_company" name="nama_company">
                                                      <option value="">== Pilih Company ==</option>
                                                      @foreach($companys as $company)
                                                          <option value="{{ $company->idCompany }}">{{ $company->namaCompany }}</option>
                                                      @endforeach
                                                    </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-2 col-form-label" for="nama_branch">Nama Branch</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="nama_branch" name="nama_branch">
                                                      <option value="">== Pilih Branch ==</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            {{-- <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="email_user">Email User</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="email_user" name="email_user" value="" autocomplete="off">
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-2 col-form-label" for="nama_user">Nama User</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="nama_user" name="nama_user" value="" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-md-10">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    {{-- <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a> --}}
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <a onclick="showMultiModal(1, 'create', '{{ csrf_token() }}', '', '{{ route('login.mitrausermodal') }}')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour float-right"><i class="fa fa-plus"></i> Create</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    @endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });
</script>
@endsection
