<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Tambah User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.mitrauserpost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idUser">User</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idUser" name="idUser">
                        <option value="0">== Tidak Ada ==</option>
                        @foreach($users as $user)
                            <option value="{{ $user->idUser }}">{{ $user->username }} - {{ $user->namaKaryawan }}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idLevel">Kelompok Akses *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idLevel" name="idLevel">
                        <option value="">== Tidak Ada ==</option>
                        @foreach($levels as $level)
                            <option value="{{ $level->idLevel }}">{{ $level->namaLevel }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
