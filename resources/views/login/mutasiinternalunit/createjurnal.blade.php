<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Create Jurnal Sukses</h4>
        <button type="button" class="close" data-dismiss="modal" onclick="doSearch('Populate');" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1); doSearch('Populate');"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
