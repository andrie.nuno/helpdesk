<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Tambah Menu Coa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.menucoapost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="UTF-8">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coa">Coa Promas *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coa" name="coa" value="" autocomplete="off" required>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coaOracle">Coa Oracle *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coaOracle" name="coaOracle" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="description">Deskripsi *</label>
                <div class="col-sm-9">
                    <textarea class="form-control" id="description" name="description" autocomplete="off" required rows="3"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="headAccount">Head Account *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="headAccount" name="headAccount" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="usedFor">Used For *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="usedFor" name="usedFor">
                        <option value="">== Pilih Used For ==</option>
                        @foreach($usedfors as $usedFor)
                            <option value="{{ $usedFor->idUsedfor }}">{{ $usedFor->namaUsedfor }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="cashFlow">Cash Flow *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="cashFlow" name="cashFlow">
                        <option value="">== Pilih Cash Flow ==</option>
                        @foreach($cashflows as $cashFlow)
                            <option value="{{ $cashFlow->idCashflow }}">{{ $cashFlow->kodeCashflow }}-{{ $cashFlow->namaCashflow }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="accountType">Account Type *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="accountType" name="accountType">
                        <option value="Asset">Asset</option>
                        <option value="Bank">Bank</option>
                        <option value="Expense">Expense</option>
                        <option value="Fixed Asset">Fixed Asset</option>
                        <option value="Liability">Liability</option>
                        <option value="Owners' equity">Owners' equity</option>
                        <option value="Revenue">Revenue</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="qualifiers">Qualifiers *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="qualifiers" name="qualifiers">
                        <option value="Asset">Asset</option>
                        <option value="Expense">Expense</option>
                        <option value="Liability">Liability</option>
                        <option value="Owners' equity">Owners' equity</option>
                        <option value="Revenue">Revenue</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="jenisCoa">Jenis Coa *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="jenisCoa" name="jenisCoa">
                        <option value="0">Konven & Umum</option>
                        <option value="1">SYARIAH</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="rekonBank">Rekon Bank *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="rekonBank" name="rekonBank">
                        <option value="0">COA Transaksi</option>
                        <option value="1">Bank Konven</option>
                        <option value="2">Bank Syariah</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(1, "lg"); // large
    sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>