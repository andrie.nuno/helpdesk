<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.masteruserpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idLevel">Jabatan *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idLevel" name="idLevel">
                        <option value="1" {{ $data->idLevel == 1 ? 'selected="selected"' : '' }}>ADMIN</option>
                        <option value="2" {{ $data->idLevel == 2 ? 'selected="selected"' : '' }}>PIC</option>
                    </select>
                    <input type="hidden" id="idUser" name="idUser" value="{{ $data->idUser }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaUser">Nama *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="namaUser" name="namaUser" value="{{ $data->namaUser }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="emailUser">Email *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="emailUser" name="emailUser" value="{{ $data->emailUser }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="password">Password **</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password" name="password" value="" autocomplete="off" required>
                </div>
                <label class="col-sm-12 col-form-label" for="password">**) Di isi jika ingin merubah password</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>NONAKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Simpan</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
