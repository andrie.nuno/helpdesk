<div class="table-responsive">
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Kategori</th>
                <th><center>Status</center></th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->namaKategori }}</td>
                <td><center>{{ $dita->isActive == 1 ? 'Aktif' : 'Nonaktif' }}</center></td>
                <td>
                    <center>
                        <a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idKategori }}', '{{ route('login.masterkategorimodal') }}')"><img src="{{ asset('assets/edit.svg') }}" alt=""></a>
                        @if($dita->isActive == 0)
                            <a onclick="doDelete('{{ $dita->idKategori }}', '{{ csrf_token() }}', '{{ route('login.masterkategorimodal') }}')"><img src="{{ asset('assets/trash.svg') }}" alt=""></a>
                        @endif
                    </center>
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
