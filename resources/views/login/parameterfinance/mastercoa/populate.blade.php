<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Coa</th>
                <th>Coa Oracle</th>
                <th>Deskripsi</th>
                <th><center>Status</center></th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->coa }}</td>
                <td>{{ $dita->coaOracle }}</td>
                <td>{{ $dita->description }}</td>
                <td><center>{{ $dita->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                <td align='center'><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idCoa }}', '{{ route('login.mastercoamodal') }}')"><span class="fa fa-fw fa-edit"></span></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
