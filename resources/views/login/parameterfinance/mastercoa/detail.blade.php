<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{ $data->namaProduk }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <div class="card">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Nama Produk :</b> {{ $data->namaProduk }}</li>
                <li class="list-group-item"><b>Title Produk :</b> {{ $data->titleProduk }}</li>
                <li class="list-group-item"><b>Image Produk :</b> <img src="{{ $data->imgProduk }}" width="400"></li>
                <li class="list-group-item"><b>Deskripsi Produk :</b> {{ $data->deskripsiProduk }}</li>
                <li class="list-group-item"><b>Status :</b> {{ $data->isActive == 1 ? 'Active' : 'Disabled' }}</li>
            </ul>
        </div>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
