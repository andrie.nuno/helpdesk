<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Detail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Bank</th>
                    <th>Nama Bank</th>
                    <th>No Rekening</th>
                    <th>Saldo Bank</th>
                    <th><center>Action</center></th>
                </tr>
            </thead>
            <tbody>
            @if($data)
                @php($nomor=1)
                @foreach($data as $dita)
                <tr>
                    <td>{{ $nomor++ }}</td>
                    <td>{{ $dita->kd_bank }}</td>
                    <td>{{ $dita->namaBank }}</td>
                    <td>{{ $dita->noRekening }}</td>
                    <td>Rp. {{ number_format($dita->saldoAkhir) }}</td>
                    {{-- <td align='center'><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idCabang }}', '{{ route('login.masterbankunitmodal') }}')"><span class="fa fa-fw fa-info-circle"></span></a></td> --}}
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
