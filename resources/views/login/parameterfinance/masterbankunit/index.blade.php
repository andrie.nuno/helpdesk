@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Master Bank Unit</h1>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.masterbankunitpopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="title_code">Kode Cabang</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="kode_cabang" name="kode_cabang" value="" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="title_code">Nama Cabang</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="nama_cabang" name="nama_cabang" value="" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="limit" id="limit" value="">
                                                <input type="hidden" name="page" id="page" value="">
                                                <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                            </div>
                                            {{-- <div class="col-md-2">
                                                <a onclick="showMultiModal(1, 'create', '{{ csrf_token() }}', '', '{{ route('login.masterbankunitmodal') }}')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour float-right"><i class="fa fa-plus"></i> Create Master Bank Unit</button></a>
                                            </div> --}}
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });
</script>
@endsection