<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Mutasi Bank</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="card card-primary card-outline">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-search" id="panel-searchPopulateMutasi">
                        <form action="{{ route('login.saldobankpopulatemutasi') }}" class="row" id="form-searchPopulateMutasi">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Cabang</label>
                                    <div class="col-sm-3">{{ $cabang->namaCabang }}</div>
                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                    <label class="col-sm-2 col-form-label">Nama Bank</label>
                                    <div class="col-sm-3">{{ $bank->namaBank }}</div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="tanggalAwal">Tanggal Awal</label>
                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" id="tanggalAwal" name="tanggalAwal" value="{{ $tanggal }}">
                                    </div>
                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                    <label class="col-sm-2 col-form-label" for="tanggalAkhir">Tanggal Akhir</label>
                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" id="tanggalAkhir" name="tanggalAkhir" value="{{ $tanggal }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group row">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="idCabang" id="idCabang" value="{{ $idCabang }}">
                                    <input type="hidden" name="idBank" id="idBank" value="{{ $idBank }}">
                                    <a onclick="doSearch('PopulateMutasi')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- ./card-header -->
        <div class="card-body">
            <div class="panel-data" id="panel-dataPopulateMutasi"></div>
        </div><!-- ./card-body -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(2)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(2, "lg"); // large
    sizeMultiModal(2, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(2);

    setTimeout(function(){
        doSearch('PopulateMutasi');
    }, 500);
});
