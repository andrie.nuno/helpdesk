<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Jurnal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" style="font-size: small">
        @if($data)
            @php($batch = '')
            @foreach($data as $dita)
                @if($batch != $dita->batch)
                    @if($batch != '')
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td align='right'><b>{{ number_format($sumDebet) }}</b></td>
                                            <td align='right'><b>{{ number_format($sumKredit) }}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </ul>
                    </div>
                    @endif
                    @php($sumDebet = 0)
                    @php($sumKredit = 0)
                    <div class="card">
                        <ul class="list-group">
                            <li class="list-group-item"><b>Nomor Referensi :</b> {{ $noReferensi }}</li>
                            <li class="list-group-item"><b>Jenis Jurnal :</b> {{ $dita->namaJenisJurnal }}</li>
                            <li class="list-group-item"><b>Batch :</b> {{ $dita->batch }}</li>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
                                    <thead>
                                        <tr>
                                            <th>Tanggal</th>
                                            <th>COA</th>
                                            <th>Keterangan</th>
                                            <th>Debet</th>
                                            <th>Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $dita->tanggal }}</td>
                                            <td>{{ $dita->coaCabang }}</td>
                                            <td>{{ $dita->keterangan }}</td>
                                            <td align='right'>{{ $dita->dk == 'D' ? number_format($dita->amount) : '0' }}</td>
                                            <td align='right'>{{ $dita->dk == 'K' ? number_format($dita->amount) : '0' }}</td>
                                        </tr>
                @else
                                        <tr>
                                            <td>{{ $dita->tanggal }}</td>
                                            <td>{{ $dita->coaCabang }}</td>
                                            <td>{{ $dita->keterangan }}</td>
                                            <td align='right'>{{ $dita->dk == 'D' ? number_format($dita->amount) : '0' }}</td>
                                            <td align='right'>{{ $dita->dk == 'K' ? number_format($dita->amount) : '0' }}</td>
                                        </tr>
                @endif
                @php($batch = $dita->batch)
                @if($dita->dk == 'D')
                    @php($sumDebet += $dita->amount)
                @else
                    @php($sumKredit += $dita->amount)
                @endif
            @endforeach
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td align='right'><b>{{ number_format($sumDebet) }}</b></td>
                                            <td align='right'><b>{{ number_format($sumKredit) }}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </ul>
                    </div>
        @endif
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(3)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(3, "lg"); // large
    // sizeMultiModal(3, "xl"); // extra large
    // sizeMultiModal(3, "sm"); // small
	centerMultiModal(3);
});
