<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Saldo Bank</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" style="font-size: small">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item"><b>Kode Cabang :</b> {{ $cabang->kodeCabang }}</li>
                <li class="list-group-item"><b>Nama Cabang :</b> {{ $cabang->namaCabang }}</li>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Kode Bank</th>
                                <th>Nama Bank</th>
                                <th>Saldo Awal</th>
                                <th>Saldo Masuk</th>
                                <th>Saldo Keluar</th>
                                <th>Saldo Akhir</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($bank)
                                @php($nomor=1)
                                @foreach($bank as $dita)
                                    <tr>
                                        <td>{{ $nomor++ }}</td>
                                        <td>{{ $dita->tanggal }}</td>
                                        <td>{{ $dita->kd_bank }}</td>
                                        <td><a onclick="showMultiModal(2, 'mutasi', '{{ csrf_token() }}', '{{ $dita->idCabang }}|{{ $dita->idBank }}|{{ $dita->tanggal }}', '{{ route('login.saldobankmodal') }}')"><span style="color:blue">{{ $dita->namaBank }}</span></a></td>
                                        <td align='right'>{{ number_format($dita->saldoAwal) }}</td>
                                        <td align='right'>{{ number_format($dita->saldoMasuk) }}</td>
                                        <td align='right'>{{ number_format($dita->saldoKeluar) }}</td>
                                        <td align='right'>{{ number_format($dita->saldoAkhir) }}</td>
                                        @if(Auth::user()->idLevel == 1)
                                            <td nowrap><a onclick="showMultiModal(2, 'editsaldo', '{{ csrf_token() }}', '{{ $dita->idCabang }}-{{ $dita->idBank }}', '{{ route('login.saldobankmodal') }}')"><span class="btn btn-primary btn-sm">Edit Saldo</span></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                    </table>
                </div>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(1, "lg"); // large
    sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
