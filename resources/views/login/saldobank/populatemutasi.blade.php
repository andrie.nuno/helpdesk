@if($error == '')
    @if($data)
        <div class="col-md-12">
            <span data-href="{{ route('login.saldobankexcel') }}?idCabang={{ $idCabang }}&idBank={{ $idBank }}&tanggalAwal={{ $tanggalAwal }}&tanggalAkhir={{ $tanggalAkhir }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span>
        </div>
    @endif
@endif
<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Keterangan</th>
                <th>Referensi</th>
                <th>Debet</th>
                <th>Kredit</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            @if($error == '')
                @if($data)
                    @php($tanggal = '')
                    @php($saldoAkhir = '')
                    @foreach($data as $dita)
                        @if($tanggal != $dita->tanggalSistem)
                            @if($tanggal != '')
                                <tr>
                                    <td>{{ $dita->tanggalSistem }}</td>
                                    <td>Saldo Akhir Tanggal {{ $tanggal }}</td>
                                    <td></td>
                                    <td align='right'></td>
                                    <td align='right'></td>
                                    <td align='right'>{{ number_format($saldoAkhir) }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td>{{ $dita->tanggalSistem }}</td>
                                <td>Saldo Awal Tanggal {{ $dita->tanggalSistem }}</td>
                                <td></td>
                                <td align='right'></td>
                                <td align='right'></td>
                                <td align='right'>{{ number_format($dita->saldoAwal) }}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>{{ $dita->tanggalSistem }}</td>
                            <td>{{ $dita->keterangan }}</td>
                            <td><a onclick="showMultiModal(3, 'jurnal', '{{ csrf_token() }}', '{{ $dita->kodeTrans }}', '{{ route('login.saldobankmodal') }}')"><span style="color:blue">{{ $dita->kodeTrans }}</span></a></td>
                            <td align='right'>{{ number_format($dita->debet) }}</td>
                            <td align='right'>{{ number_format($dita->kredit) }}</td>
                            <td align='right'>{{ number_format($dita->saldoAkhir) }}</td>
                        </tr>
                        @php($tanggal = $dita->tanggalSistem)
                        @php($saldoAkhir = $dita->saldoAkhir)
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">Data Tidak Ada</td>
                    </tr>
                @endif
            @else
                <tr>
                    <td colspan="6">{{ $error }}</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
