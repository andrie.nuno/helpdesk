<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Master Subjek</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.mastersubjekpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idKategori">Nama Kategori *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idKategori" name="idKategori">
                        <option value="">== Pilih Kategori ==</option>
                        @foreach($kategoris as $kategori)
                            <option value="{{ $kategori->idKategori }}" {{ $data->idKategori == $kategori->idKategori ? "selected":"" }}>{{ $kategori->namaKategori }}</option>
                        @endforeach
                    </select>
                <input type="hidden" id="idSubjek" name="idSubjek" value="{{ $data->idSubjek }}" readonly="readonly">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idTask">Nama Task *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idTask" name="idTask">
                        <option value="">== Pilih Task ==</option>
                        @foreach($tasks as $task)
                            <option value="{{ $task->idTask }}" {{ $data->idTask == $task->idTask ? "selected":"" }}>{{ $task->namaTask }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaSubjek">Nama Subjek *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="namaSubjek" name="namaSubjek" value="{{ $data->namaSubjek }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>NONAKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Simpan</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Tutup</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});

$("#idKategori").change(function() {
    var idKategori = $('select[name="idKategori"]').val();
    $("#idTask").html('');
    // alert(idKategori);
    $.ajax({
        type: "POST",
        url: "mastersubjekpopulateajax?type=task&_token={{ csrf_token() }}",
        data: {
            idKategori: idKategori
        },
        success: function (result) {
            $return = result;
            $('#idTask').append($return);
        }
    });
});
</script>
