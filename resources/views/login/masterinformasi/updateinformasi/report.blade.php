<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Report Konfirmasi User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="card card-primary card-outline">
        <div class="card-header">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-search" id="panel-searchPopulateReport">
                        <form action="{{ route('login.updateinformasipopulatereport') }}" class="row" id="form-searchPopulateReport">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="nama_wilayah">Nama Wilayah</label>
                                    <div class="col-sm-3">
                                    <select class="form-control" id="nama_wilayah" name="nama_wilayah">
                                        <option value="">== Pilih Wilayah ==</option>
                                        @foreach($wilayahs as $wilayah)
                                            <option value="{{ $wilayah->idWilayah }}">{{ $wilayah->namaWilayah }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <label class="col-sm-1 col-form-label"></label>
                                    <label class="col-sm-2 col-form-label" for="nama_cabang">Nama Cabang</label>
                                    <div class="col-sm-3">
                                    <select class="form-control" id="nama_cabang" name="nama_cabang">
                                        <option value="">== Pilih Cabang ==</option>
                                    </select>
                                    </div>
                                    <label class="col-sm-1 col-form-label"></label>
                                    <label class="col-sm-2 col-form-label" for="nama_unit">Nama Unit</label>
                                    <div class="col-sm-3">
                                    <select class="form-control" id="nama_unit" name="nama_unit">
                                        <option value="">== Pilih Unit ==</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group row">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="idUpdate" id="idUpdate" value="{{ $idUpdate }}">
                                    <a onclick="doSearch('PopulateReport')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- ./card-header -->
        <div class="card-body">
            <div class="panel-data" id="panel-dataPopulateReport"></div>
        </div><!-- ./card-body -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(2)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(2, "lg"); // large
    sizeMultiModal(2, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(2);

    setTimeout(function(){
        doSearch('PopulateReport');
    }, 500);
});

$("#nama_wilayah").change(function() {
        var idWilayah = $('select[name="nama_wilayah"]').val();
        $("#nama_cabang").html('');
        // alert(idWilayah);
        $.ajax({
            type: "POST",
            url: "updateinformasipopulateajax?type=cabang&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah
            },
            success: function (result1) {
                $return1 = result1;
                $('#nama_cabang').append($return1);
            }
        });
    });

$("#nama_cabang").change(function() {
    var idCabang = $('select[name="nama_cabang"]').val();
    $("#nama_unit").html('');
    // alert(idCabang);
    $.ajax({
        type: "POST",
        url: "updateinformasipopulateajax?type=unit&_token={{ csrf_token() }}",
        data: {
            idCabang: idCabang
        },
        success: function (result2) {
            $return2 = result2;
            $('#nama_unit').append($return2);
        }
    });
});