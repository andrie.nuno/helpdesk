<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{ $data->profilNama }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <div class="card">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>Nama :</b> {{ $data->profilNama }}</li>
                <li class="list-group-item"><b>Jabatan :</b> {{ $data->profilJabatan }}</li>
                <li class="list-group-item"><b>Foto :</b> <img style="width: 100px;" src="{{ $data->profilImage }}"></li>
                <li class="list-group-item"><b>Keterangan :</b> {{ $data->profilKeterangan }}</li>
                <li class="list-group-item"><b>Status :</b> {{ $data->isActive == 1 ? 'Active' : 'Disabled' }}</li>
            </ul>
        </div>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
