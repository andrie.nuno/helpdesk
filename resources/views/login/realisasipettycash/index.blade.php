@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.realisasipcpopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="idWilayah">Nama Wilayah</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="idWilayah" name="idWilayah">
                                                        <option value="">== Nama Wilayah ==</option>
                                                        @foreach($wilayah as $dita)
                                                            <option value="{{ $dita->idWilayah }}">{{ $dita->namaWilayah }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                                    <label class="col-sm-2 col-form-label" for="idCabang">Ho/Cabang/Unit</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="idCabang" name="idCabang">
                                                        <option value="">== Nama Ho/Cabang/Unit ==</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggalAwal">Tanggal Awal</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAwal" name="tanggalAwal" value="{{ date('Y-m-d') }}">
                                                    </div>
                                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                                    <label class="col-sm-2 col-form-label" for="tanggalAkhir">Tanggal Akhir</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggalAkhir" name="tanggalAkhir" value="{{ date('Y-m-d') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="statusJurnal">Status Jurnal</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="statusJurnal" name="statusJurnal">
                                                        <option value="">Semua</option>
                                                        <option value="0">Belum Create Jurnal</option>
                                                        <option value="1">Sudah Create Jurnal</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });

    $("#idWilayah").change(function() {
        var idWilayah = $('select[name="idWilayah"]').val();
        $("#idCabang").html('');
        // alert(idWilayah);
        $.ajax({
            type: "POST",
            url: "realisasipcpopulateajax?type=cabang&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah
            },
            success: function (result) {
                $return = result;
                $('#idCabang').append($return);
            }
        });
    });
</script>
@endsection
