<!-- resources/views/emails/password_reset_email.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        body{
            font-family: 'Poppins';
        }
        table.fixed-table
        {
            width:100%;
            table-layout:fixed;
        }

        /*Don't forget to set the width you want for each td */

        table.fixed-table td:nth-child(2),
        table.fixed-table td:nth-child(3)
        {
            width:30%;
        }
    </style>
</head>
<body>
<div class="">
    <div class="aHl"></div>
    <div id=":1zc" tabindex="-1"></div>
    <div id=":204" class="ii gt">
        <div id=":205" class="a3s aXjCH ">
            <u></u>
            <div style="margin:0;background:#fafafa">
                <table cellpadding="0" cellspacing="0" style="background:#fafafa" width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <div style="max-width:670px;margin:0 auto;padding:5% 0">
                                <table style="border:none;background:#ffffff" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>

                                    <tr>
                                        <td align="center" style="background-color:#fafafa"><img src="https://nos.wjv-1.neo.id/gadaimas/Logo%20Gadai%20MAS.png"  style="border: 0;width: 120px;height: auto;max-height: 77px;margin-top: 10px;margin-bottom: 30px;" class="CToWUd" alt="gadaimas">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding:23px 30px 10px 42px" valign="top"><span style="color:#333333;font-size:13px"> Dear Bpk/Ibu {{ $user->namaUser }} </span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding:3px 30px 10px 42px" valign="top"><span style="color:#777777;font-size:13px"> Anda telah melakukan permintaan pengaturan ulang password. Klik tombol dibawah ini untuk melakukan pembuatan ulang password Anda</span>
                                        </td>
                                    </tr>




                                    <tr>
                                        <td style="padding:5px 30px 8px 42px;text-align:center" valign="top">
                                            <a href="{{ $resetLink }}" style="background: linear-gradient(to bottom,#266AA2 1%,#266AA2 100%);;color:#ffffff;padding:10px 30px;text-decoration:none;display:block;text-align:center;border:1px solid #078bd2;font-size:16px;font-weight:bold;border-radius:4px;display:inline-block" target="_blank" data-saferedirecturl="">
                                                Buat Password Baru
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding:3px 30px 10px 42px" valign="top"><span style="color:#777777;font-size:13px">Jika Anda tidak membuat permintaan ini, harap abaikan email ini.</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding:0 30px 20px 42px" valign="top"><br>
                                            <span style="color:#777777;font-size:13px">
                                                Best Regards,
                                                <br> Tim

                                                <span class="il">Helpdesk Gadai MAS</span>
                                            </span>

                                        </td>
                                    </tr>
                                    <tr>

                                        <td style="padding:0 30px 0 42px" valign="top">
                                            <table cellpadding="0" cellspacing="0" width="100%" style="border-top:1px dotted #e1e2e3">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:15px 0">
																			<span style="color:#8a8a8a;font-size:10px"> Email Ini merupakan pesan surat otomatis, mohon jangan membalasnya secara langsung. Jika Anda memiliki pertanyaan
																					<a href="https://www.danain.co.id/kontak" style="color:#0574b5;text-decoration:none" target="_blank" data-saferedirecturl="https://www.danain.co.id/kontak">hubungi </a> kami.
																			</span>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                    </tbody>

                                </table>


                                <table cellpadding="0" cellspacing="0" width="100%" border="0" style="background:#ffffff;margin-top:1rem;" width="100%" class="fixed-table">
                                    <tbody>
                                    <tr>
                                        <td style="padding:10px 30px 10px 42px" valign="top" >
															<span style="color:#AAAAAA;font-size:10px;">Copyright ©2024 PT Maju Aman Sejahtera. All rights reserved.</span>
                                        </td>
                                        <td style="padding:10px 30px 10px 42px" valign="top">
                                            <img src="https://nos.jkt-1.neo.id/appsdanain-borrower/image-asset/googleplay.png"><br />
                                            <img src="https://nos.jkt-1.neo.id/appsdanain-borrower/image-asset/fb.png"><img src="https://nos.jkt-1.neo.id/appsdanain-borrower/image-asset/twitter.png"><img src="https://nos.jkt-1.neo.id/appsdanain-borrower/image-asset/instagram.png">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="yj6qo"></div>
            <div class="adL">
            </div>
        </div>
    </div>


    <div id=":1zo" class="ii gt" style="display:none">
        <div id=":1zp" class="a3s aXjCH undefined">
        </div>
    </div>
    <div class="hi">
    </div>

</div>
</body>
</html>
