<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Helpdesk | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <style>
    .login_oueter {
    width: 360px;
    max-width: 100%;
}
.logo_outer{
    text-align: center;
}
.logo_outer img{
    width:120px;
    margin-bottom: 40px;
}
   /* Custom styles for the left side */
.left-side {
  background-image: url('assets/LoginBg.svg');
  background-size: cover;
  color: white;
  padding: 20px;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}

/* Logo style */
.logo {
  font-size: 32px;
  color: #266AA2;
  margin-top: 20px;
}

/* Header title style */
.header-title {
  font-size: 24px;
  margin-bottom: 20px;
  color: #5A5A66;
}

/* Custom styles for the right side */
.right-side {
  padding: 20px;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
}

/* Adjustments for responsiveness */
@media (max-width: 768px) {
  .left-side, .right-side {
    height: auto;
  }
}
  </style>

</head>
<body class="hold-transition login-page">
  <div class="container-fluid">
    <div class="row">
      <!-- Left side with fancy gradient background -->
      <div class="col-md-5 left-side">
        <!-- You can add content here if needed -->
      </div>
      <!-- Right side with the login form -->
      <div class="col-md-7 right-side">
        <div class="login-box">
          <div class="login-logo">
            <h1>Login</h1>
          </div>
          <!-- /.login-logo -->
          <div class="card">
            <form action="{{ route('login') }}" method="post">
              @csrf
              <div class="card-body login-card-body">
                @if (Session::has('success'))
                <div class="alert alert-success">
                  {{ Session::get('success') }}
                </div>
                @endif
                @if (Session::has('error'))
                <div class="alert alert-danger">
                  {{ Session::get('error') }}
                </div>
                @endif
                @if (Session::has('errors'))
                    <div class="alert alert-danger">
                        {{ Session::get('errors')->first() }}
                    </div>
                @endif
                <div class="input-group-prepend mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                        <span class="fas fa-user"></span>
                        </div>
                    </div>
                    <input id="emailInput" name="email" type="email" class="form-control" placeholder="Email">
                </div>
                <div class="input-group-prepend mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-lock"></i></span>
                    </div>
                    <input id="passwordInput" name="password" type="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <span class="input-group-text" onclick="password_show_hide();">
                          <i class="fas fa-eye" id="show_eye"></i>
                          <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                        </span>
                      </div>
                </div>
                <div class="row">
                  <div class="col-8">
                    <div class="icheck-primary">
                      <input type="checkbox" id="remember">
                      <label for="remember">
                        Remember Me
                      </label>
                    </div>
                  </div>
                  <div class="col-4">
                    <button id="submitButton" type="submit" class="btn btn-secondary btn-block" disabled>Sign In</button>
                </div>
                  <!-- Forgot Password link -->
                    <div class="row mt-3">
                        <div class="col-12">
                        <a href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>
                    </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery -->
  <script src="assets/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="assets/dist/js/adminlte.min.js"></script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    const emailInput = document.getElementById('emailInput');
    const passwordInput = document.getElementById('passwordInput');
    const submitButton = document.getElementById('submitButton');

  // Function to check if both email and password fields are filled
  function checkInputs() {
    const emailValue = emailInput.value.trim();
    const passwordValue = passwordInput.value.trim();

    // If both fields have content, enable the submit button, otherwise disable it
    if (emailValue !== '' && passwordValue !== '') {
      submitButton.removeAttribute('disabled');
      submitButton.classList.remove('btn-secondary');
      submitButton.classList.add('btn-primary');
    } else {
      submitButton.setAttribute('disabled', 'disabled');
      submitButton.classList.remove('btn-primary');
      submitButton.classList.add('btn-secondary');
    }
  }

  // Call the checkInputs function whenever the user types in the email or password fields
  emailInput.addEventListener('input', checkInputs);
  passwordInput.addEventListener('input', checkInputs);

  function password_show_hide() {
  var x = document.getElementById("passwordInput");
  var show_eye = document.getElementById("show_eye");
  var hide_eye = document.getElementById("hide_eye");
  hide_eye.classList.remove("d-none");
  if (x.type === "password") {
    x.type = "text";
    show_eye.style.display = "none";
    hide_eye.style.display = "block";
  } else {
    x.type = "password";
    show_eye.style.display = "block";
    hide_eye.style.display = "none";
  }
}
  </script>
</body>
</html>
