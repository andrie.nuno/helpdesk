<!-- Main Footer -->
  {{-- <footer class="main-footer"> --}}
    <!-- To the right -->
    {{-- <div class="float-right d-none d-sm-inline"> --}}
      {{-- Anything you want --}}
    {{-- </div> --}}
    <!-- Default to the left -->
    {{-- <strong>Copyright &copy; 2024 Helpdesk.</strong> All rights reserved. --}}
  {{-- </footer> --}}

<footer class="main-footer text-center fixed-bottom">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                    {{-- Anything you want --}}
                </div>
                <!-- Default to the left -->
                <strong>2024 &copy; Helpdesk.</strong>
                All rights reserved.
</footer>
