<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-light navbar-white">

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" id="sidebarToggle" data-widget="pushmenu" href="#" role="button">
                    <i id="sidebarToggleIcon" class="bi bi-chevron-left"></i>
                </a>
            </li>
        </ul>

        @if (Request::segment(1) != "update-password")
        <div class="row mb-2">
            <div class="col-sm-12">
            <h5 class="m-0 text-bold text-dark">{{ helpers::getSegmenNameMenu(Request::segment(1)) }}</h5>
        </div>
        @else
        <div class="row mb-2">
            <div class="col-sm-12">
                <h5 class="m-0 text-dark"> Update Password </h5>
        </div>
        @endif


        {{-- @if (menuads::getNameBySegmenMenu(Request::segment(1)))
            {{ menuads::getNameBySegmenMenu() }}
        @endif --}}


      </div>

      <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownUser" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2">{{ Auth::user()->namaUser }}</span> <!-- Display user's name -->
                    <i class="bi bi-chevron-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownUser">
                    <a class="dropdown-item" href="{{ route('update.password.form') }}">Change Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                </div>
            </li>
        </ul>
  </nav>
  <!-- /.navbar -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script>
      $(document).ready(function () {
          $('#sidebarToggle').click(function () {
              $('#sidebarToggleIcon').toggleClass('bi-chevron-left bi-chevron-right');
          });
      });
  </script>
