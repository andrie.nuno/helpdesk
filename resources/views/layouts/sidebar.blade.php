<aside class="main-sidebar sidebar-light-primary elevation-1">
    {{-- <div class="brand-link logo-switch d-flex">
        <a href="{{ route("home") }}"><img style="width: 150px" src="{{ asset('assets/MasHelpdesk.png') }}" class="brand-image-xs logo-xl" alt="Logo"></a>
        <a href="{{ route("home") }}"><img style="width: 40px; left: 15px;" src="{{ asset('assets/LogoMas.svg') }}" class="brand-image-xl logo-xs" alt="Logo"></a>
    </div> --}}
    <div class="sidebar side">
        <div class="user-panel d-flex">
            <div class="brand-link logo-switch">
                <a href="{{ route("home") }}"><img style="width: 150px" src="{{ asset('assets/MasHelpdesk.png') }}" class="brand-image-xs logo-xl" alt="Logo"></a>
                <a href="{{ route("home") }}"><img style="width: 50px; left: 2%" src="{{ asset('assets/LogoMas.svg') }}" class="brand-image-xl logo-xs" alt="Logo"></a>
            </div>
        </div>

        {{--
            <div class="info">
                <a href="{{ route("home") }}"><img style="width: 150px" src="{{ asset('assets/MasHelpdesk.png') }}" alt="Logo"></a>
            </div>
                <!-- Left navbar links -->

        </div> --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
                @if(helpers::leftMenu())
                    @foreach(helpers::leftMenu() as $menu)
                    <li class="nav-item has-treeview {{ Request::segment(1) == "$menu->menuSegmen" ? "menu-open":"" }}">
                        <a href="{{ $menu->menuUrl != "#" ? route($menu->menuUrl):$menu->menuUrl }}" class="nav-link {{ Request::segment(1) == "$menu->menuSegmen" ? "active":"" }}">
                            @if (Request::segment(1) == $menu->menuSegmen)
                            <img class="fas fa-th menu-logo" style="width: 20px;" src="{{ $menu->menuLogoActive }}" alt="">
                            @elseif ($menu->menuLogo)
                            <img class="fas fa-th menu-logo" style="width: 20px; filter: grayscale(100%) invert(100%);" src="{{ $menu->menuLogo }}" alt="">
                            @else
                            <i class="nav-icon fas fa-th"></i>
                            @endif
                            <p class="menu-text">{{ $menu->menuNama }}</p>
                        </a>
                        @if($menu->sub)
                            @foreach($menu->sub as $sub)
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route($sub->menuUrl) }}" class="nav-link {{ Request::segment(2) == "$sub->menuSegmen" ? "active":"" }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <span>{{ $sub->menuNama }}</span>
                                    </a>
                                </li>
                            </ul>
                            @endforeach
                        @endif
                    </li>
                    @endforeach
                @endif
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <style>
    /* Custom CSS for active menu items */
    .nav-sidebar .nav-link.active .menu-text,
    .nav-sidebar .nav-link.active .menu-logo {
        color: #266AA2; /* Change the color of menu text and menu logo */
    }

    /* Remove default background color for active menu items */
    .nav-sidebar .nav-link.active {
        background-color: #ffffff !important; /* Remove background color */
        border: 3px !important; /* Remove border */
        box-shadow: none !important;
    }
    .nav-sidebar .nav-link.active:hover {
        background-color: rgba(159, 205, 255, 0.7) !important; /* Remove background color */
    }
</style>
