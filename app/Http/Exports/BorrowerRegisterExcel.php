<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\RegisterPeminjam;

class BorrowerRegisterExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $awal;
    protected $sampai;

    function __construct($jenis, $awal, $sampai) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->awal = $awal;
        $this->sampai = $sampai;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Detail") {
            $data = RegisterPeminjam::leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
                ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
                ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
                ->leftJoin('master_pendidikan', 'master_pendidikan.idPendidikan', '=', 'register_peminjam.pendidikanTerakhir')
                ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
                ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
                ->orderBy('register_peminjam.created_at')
                ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pendidikan.namaPendidikan, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana')
                ->where('register_peminjam.tanggal', '>=', $this->awal)
                ->where('register_peminjam.tanggal', '<=', $this->sampai)
                ->get();
        } else if ($this->jenis == "Tanggal") {
            $data = RegisterPeminjam::orderBy('created_at')
                ->selectRaw('tanggal AS tanggal, count(idBorrower) AS jumlah')
                ->where('tanggal', '>=', $this->awal)
                ->where('tanggal', '<=', $this->sampai)
                ->groupBy('tanggal')
                ->get();
        } else {
            $data = RegisterPeminjam::orderBy('created_at')
                ->selectRaw('YEAR(tanggal) tahun, MONTH(tanggal) bulan, count(idBorrower) AS jumlah')
                ->where('tanggal', '>=', $this->awal)
                ->where('tanggal', '<=', $this->sampai)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->get();
        }
        return view('login.laporanborrower.borrowerregister.excel',[
            "jenisLaporan"  => $this->jenis, 
            "tglAwal"       => $this->awal,
            "tglSampai"     => $this->sampai,
            "data"          => $data, 
        ]);
    }
}
