<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\master_user;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class WorkspaceExcel implements FromQuery, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class AzKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    protected $lender;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct() {
    }

    public function query()
    {
        $user = Auth::user();
        // $tanggal = $this->tanggal;
        $query = master_user::
        select("master_user.*")
        ->selectRaw("COUNT(task_problem.idTaskProblem) + COUNT(task_incident.idTaskIncident) + COUNT(task_pelaporan.idTaskPelaporan) AS totalTask")
        ->selectRaw("COUNT(CASE WHEN task_problem.status = 2 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_incident.status = 2 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_pelaporan.status = 2 THEN 1 ELSE NULL END) AS pendingTask")
        ->selectRaw("COUNT(CASE WHEN task_problem.status = 3 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_incident.status = 3 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_pelaporan.status = 3 THEN 1 ELSE NULL END) AS doneTask")
        ->selectRaw("COALESCE(
            TIME_FORMAT(
                SEC_TO_TIME(
                    (SUM(
                        CASE
                            WHEN task_problem.status = 3 THEN TIMESTAMPDIFF(SECOND, task_problem.startDate, task_problem.doneDate)
                            WHEN task_incident.status = 3 THEN TIMESTAMPDIFF(SECOND, task_incident.startDate, task_incident.doneDate)
                            WHEN task_pelaporan.status = 3 THEN TIMESTAMPDIFF(SECOND, task_pelaporan.startDate, task_pelaporan.doneDate)
                            ELSE 0
                        END
                    )) /
                    NULLIF(
                        COUNT(CASE WHEN task_problem.status = 3 THEN 1 ELSE NULL END) +
                        COUNT(CASE WHEN task_incident.status = 3 THEN 1 ELSE NULL END) +
                        COUNT(CASE WHEN task_pelaporan.status = 3 THEN 1 ELSE NULL END),
                        0
                    )
                ),
                '%H:%i:%s'
            ),
            '00:00:00'
        ) AS averageDuration")
        ->leftJoin("task_problem", "task_problem.idPic", "=", "master_user.idUser")
        ->leftJoin("task_incident", "task_incident.idPic", "=", "master_user.idUser")
        ->leftJoin("task_pelaporan", "task_pelaporan.idPic", "=", "master_user.idUser")
        ->where("master_user.isActive", 1);
        if ($user->idLevel == 2) {
            $query->where('master_user.idUser', '=', $user->idUser);
        }
        $query = $query->groupBy('master_user.idUser');
        // dd($query);
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            // [
            //     'Tanggal : '.$this->tanggal
            // ],
            [
                'No',
                'Nama PIC',
                'Total Task',
                'Task Pending',
                'Task Done',
                'Rerata Selesai',
            ]
        ];
    }

    public function map($data): array
    {
        $totaltask = $data->totalTask ?? 0;
        $pendingtask = $data->pendingTask ?? 0;
        $donetask = $data->doneTask ?? 0;

        $rowData = collect([
            $this->rownum++,
            $data->namaUser,
            (string)$totaltask,
            (string)$pendingtask,
            (string)$donetask,
            $data->averageDuration ?? 0,
        ]);

        return $rowData->toArray();
    }

}
