<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Auth;
use app\CustomClass\helpers;
use App\model\kasbankdetail;
use App\model\tbluserclient;

class TransaksiLenderExcel implements FromView, ShouldAutoSize
{
    protected $email_pendana;
    protected $tanggal;

    function __construct($email_pendana, $tanggal) {
        //dd($email_pendana);
        $this->email = $email_pendana;
        $this->tanggal = $tanggal;
    }
    
    public function view(): View
    {
        $dataUser = tbluserclient::where('userId', $this->email)->first();
        if ($dataUser) {
            $data = kasbankdetail::leftJoin('deb_pinjaman', 'deb_pinjaman.idDebPinjaman', '=', 'kasbank_detail.idDebPinjaman')
                ->leftJoin('mitra_gadai', 'mitra_gadai.noSbg','=', 'deb_pinjaman.noSbg')
                ->leftJoin('sell_trans', 'sell_trans.idDebPinjaman','=', 'deb_pinjaman.idDebPinjaman')
                ->leftJoin('tblproduk', 'tblproduk.idProduk','=', 'mitra_gadai.idProduk')
                ->selectRaw('kasbank_detail.id, kasbank_detail.kdTrans, kasbank_detail.tanggal, kasbank_detail.idDebPinjaman, kasbank_detail.refNo, kasbank_detail.keterangan, kasbank_detail.amount, deb_pinjaman.noSbg, deb_pinjaman.rate, mitra_gadai.nama, mitra_gadai.TujuanPinjaman, mitra_gadai.tenor, sell_trans.durasipengembalian, tblproduk.idProduk, tblproduk.namaProduk')
                ->where('kasbank_detail.idUserClient', '=', $dataUser->idUserClient)
                ->where('kasbank_detail.tanggal', '<=', $this->tanggal)
                ->whereIn('kasbank_detail.kdTrans', array('DEP','PNP','PRP','PBL','PDL'))
                ->orderBy('kasbank_detail.tanggal', 'ASC')
                ->get();
            $dataSum = kasbankdetail::join('tbltrans', 'tbltrans.kdTrans', '=', 'kasbank_detail.kdTrans')
                ->where('kasbank_detail.idUserClient', '=', $dataUser->idUserClient)
                ->where('kasbank_detail.tanggal', '<=', $this->tanggal)
                ->whereIn('kasbank_detail.kdTrans', array('DEP','PNP','PRP','PBL','PDL'))
                ->selectRaw('tbltrans.keterangan, kasbank_detail.kdTrans, SUM(kasbank_detail.amount) AS total')
                ->groupBy('kasbank_detail.kdTrans')
                ->orderByRaw('FIELD(kasbank_detail.kdTrans, "DEP", "PNP", "PRP", "PBL", "PDL")')
                ->get();
        } else {
            $data = "";
            $dataSum = "";
        }
        return view('login.laporanlender.transaksilender.excel',[
            "dataUser"      => $dataUser, 
            "data"          => $data, 
            "dataSum"       => $dataSum, 
            "tanggal"       => $this->tanggal, 
        ]);
    }
}
