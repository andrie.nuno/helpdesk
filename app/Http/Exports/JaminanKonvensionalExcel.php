<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_payctrl;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class JaminanKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class JaminanKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $query = trans_payctrl::
            join('trans_gadai','trans_gadai.idGadai','=','trans_payctrl.idGadai')
            ->join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
            ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
            ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
            ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
            ->join('tran_taksiran','tran_taksiran.idTaksiran','=','trans_gadai.idTaksiran')
            ->join('tran_taksirandetail','tran_taksirandetail.idTaksiran','=','trans_gadai.idTaksiran')
            ->join('tblbarangemas','tblbarangemas.idBarangEmas','=','tran_taksirandetail.idBarangEmas')
            ->selectRaw("tblproduk.kodeProduk, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, 
            tblcustomer.cif, tblcustomer.noKtp, tblcustomer.namaCustomer, trans_payctrl.tanggalPencairan, trans_gadai.lamaPinjaman, 
            tran_taksirandetail.ltv, tran_taksirandetail.hargaStle, trans_gadai.transKe, trans_gadai.tglJatuhTempo, trans_payctrl.ovd, 
            trans_payctrl.totalHutang, trans_gadai.rateFlat, trans_payctrl.pokokAwal, trans_payctrl.pokokSaldo, trans_gadai.biayaAdmin, '' AS kodeMitra, '' AS namaMitra, tran_taksirandetail.idTaksiran AS idTaksiran, 
            REPLACE(REPLACE(REPLACE(tran_taksirandetail.keterangan, CHAR(13), ' '), CHAR(10), ' '), '|', ' ') AS model, tran_taksirandetail.taksiran, 
            tran_taksirandetail.karat, tran_taksirandetail.beratBersih, tran_taksirandetail.jumlah, tblbarangemas.kodeBarangEmas, 
            tblbarangemas.namaBarangEmas")
            ->where('trans_payctrl.statusPinjaman', '!=', 'WO')
            ->where('trans_payctrl.tanggalPencairan', '<=', $tanggal)
            ->where('trans_payctrl.tanggalpelunasan', '>', $tanggal)
            ->orWhereNull('trans_payctrl.tanggalpelunasan')
            ->orderBy('tran_taksirandetail.idTaksiran')
            ->orderBy('tran_taksirandetail.idTaksiranDetail')
            // ->where(DB::raw("(trans_payctrl.tanggalpelunasan > $tanggal or trans_payctrl.tanggalpelunasan is null)"))
            ;
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query = $query->where('tblcabang.idCabang', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query = $query->where('tblcabang.headCabang', $this->nama_unit);
        }
        // $query = $query->groupBy('trans_payctrl.idGadai');
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tanggal
            ],
            [
                'No',
                'Kode Produk',
                'Kode Cabang',
                'Nama Cabang',
                'Nama Wilayah',
                'No SBG',
                'No CIF',
                'No KTP',
                'Nama Customer',
                'Tanggal Pencairan',
                'Lama Pinjaman',
                'LTV',
                'Harga Stle',
                'Trans Ke',
                'Tgl Jatuh Tempo',
                'Ovd',
                'Total Hutang',
                'Rate Fiat',
                'Pokok Awal',
                'Pokok Saldo',
                'Biaya Admin',
                'Kode Mitra',
                'Nama Mitra',
                'ID Taksiran',
                'Model',
                'Taksiran',
                'Karat',
                'Berat Bersih',
                'Jumlah',
                'Kode Barang Emas',
                'Nama Barang Emas',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->kodeProduk, 
            $data->kodeCabang, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->noSbg, 
            $data->cif, 
            "'".$data->noKtp, 
            $data->namaCustomer, 
            $data->tanggalPencairan, 
            $data->lamaPinjaman, 
            $data->ltv, 
            $data->hargaSle, 
            $data->transKe, 
            $data->tglJatuhTempo, 
            $data->ovd, 
            $data->totalHutang, 
            $data->rateFlat, 
            $data->pokokAwal, 
            $data->pokokSaldo, 
            $data->biayaAdmin, 
            $data->kodeMitra, 
            $data->namaMitra, 
            $data->idTaksiran, 
            $data->model, 
            $data->taksiran, 
            $data->karat, 
            $data->beratBersih, 
            $data->jumlah, 
            $data->kodeBarangEmas, 
            $data->namaBarangEmas,
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class JaminanKonvensionalExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah, 
//             "nama_cabang"   => $this->nama_cabang, 
//             "tanggal"       => $this->tanggal, 
//             "nama_unit"  => $this->nama_unit, 
//             "data"          => $data, 
//         ]);
//     }
// }
