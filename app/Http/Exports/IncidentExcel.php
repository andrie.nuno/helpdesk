<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\task_incident;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class IncidentExcel implements FromQuery, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class AzKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal_awal;
    protected $tanggal_akhir;
    protected $pic;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    protected $dash;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal_awal, $tanggal_akhir, $pic, $nama_wilayah, $nama_cabang, $nama_unit, $dash) {
        $this->tanggal_awal = $tanggal_awal;
        $this->tanggal_akhir = $tanggal_akhir;
        $this->pic = $pic;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
        $this->dash = $dash;
    }

    public function query()
    {
        $user = Auth::user();
        // $tanggal = $this->tanggal;
        $query = task_incident::orderBy('task_incident.idTaskIncident')->join('tblwilayah','tblwilayah.idWilayah','=','task_incident.idWilayah')
        ->join('tblcabang','tblcabang.idCabang','=','task_incident.idCabang')
        ->join("tblcabang AS unit", function($join){
            $join->on('unit.idCabang', '=', 'task_incident.idUnit');
            $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
        })
        ->join('master_task','master_task.idTask','=','task_incident.idTask')
        ->join('master_subjek','master_subjek.idSubjek','=','task_incident.idSubjek')
        ->join('master_sub_subjek','master_sub_subjek.idSubSubjek','=','task_incident.idSubSubjek')
        ->join('master_user','master_user.idUser','=','task_incident.idPic');
        if ($user->idLevel == 2) {
            $query->where('master_user.idUser', '=', $user->idUser);
        } elseif ($this->pic) {
            $query->where('task_incident.idPic', '=', $this->pic);
        }
        if ($this->nama_wilayah) {
            $query->where('task_incident.idWilayah', '=', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query->where('task_incident.idCabang', '=', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query->where('task_incident.idUnit', '=', $this->nama_unit);
        }
        if ($this->tanggal_awal) {
            $query->where('task_incident.startDate', '>=', $this->tanggal_awal);
        }
        if ($this->tanggal_akhir) {
            $query->where('task_incident.dueDate', '<=', $this->tanggal_akhir);
        }
        $query->selectRaw('task_incident.*, tblwilayah.namaWilayah, tblcabang.namaCabang, unit.namaCabang as namaUnit, master_task.namaTask, master_subjek.namaSubjek, master_sub_subjek.namaSubSubjek, master_user.namaUser, CASE
        WHEN task_incident.dueDate < NOW() THEN DATEDIFF(NOW(), task_incident.dueDate)
        ELSE NULL
    END AS overdueDays');
    if ($this->dash == 1){
        $query->whereRaw('CASE
        WHEN task_incident.dueDate < NOW() THEN DATEDIFF(NOW(), task_incident.dueDate)
        ELSE NULL
    END > 0')
        ->where('task_incident.status', '!=', 3);
    }
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            // [
            //     'Tanggal : '.$this->tanggal
            // ],
            [
                'No',
                'Task',
                'Subjek',
                'Unit',
                'PIC',
                'Due Date',
                'Status',
                'Overdue',
            ]
        ];
    }

    public function map($data): array
    {
        if ($data->overdueDays) {
            $overdue = $data->overdueDays;
        } else {
            $overdue = " - ";
        }
        return [
            $this->rownum++,
            $data->namaTask,
            $data->namaSubjek,
            $data->namaUnit,
            $data->namaUser,
            $data->dueDate,
            $data->status == 1 ? 'Open' : ($data->status == 2 ? 'Pending' : 'Done'),
            $overdue,

        ];
    }
}
