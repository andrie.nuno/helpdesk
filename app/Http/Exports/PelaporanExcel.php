<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\task_pelaporan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class PelaporanExcel implements FromQuery, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class AzKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal_awal;
    protected $tanggal_akhir;
    protected $pic;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    protected $dash;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal_awal, $tanggal_akhir, $pic, $dash) {
        $this->tanggal_awal = $tanggal_awal;
        $this->tanggal_akhir = $tanggal_akhir;
        $this->pic = $pic;
        $this->dash = $dash;
    }

    public function query()
    {
        $user = Auth::user();
        // $tanggal = $this->tanggal;
        $query = task_pelaporan::orderBy('task_pelaporan.idTaskPelaporan')->join('master_task','master_task.idTask','=','task_pelaporan.idTask')
        ->join('master_subjek','master_subjek.idSubjek','=','task_pelaporan.idSubjek')
        ->join('master_user','master_user.idUser','=','task_pelaporan.idPic');
        if ($user->idLevel == 2) {
            $query->where('master_user.idUser', '=', $user->idUser);
        } elseif ($this->pic) {
            $query->where('task_pelaporan.idPic', '=', $this->pic);
        }
        if ($this->tanggal_awal) {
            $query->where('task_pelaporan.startDate', '>=', $this->tanggal_awal);
        }
        if ($this->tanggal_akhir) {
            $query->where('task_pelaporan.dueDate', '<=', $this->tanggal_akhir);
        }
        $query->selectRaw('task_pelaporan.*, master_task.namaTask, master_subjek.namaSubjek, master_user.namaUser, CASE
        WHEN task_pelaporan.dueDate < NOW() THEN DATEDIFF(NOW(), task_pelaporan.dueDate)
        ELSE NULL
    END AS overdueDays');
    if ($this->dash){
        $query->whereRaw('CASE WHEN task_pelaporan.dueDate < NOW() THEN DATEDIFF(NOW(), task_pelaporan.dueDate) ELSE NULL END > 0')
        ->where('task_pelaporan.status', '!=', 3);
    }
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            // [
            //     'Tanggal : '.$this->tanggal
            // ],
            [
                'No',
                'Task',
                'Subjek',
                'PIC',
                'Due Date',
                'Status',
                'Overdue',
            ]
        ];
    }

    public function map($data): array
    {
        if ($data->overdueDays) {
            $overdue = $data->overdueDays;
        } else {
            $overdue = " - ";
        }
        return [
            $this->rownum++,
            $data->namaTask,
            $data->namaSubjek,
            $data->namaUser,
            $data->dueDate,
            $data->status == 1 ? 'Open' : ($data->status == 2 ? 'Pending' : 'Done'),
            $overdue,
        ];
    }
}
