<?php

namespace App\Http\Exports;

use App\model\task_problem;
use App\model\task_incident;
use App\model\task_pelaporan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Support\Facades\Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class TaskExcel implements FromCollection, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class AzKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal_awal;
    protected $tanggal_akhir;
    protected $pic;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    protected $nama_task;
    protected $dash;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal_awal, $tanggal_akhir, $pic, $nama_wilayah, $nama_cabang, $nama_unit, $nama_task, $dash) {
        $this->tanggal_awal = $tanggal_awal;
        $this->tanggal_akhir = $tanggal_akhir;
        $this->pic = $pic;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
        $this->nama_task = $nama_task;
        $this->dash = $dash;
    }

    public function collection()
    {
        $user = Auth::user();

        // Combine the results into one collection
        $combinedData = new Collection();

        // Perform the first query
        $data1 = task_problem::leftJoin('tblwilayah', 'tblwilayah.idWilayah', '=', 'task_problem.idWilayah')
            ->leftJoin('tblcabang', 'tblcabang.idCabang', '=', 'task_problem.idCabang')
            ->leftJoin("tblcabang AS unit", function ($join) {
                $join->on('unit.idCabang', '=', 'task_problem.idUnit');
                $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
            })
            ->leftJoin('master_task', 'master_task.idTask', '=', 'task_problem.idTask')
            ->leftJoin('master_subjek', 'master_subjek.idSubjek', '=', 'task_problem.idSubjek')
            ->leftJoin('master_sub_subjek', 'master_sub_subjek.idSubSubjek', '=', 'task_problem.idSubSubjek')
            ->leftJoin('master_user', 'master_user.idUser', '=', 'task_problem.idPic')
            ->selectRaw('task_problem.*, tblwilayah.namaWilayah, tblcabang.namaCabang, unit.namaCabang as namaUnit, master_task.namaTask, master_subjek.namaSubjek, master_sub_subjek.namaSubSubjek, master_user.namaUser, CASE WHEN task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_problem.dueDate - INTERVAL 24 HOUR) / 24) ELSE NULL END AS overdueDays');
            if ($user->idLevel == 2) {
                $data1->where('master_user.idUser', '=', $user->idUser);
            } elseif ($this->pic) {
                $data1->where('task_problem.idPic', '=', $this->pic);
            }
            if ($this->nama_wilayah) {
                $data1->where('task_problem.idWilayah', '=', $this->nama_wilayah);
            }
            if ($this->nama_cabang) {
                $data1->where('task_problem.idCabang', '=', $this->nama_cabang);
            }
            if ($this->nama_unit) {
                $data1->where('task_problem.idUnit', '=', $this->nama_unit);
            }
            if ($this->tanggal_awal) {
                $data1->where('task_problem.startDate', '>=', $this->tanggal_awal);
            }
            if ($this->tanggal_akhir) {
                $data1->where('task_problem.dueDate', '<=', $this->tanggal_akhir);
            }
            if ($this->nama_task) {
                $data1->where('master_task.namaTask', 'like', '%'.$this->nama_task.'%');
            }
            if ($this->dash == 1){
                $data1->whereRaw('CASE WHEN task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_problem.dueDate) ELSE NULL END > 0')
                ->where('task_problem.status', '!=', 3);
            }
            $data1 = $data1->get();

        // Perform the second query
        $data2 = task_incident::leftJoin('tblwilayah', 'tblwilayah.idWilayah', '=', 'task_incident.idWilayah')
            ->leftJoin('tblcabang', 'tblcabang.idCabang', '=', 'task_incident.idCabang')
            ->leftJoin("tblcabang AS unit", function ($join) {
                $join->on('unit.idCabang', '=', 'task_incident.idUnit');
                $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
            })
            ->leftJoin('master_task', 'master_task.idTask', '=', 'task_incident.idTask')
            ->leftJoin('master_subjek', 'master_subjek.idSubjek', '=', 'task_incident.idSubjek')
            ->leftJoin('master_sub_subjek', 'master_sub_subjek.idSubSubjek', '=', 'task_incident.idSubSubjek')
            ->leftJoin('master_user', 'master_user.idUser', '=', 'task_incident.idPic')
            ->selectRaw('task_incident.*, tblwilayah.namaWilayah, tblcabang.namaCabang, unit.namaCabang as namaUnit, master_task.namaTask, master_subjek.namaSubjek, master_sub_subjek.namaSubSubjek, master_user.namaUser, CASE WHEN task_incident.dueDate < NOW() + INTERVAL 7 HOUR THEN CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_incident.dueDate - INTERVAL 24 HOUR) / 24) ELSE NULL END AS overdueDays');
            if ($user->idLevel == 2) {
                $data2->where('master_user.idUser', '=', $user->idUser);
            } elseif ($this->pic) {
                $data2->where('task_incident.idPic', '=', $this->pic);
            }
            if ($this->nama_wilayah) {
                $data2->where('task_incident.idWilayah', '=', $this->nama_wilayah);
            }
            if ($this->nama_cabang) {
                $data2->where('task_incident.idCabang', '=', $this->nama_cabang);
            }
            if ($this->nama_unit) {
                $data2->where('task_incident.idUnit', '=', $this->nama_unit);
            }
            if ($this->tanggal_awal) {
                $data2->where('task_incident.startDate', '>=', $this->tanggal_awal);
            }
            if ($this->tanggal_akhir) {
                $data2->where('task_incident.dueDate', '<=', $this->tanggal_akhir);
            }
            if ($this->nama_task) {
                $data2->where('master_task.namaTask', 'like', '%'.$this->nama_task.'%');
            }
            if ($this->dash){
                $data2->whereRaw('CASE WHEN task_incident.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_incident.dueDate) ELSE NULL END > 0')
                ->where('task_incident.status', '!=', 3);
            }
            $data2 = $data2->get();

        // Perform the third query
        $data3 = task_pelaporan::leftJoin('master_task', 'master_task.idTask', '=', 'task_pelaporan.idTask')
        ->leftJoin('master_subjek', 'master_subjek.idSubjek', '=', 'task_pelaporan.idSubjek')
        ->leftJoin('master_user', 'master_user.idUser', '=', 'task_pelaporan.idPic')
        ->selectRaw('task_pelaporan.*, master_task.namaTask, master_subjek.namaSubjek, master_user.namaUser, CASE WHEN task_pelaporan.dueDate < NOW() + INTERVAL 7 HOUR THEN CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_pelaporan.dueDate - INTERVAL 24 HOUR) / 24) ELSE NULL END AS overdueDays');
        if ($user->idLevel == 2) {
            $data3->where('master_user.idUser', '=', $user->idUser);
        } elseif ($this->pic) {
            $data3->where('task_pelaporan.idPic', '=', $this->pic);
        }
        if ($this->tanggal_awal) {
            $data3->where('task_pelaporan.startDate', '>=', $this->tanggal_awal);
        }
        if ($this->tanggal_akhir) {
            $data3->where('task_pelaporan.dueDate', '<=', $this->tanggal_akhir);
        }
        if ($this->nama_task) {
            $data3->where('master_task.namaTask', 'like', '%'.$this->nama_task.'%');
        }
        if ($this->dash){
            $data3->whereRaw('CASE WHEN task_pelaporan.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_pelaporan.dueDate) ELSE NULL END > 0')
            ->where('task_pelaporan.status', '!=', 3);
        }
        $data3 = $data3->get();

        // Concatenate the data into one collection
        $combinedData = $combinedData->concat($data1)->concat($data2)->concat($data3);
        // dd($combinedData);
        return $combinedData;
    }


    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->collection()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            // [
            //     'Tanggal : '.$this->tanggal
            // ],
            [
                'No',
                'Kategori',
                'Task',
                'Subjek',
                'Unit',
                'PIC',
                'Due Date',
                'Status',
                'Overdue',
            ]
        ];
    }

    public function map($data): array
    {
        // dd($data);
        if ($data instanceof task_problem) {
            $kategori = 'Problem';
        } elseif ($data instanceof task_incident) {
            $kategori = 'Incident';
        } elseif ($data instanceof task_pelaporan) {
            $kategori = 'Pelaporan';
        } else {
            $kategori = 'Unknown'; // Set default value if needed
        }

        if ($data->overdueDays) {
            $overdue = abs($data->overdueDays);
        } else {
            $overdue = " - ";
        }
        return [
            $this->rownum++,
            $kategori,
            $data->namaTask ?? "kosong",
            $data->namaSubjek ?? "kosong",
            $data->namaUnit ?? "kosong",
            $data->namaUser ?? "kosong",
            $data->dueDate ?? "kosong",
            $data->status == 1 ? 'Open' : ($data->status == 2 ? 'Pending' : 'Done'),
            $overdue . " Hari",

        ];
    }
}
