<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Illuminate\Support\Facades\Auth;

class TwoFactorAuthController extends Controller
{
    public function showSetupForm()
    {
        return view('auth.setup-2fa');
    }

    public function enableTwoFactorAuth(Request $request)
    {
        $user = Auth::user();

        $authenticator = app(Authenticator::class)->boot($user);

        $secret = $authenticator->generateSecretKey();

        // Save the secret key to the user's record in the database
        $user->google2fa_secret = $secret;
        $user->save();

        // Generate QR code for the user to scan with their authenticator app
        $qrCode = $authenticator->getQRCodeInline(
            config('app.name'),
            $user->email,
            $secret
        );

        return view('auth.show-qr-code', compact('qrCode', 'secret'));
    }
}
