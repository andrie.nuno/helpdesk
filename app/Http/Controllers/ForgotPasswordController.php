<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Services\SendGridMail; // Import the SendGridMail service
use DB;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    protected $sendGridMail;

    public function __construct(SendGridMail $sendGridMail)
    {
        $this->sendGridMail = $sendGridMail;
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $user = User::where('emailUser', $request->email)->first();

        if (!$user) {
            return back()->withErrors([
                'email' => 'Email tidak terdaftar.'
            ]);
        }

        // Generate the password reset token
        $token = $this->broker()->createToken($user);
        // Store the password reset token in the database
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => now()
        ]);

        // Compose email content using the provided HTML template
        $emailContent = view('emails.password_reset_email', [
            'user' => $user,
            'resetLink' => url('/password/reset/' . $token)
        ])->render();

        // Send the password reset email using SendGridMail service
        try {
            $this->sendGridMail->send($user->emailUser, 'Password Reset Helpdesk', $emailContent);
            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            // Log the error or handle it accordingly
            \Log::error('Error sending password reset email: ' . $e->getMessage());
            return back()->withErrors([
                'email' => 'Error sending password reset email. Please try again later.'
            ]);
        }
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }
}
