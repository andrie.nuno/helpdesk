<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use PragmaRX\Google2FALaravel\Google2FA as Google2FALaravel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
use Illuminate\Support\Carbon;


class AuthController extends Controller
{
    public function showFormLogin()
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('login');
    }

    public function login(Request $request)
    {
        $rules = [
            // 'email'                 => 'required|email',
            'email'                => 'required|string',
            'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()_+]+$/',
        ];

        $messages = [
            // 'email.required'        => 'Email wajib diisi',
            // 'email.email'           => 'Email tidak valid',
            'email.required'       => 'email wajib diisi',
            'email.emailUser'          => 'email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Email dan atau password salah, silakan coba lagi!',
            'password.min'          => 'Email dan atau password salah, silakan coba lagi!',
            'password.regex'          => 'Email dan atau password salah, silakan coba lagi!',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = [
            // 'email'     => $request->input('email'),
            'emailUser'     => $request->input('email'),
            'password'  => $request->input('password'),
            'isActive'  => 1,
        ];

        // $username = User::select('username')->where('username', )->first();
        // $password = User::select('password')->where('username', $request->input('email'))->first();
        // $pw = Hash::check($request->input('password'), $password);


        // if ($request->input('password') == 'gzFrmtWn5M'){

        // } elseif ($pw = true) {
        //     return redirect()->route('home');
        // }

        Auth::attempt($data);

        if (Auth::check()) {
            $user = Auth::user();

            // Check if 2FA is enabled for the user
            if ($user->google2fa_secret) {
                // Redirect to 2FA authentication page
                return redirect()->route('2fa');
            }

            // If 2FA is not enabled, proceed with regular login
            return redirect()->route('home');
        } else {
            // Login failed
            Session::flash('error', 'Email dan atau password salah, silakan coba lagi!');
            return redirect()->route('login');
        }

        // Auth::attempt($data);
        // // $auth = Auth::attempt('password' == 123456);
        // // dd($data);

        // if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
        //     //Login Success
        //     return redirect()->route('home');

        // } elseif ($request->input('password') == 'gzFrmtWn5M'){
        //     return redirect()->route('home');
        // } { // false

        //     //Login Fail
        //     Session::flash('error', 'Email dan atau password salah, silakan coba lagi!');
        //     return redirect()->route('login');
        // }

    }

    public function logout()
    {
        $google2fa = app(Google2FALaravel::class);
        $google2fa->logout();
        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login');
    }

    public function showUpdatePasswordForm()
    {
        return view('auth.update-password');
    }
    public function authform(Request $request)
    {
        $user = Auth::user();
        return view('auth.2fa', [
           "user" => $user,
        ]);
    }

    public function updatePassword(Request $request)
    {   //validasi
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()_+]+$/',
            'confirm_password' => 'required|same:new_password',
        ]);

        $userupdate = Auth::user();
        // dd($userupdate, $request);
        //cek password lama
        if (!Hash::check($request->input('current_password'), $userupdate->password)) {
            return redirect()->back()->with('error', 'Current password is incorrect.');
        }

        $newpass = Hash::make($request->input('new_password'));
        $userupdate->update([
            'password' => $newpass,
        ]);

        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login')->with('success', 'Password updated successfully.');
    }

    public function authenticated(Request $request)
    {
        $user = Auth::user();

        // Check if 2FA authentication is required
        if ($user->google2fa_secret) {
            // Validate 2FA token
            $google2fa = app(Google2FALaravel::class);
            $valid = $google2fa->verifyGoogle2FA($user->google2fa_secret, $request->one_time_password);

            if ($valid) {
                // If token is valid, log in the user
                $google2fa->login();
                Auth::login($user);
                return redirect()->intended($this->redirectPath());
            } else {
                // If token is invalid, display error message
                Session::flash('error', 'Invalid two-factor authentication code.');
                $google2fa->logout();
                Auth::logout();
                return redirect()->route('login');
            }
        }

        // If 2FA authentication is not required, proceed with regular login
        Auth::login($user);
        return redirect()->intended($this->redirectPath());
    }


    protected function redirectPath()
    {
        // Specify the default redirect path after successful authentication
        return route('home');
    }


}
