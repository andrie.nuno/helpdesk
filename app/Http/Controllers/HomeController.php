<?php

namespace App\Http\Controllers;

use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\master_user;
use App\model\task_problem;
use App\model\task_incident;
use Illuminate\Http\Request;
use App\model\task_pelaporan;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\WorkspaceExcel;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    // protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        // dd($user);

        $wilayah = tblwilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        $pic = master_user::where('isActive', '=', 1)->get();

        $pending = 0;
        $open = 0;
        $done = 0;
        $total = 0;

        if ($user->idLevel == 2) {
            $pendingproblem = task_problem::where('status', 2)->where('idPic', $user->idUser)->count();
            $openproblem = task_problem::where('status', 1)->where('idPic', $user->idUser)->count();
            $doneproblem = task_problem::where('status', 3)->where('idPic', $user->idUser)->count();
            $totalproblem = task_problem::where('idPic', $user->idUser)->count();

            $pendingincident = task_incident::where('status', 2)->where('idPic', $user->idUser)->count();
            $openincident = task_incident::where('status', 1)->where('idPic', $user->idUser)->count();
            $doneincident = task_incident::where('status', 3)->where('idPic', $user->idUser)->count();
            $totalincident = task_incident::where('idPic', $user->idUser)->count();

            $pendingpelaporan = task_pelaporan::where('status', 2)->where('idPic', $user->idUser)->count();
            $openpelaporan = task_pelaporan::where('status', 1)->where('idPic', $user->idUser)->count();
            $donepelaporan = task_pelaporan::where('status', 3)->where('idPic', $user->idUser)->count();
            $totalpelaporan = task_pelaporan::where('idPic', $user->idUser)->count();
        } else {
            $pendingproblem = task_problem::where('status', 2)->count();
            $openproblem = task_problem::where('status', 1)->count();
            $doneproblem = task_problem::where('status', 3)->count();
            $totalproblem = task_problem::count();

            $pendingincident = task_incident::where('status', 2)->count();
            $openincident = task_incident::where('status', 1)->count();
            $doneincident = task_incident::where('status', 3)->count();
            $totalincident = task_incident::count();

            $pendingpelaporan = task_pelaporan::where('status', 2)->count();
            $openpelaporan = task_pelaporan::where('status', 1)->count();
            $donepelaporan = task_pelaporan::where('status', 3)->count();
            $totalpelaporan = task_pelaporan::count();
        }

        $pending += $pendingproblem;
        $open += $openproblem;
        $done += $doneproblem;
        $total += $totalproblem;

        $pending += $pendingincident;
        $open += $openincident;
        $done += $doneincident;
        $total += $totalincident;

        $pending += $pendingpelaporan;
        $open += $openpelaporan;
        $done += $donepelaporan;
        $total += $totalpelaporan;

        $pics = master_user::select("master_user.*")
        ->selectRaw("COUNT(task_problem.idTaskProblem) + COUNT(task_incident.idTaskIncident) + COUNT(task_pelaporan.idTaskPelaporan) AS totalTask")
        ->selectRaw("COUNT(CASE WHEN task_problem.status = 2 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_incident.status = 2 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_pelaporan.status = 2 THEN 1 ELSE NULL END) AS pendingTask")
        ->selectRaw("COUNT(CASE WHEN task_problem.status = 3 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_incident.status = 3 THEN 1 ELSE NULL END) + COUNT(CASE WHEN task_pelaporan.status = 3 THEN 1 ELSE NULL END) AS doneTask")
        ->selectRaw("COALESCE(
            TIME_FORMAT(
                SEC_TO_TIME(
                    (SUM(
                        CASE
                            WHEN task_problem.status = 3 THEN TIMESTAMPDIFF(SECOND, task_problem.startDate, task_problem.doneDate)
                            WHEN task_incident.status = 3 THEN TIMESTAMPDIFF(SECOND, task_incident.startDate, task_incident.doneDate)
                            WHEN task_pelaporan.status = 3 THEN TIMESTAMPDIFF(SECOND, task_pelaporan.startDate, task_pelaporan.doneDate)
                            ELSE 0
                        END
                    )) /
                    NULLIF(
                        COUNT(CASE WHEN task_problem.status = 3 THEN 1 ELSE NULL END) +
                        COUNT(CASE WHEN task_incident.status = 3 THEN 1 ELSE NULL END) +
                        COUNT(CASE WHEN task_pelaporan.status = 3 THEN 1 ELSE NULL END),
                        0
                    )
                ),
                '%H:%i:%s'
            ),
            '00:00:00'
        ) AS averageDuration")
        ->leftJoin("task_problem", "task_problem.idPic", "=", "master_user.idUser")
        ->leftJoin("task_incident", "task_incident.idPic", "=", "master_user.idUser")
        ->leftJoin("task_pelaporan", "task_pelaporan.idPic", "=", "master_user.idUser")
        ->where("master_user.isActive", 1);
        if ($user->idLevel == 2) {
            $pics->where('master_user.idUser', $user->idUser);
        }
        $pics->groupBy("master_user.idUser");
        // dd($pics->toSql());
        $pics = $pics->get();




        $dash = 1;
        return view('home', [
            "wilayahs"       => $wilayah,
            "pics"       => $pics,
            "pending" => $pending,
            "open" => $open,
            "done" => $done,
            "total" => $total,
            "pic" => $pic,
            "dash" => $dash,
        ]);
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
				$cabang = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('idWilayah', $idWilayah)
                    ->where('idJenisCabang', 4)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('kodeCabang')
                    ->get();
				$result1 = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabang) {
					foreach ($cabang as $branch) {
						$result1 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
					}
				}
				echo $result1;
				break;
            case 'unit':
                $idCabang = $request->idCabang;
                $branchs = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('headCabang', $idCabang)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('kodeCabang')->get();
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($branchs) {
                    foreach ($branchs as $branch) {
                        $result2 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
		}
	}

    public function populate(Request $request) {
        $user = Auth::user();
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = task_problem::orderBy('task_problem.idTaskProblem');
        $data = $query
        ->leftJoin('tblwilayah','tblwilayah.idWilayah','=','task_problem.idWilayah')
        ->leftJoin('tblcabang','tblcabang.idCabang','=','task_problem.idCabang')
        ->leftJoin("tblcabang AS unit", function($join){
            $join->on('unit.idCabang', '=', 'task_problem.idUnit');
            $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
        })
        ->leftJoin('master_task','master_task.idTask','=','task_problem.idTask')
        ->leftJoin('master_subjek','master_subjek.idSubjek','=','task_problem.idSubjek')
        ->leftJoin('master_sub_subjek','master_sub_subjek.idSubSubjek','=','task_problem.idSubSubjek')
        ->leftJoin('master_user','master_user.idUser','=','task_problem.idPic');
            if ($request->id_pic) {
                $data = $query->where('task_problem.idPic', '=', $request->id_pic);
            }
            if ($request->nama_wilayah) {
                $data = $query->where('task_problem.idWilayah', '=', $request->nama_wilayah);
            }
            if ($request->nama_cabang) {
                $data = $query->where('task_problem.idCabang', '=', $request->nama_cabang);
            }
            if ($request->nama_unit) {
                $data = $query->where('task_problem.idUnit', '=', $request->nama_unit);
            }
            if ($request->tanggalAwal) {
                $query = $query->where('task_problem.startDate', '>=', $request->tanggalAwal);
            }
            if ($request->tanggalAkhir) {
                $query = $query->where('task_problem.dueDate', '<=', $request->tanggalAkhir);
            }
            if ($request->nama_task) {
                $data = $query->where('master_task.namaTask', 'like', '%'.$request->nama_task.'%');
            }
            if ($user->idLevel == 2) {
                $data = $query->where('master_user.idUser', '=', $user->idUser);
            }

            $data = $query->selectRaw('task_problem.*,
            tblwilayah.namaWilayah,
            tblcabang.namaCabang,
            unit.namaCabang as namaUnit,
            master_task.namaTask,
            master_subjek.namaSubjek,
            master_sub_subjek.namaSubSubjek,
            master_user.namaUser,
            CASE
                WHEN task_problem.status != 3 AND task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN
                    CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_problem.dueDate - INTERVAL 24 HOUR) / 24)
                WHEN task_problem.status = 3 AND task_problem.doneDate > task_problem.dueDate THEN
                    CEIL(TIMESTAMPDIFF(HOUR, task_problem.dueDate, task_problem.doneDate) / 24)
                ELSE NULL
            END AS overdueDays');
            if ($request->dash){
                $query->whereRaw('CASE WHEN task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_problem.dueDate) ELSE NULL END > 0')
                ->where('task_problem.status', '!=', 3);
            }
        if ($user->idLevel == 2) {
            $query->where('master_user.idUser', '=', $user->idUser);
        }
        // $query->where('overdueDays', '>', 0);
        $data = $query->paginate($limit);
        // dd($request);
        return view('login.problem.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
            "tanggal_awal" => $request->tanggalAwal,
            "tanggal_akhir" => $request->tanggalAkhir,
            "pic" => $request->id_pic,
            "nama_wilayah" => $request->nama_wilayah,
            "nama_cabang" => $request->nama_cabang,
            "nama_unit" => $request->nama_unit,
            "dash" => $request->dash
        ]);
    }

    public function excelpic(Request $request) {

        // return Excel::store(new WorkspaceExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'excel/AzKonvensional.xlsx', 'local');

        return Excel::download(new WorkspaceExcel(), 'WorkspacePoint.xlsx');
    }
}
