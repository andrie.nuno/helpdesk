<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\Http\Exports\AzKonvensionalExcel;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\report_harian;
// use App\model\RegisterJaminan;
// use App\model\tblsuperlender;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\exportOutstandingJob;
use App\Http\Exports\OutstandingPinjamanExcel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Download;
use Illuminate\Support\Facades\Artisan;

class AzKonvensional extends Controller
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AzKonvensionalCommandTxt::class,
    ];

    public function index()
    {
        return view('login.reportpromas.azkonvensional.index');
    }

    public function populate(Request $request) {
        // Artisan::call('exportaz:data');

        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = report_harian::where('tanggal', '>=', $request->tanggalAwal)
            ->where('tanggal', '<=', $request->tanggalAkhir)
            ->where('jenisReport', 'AZ')
            ->orderBy('tanggal', 'DESC');
        $data = $query->paginate($limit);

        return view('login.reportpromas.azkonvensional.populate',[
            "tanggalAwal"   => $request->tanggalAwal, 
            "tanggalAkhir"  => $request->tanggalAkhir, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function download(Request $request) {
        $data = report_harian::find($request->id);
        $url = $data->urlFile;
        $fileName = "AZ_".str_replace('-', '', $data->tanggal).".txt";
        // Fetch the content from the URL
        $fileContent = file_get_contents($url);
        // Set the headers for force download
        $headers = [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="'.$fileName.'"',
        ];
        // Return the response with the file content and headers
        return response($fileContent, 200, $headers);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('trans_payctrl', 'trans_payctrl.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, trans_payctrl.jmlHari, trans_payctrl.bungaHarian, trans_payctrl.jasaMitraHarian, trans_payctrl.bungaTerhutang, trans_payctrl.jasaMitraTerhutang, trans_payctrl.dendaTerhutang, trans_payctrl.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.reportpromas.azkonvensional.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    public function excel(Request $request) {
        // $result = new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit);

        // return Excel::download(new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'AzKonvensional.xlsx');

        return Excel::store(new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'excel/AzKonvensional.xlsx', 'local');
        
        // return (new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->queue('AzKonvensional.xlsx')->allOnQueue('exports');


        // (new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->queue('AzKonvensional.xlsx');
        // return back()->withSuccess('Export started!');

        // (new AzKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->store('public/AzKonvensional.xlsx');

        // exportOutstandingJob::dispatch($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit);
        // return Storage::download('public/AzKonvensional.xlsx');
    }
}
