<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\master_kategori;
use App\model\mitra_membercompany;
use App\model\mitra_memberbranch;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_limit;
use App\model\mitra_stle;
use App\model\master_task;
use Illuminate\Support\Facades\Hash;

class MasterTask extends Controller
{
    public function index()
    {
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.mastertask.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = master_task::orderBy('master_task.namaTask');
        $data = $query
            ->join('master_kategori', 'master_kategori.idKategori', '=', 'master_task.idKategori');
            // ->where('master_task.isActive', '=', 1);
        if ($request->status_task != "") {
            if ($request->status_task == 1) {
                $data = $query->where('master_task.isActive', 1);
            } elseif ($request->status_task != 1) {
                $data = $query->where('master_task.isActive', 0);
            }
        }
        if ($request->nama_task) {
            $data = $query->where('master_task.namaTask', 'like', '%'.$request->nama_task.'%');
        }
        $data = $query->selectRaw('master_task.*, master_kategori.namaKategori');
        $data = $query->paginate($limit);
        return view('login.mastertask.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $kategori = master_kategori::where('isActive', 1)->get();
                return view('login.mastertask.create', [
                    "kategoris" => $kategori,
                ]);
				break;
            case 'edit':
                $kategori = master_kategori::where('isActive', 1)->get();
                $data = master_task::find($request->id);
                return view('login.mastertask.edit', [
                    "kategoris"      => $kategori,
                    "data"      => $data,
                ]);
                break;
            case 'delete':
                $data = master_task::find($request->id);
                if ($data) {
                    $data->delete();

                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'branch':
				$idCompany = $request->idCompany;
				$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
				$result = "<option value=''>== Pilih Branch ==</option>";
				if ($branchs) {
					foreach ($branchs as $branch) {
						$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
					}
				}
				echo $result;
				break;
            case 'kabupaten':
                $idProvinsi = $request->idProvinsi;
                $kabupatens = master_kabupaten::where('idProvinsi',$idProvinsi)->get();
                $result = "<option value=''>== Pilih Kabupaten ==</option>";
                if ($kabupatens) {
                    foreach ($kabupatens as $kabupaten) {
                        $result .= "<option value='".$kabupaten->idKabupaten."'>".$kabupaten->namaKabupaten."</option>";
                    }
                }
                echo $result;
                break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'idKategori'      => 'required',
                'namaTask'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Nama Task wajib diisi.',
                'namaTask.required'         => 'Nama Task wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'idKategori'      => 'required',
                'namaTask'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Kategori wajib diisi.',
                'namaTask.required'         => 'Nama Task wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = master_task::firstOrNew(array('idTask' => $request->idTask));
                // dd($request);
                $data->idKategori = $request->idKategori;
                $data->namaTask = $request->namaTask;
                $data->isActive = $request->isActive;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
