<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblcoa;
use App\model\tblbank;
use App\model\tblcabang;
use App\model\tblproduk;
use App\model\member_jamMulai;
use App\model\member_branch;
use App\model\tblcoatemplate;
use App\model\tbllogit;

class MasterBankUnit extends Controller
{
    public function index()
    {
        return view('login.parameterfinance.masterbankunit.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        // $data = tblcoa::
        // join('tblcabang','tblcabang.idCabang','=','tblcoa.idCabang')
        // ->join('tblbank','tblbank.idBank','=','tblcoa.idBank')
        // ->selectRaw('tblcoa.*, tblcabang.namaCabang')
        // ->paginate($limit);
        $query = tblcabang::
        join('tbljeniscabang','tbljeniscabang.idJenisCabang','=','tblcabang.idJenisCabang')
        ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        ->selectRaw('tblcabang.*, tbljeniscabang.namaJenisCabang, tblwilayah.namaWilayah');
        if ($request->kode_cabang) {
            $data = $query->where('tblcabang.kodeCabang', '=', $request->kode_cabang);
        }
        if ($request->nama_cabang) {
            $data = $query->where('tblcabang.namaCabang', 'like', '%'.$request->nama_cabang.'%');
        }
        $data = $query->paginate($limit);
        // $data = tblcoa::all();
        return view('login.parameterfinance.masterbankunit.populate',[
            "data"          => $data, 
            // "date"          => "test", 
            // "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $coa = tblcoatemplate::all();
                $bank = tblbank::all();
                $cabang = tblcabang::all();
                return view('login.parameterfinance.masterbankunit.create', [
                    "banks" => $bank,
                    "coas" => $coa,
                    "cabangs" => $cabang
                ]);				
				break;
            case 'edit':
                $data = tblcoa::where('idCoa', '=', $request->id)
                ->where('idCabang', '=', $request->id2)
                ->first();
                $coa = tblcoatemplate::all();
                $bank = tblbank::all();
                $cabang = tblcabang::all();
                return view('login.parameterfinance.masterbankunit.edit', [
                    "data"      => $data,
                    "banks" => $bank,
                    "coas" => $coa,
                    "cabangs" => $cabang
                ]);
                break;
            case 'detail':
                $data = tblcoa::
                Join('tblbank', 'tblbank.idbank', '=', 'tblcoa.idBank')
                ->Join("fa_saldobank", function($join){
                    $join->on('fa_saldobank.idCabang', '=', 'tblcoa.idCabang');
                    $join->on('fa_saldobank.idBank', '=', 'tblcoa.idBank');
                })
                ->selectRaw('tblcoa.idCoa, tblcoa.idCabang, tblcoa.idBank, tblcoa.noRekening, fa_saldobank.saldoAwal, fa_saldobank.saldoMasuk, 
                fa_saldobank.saldoKeluar, fa_saldobank.saldoAkhir, tblbank.kd_bank, tblbank.namaBank')
                ->orderBY('tblbank.namaBank', 'ASC')
                ->where('tblcoa.idCabang', '=', $request->id)
                ->get();
                return view('login.parameterfinance.masterbankunit.detail', [
                    "data"  => $data
                ]);
                break;
		}
    }

    public function proses(Request $request) {
        $rules = [
            'idCabang'          => 'required',
            'idCoa'           => 'required',
        ]; 
        $messages = [
            'idCabang.required'         => 'Cabang wajib diisi.',
            'idCoa.required'          => 'COA wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $coa = tblcoatemplate::where('idCoa', '=', $request->idCoa)->first();
                $data = tblcoa::firstOrNew(['idCoa' => $request->idCoa, 'idCabang' => $request->idCabang]);
                $data->idBank = $request->idBank;
                $data->noRekening = $request->noRekening;
                $data->isActive = $request->isActive;
                $data->idUser = Auth::id();
                $data->coa = $coa->coa;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
