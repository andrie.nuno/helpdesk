<?php
 
namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPayctrl;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class Cicilan extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Pencairan Gadai Cicilan');        
        return view('login.cicilan.index',[
            "wilayah"       => $wilayah, 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_gadai.noSbg, trans_gadai.pengajuanPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, tblcabang.kodeCabang, tblcabang.namaCabang')
            ->orderBy('trans_payctrl.tanggalPencairan', 'ASC')
            ->orderBy('trans_payctrl.created_at', 'ASC')
            ->where('tblproduk.statusFunding', '=', 0)
            ->where('tblproduk.idJenisProduk', '=', 2);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('trans_payctrl.isJurnal', '=', $request->statusJurnal);
        }
        $data = $query->paginate($limit);
        return view('login.cicilan.populate',[
            "idWilayah"         => $request->idWilayah, 
            "idCabang"          => $request->idCabang, 
            "tanggalAwal"       => $request->tanggalAwal, 
            "tanggalAkhir"      => $request->tanggalAkhir, 
            "statusJurnal"      => $request->statusJurnal, 
            "data"              => $data, 
            "limit"             => $limit, 
            "page"              => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
                    ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
                    ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.idCabang, trans_gadai.nilaiPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, trans_gadai.biayaPenyimpanan, trans_gadai.pembulatan, tblcabang.kodeCabang, tblcabang.namaCabang')
                    ->where('trans_payctrl.idGadai', '=', $request->id)
                    ->first();
                if ($data) {
                    // yyyy-mm-dd
                    $v_tahunJurnal = substr($data->tanggalPencairan, 0, 4);
                    $v_bulanJurnal = substr($data->tanggalPencairan, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $data->tanggalPencairan)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($data->tanggalPencairan, 2, 2).substr($data->tanggalPencairan, 5, 2).substr($data->tanggalPencairan, 8, 2).'000001';
                    }
                    $dataSummary = [
                        'idJenisJurnal'         => 57, // Cicilan - Tunai
                        'idGadai'               => $data->idGadai, 
                        'idProduk'              => $data->idProduk, 
                        'idCabang'              => $data->idCabang, 
                        'idBankKeluar'          => 25, // Buffer
                        'tanggal'               => $data->tanggalPencairan, 
                        'tahun'                 => $v_tahunJurnal, 
                        'bulan'                 => $v_bulanJurnal, 
                        'batch'                 => $v_batchJurnal, 
                        'kodeTransaksi'         => $data->noSbg, 
                        'pokok'                 => $data->nilaiPinjaman, 
                        'pencairan'             => $data->nilaiApCustomer, 
                        'bunga'                 => $data->biayaPenyimpanan, 
                        'admin'                 => $data->biayaAdmin, 
                        'pembulatan'            => $data->pembulatan, 
                        'keterangan'            => 'Pencairan '.$data->noSbg, 
                        'keteranganTransaksi'   => 'Pencairan Gadai Cicilan '.$data->noSbg, 
                    ];
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;
                    
                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.pokok, akunting_summary.pencairan, akunting_summary.bunga, akunting_summary.admin, akunting_summary.pembulatan')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        $sumDebet = 0;
                        $sumKredit = 0;
                        foreach($dataJurnal as $dita) {
                            if ($dita->nilai == 'pokok') {
                                $amount = $dita->pokok;
                            }
                            if ($dita->nilai == 'pencairan') {
                                $amount = $dita->pencairan;
                            }
                            if ($dita->nilai == 'bunga') {
                                $amount = $dita->bunga;
                            }
                            if ($dita->nilai == 'admin') {
                                $amount = $dita->admin;
                            }
                            if ($dita->nilai == 'pembulatan') {
                                $amount = $dita->pembulatan;
                            }
                            if ($amount > 0) {
                                if ($dita->idCoa == 207) {
                                    if ($sumDebet != $sumKredit) {
                                        $dataDetail = [
                                            'idSummary'     => $lastId, 
                                            'idCabang'      => $dita->idCabang, 
                                            'urut'          => $dita->urut, 
                                            'idCoa'         => $dita->idCoa, 
                                            'tanggal'       => $dita->tanggal, 
                                            'tahun'         => $v_tahunJurnal, 
                                            'bulan'         => $v_bulanJurnal, 
                                            'batch'         => $v_batchJurnal, 
                                            'coa'           => $dita->coa, 
                                            'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa, 
                                            'keterangan'    => $dita->description, 
                                            'dk'            => $dita->dk, 
                                            'amount'        => $amount
                                        ];
                                        $detail = AkuntingDetail::create($dataDetail);
                                    }
                                } else {
                                    if ($dita->dk == 'D') {
                                        $sumDebet += $amount;
                                    } else {
                                        $sumKredit += $amount;
                                    }
                                    $dataDetail = [
                                        'idSummary'     => $lastId, 
                                        'idCabang'      => $dita->idCabang, 
                                        'urut'          => $dita->urut, 
                                        'idCoa'         => $dita->idCoa, 
                                        'tanggal'       => $dita->tanggal, 
                                        'tahun'         => $v_tahunJurnal, 
                                        'bulan'         => $v_bulanJurnal, 
                                        'batch'         => $v_batchJurnal, 
                                        'coa'           => $dita->coa, 
                                        'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa, 
                                        'keterangan'    => $dita->description, 
                                        'dk'            => $dita->dk, 
                                        'amount'        => $amount
                                    ];
                                    $detail = AkuntingDetail::create($dataDetail);
                                }
                            }
                        }
                    }
                    $dataUpdate = [
                        'isJurnal'      => 1,  
                        'dateJurnal'    => date('Y-m-d H:i:s'),
                    ];
                    TransPayctrl::where('idGadai', '=', $request->id)
                        ->update($dataUpdate);
                }
                DB::commit();
                return view('login.cicilan.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.cicilan.jurnal',[
                    "idGadai"   => $request->id, 
                    "data"      => $data, 
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
