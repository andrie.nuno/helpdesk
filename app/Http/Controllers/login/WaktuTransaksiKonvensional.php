<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\trans_gadai;
use App\model\RegisterJaminan;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\OutstandingPinjamanExcel;
use App\Http\Exports\WaktuTransaksiKonvensionalExcel;
use App\model\tblwilayah;

class WaktuTransaksiKonvensional extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::orderBy('idWilayah')->get();
        return view('login.reporttransaksi.waktutransaksikonvensional.index',[
            "wilayahs"  => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = trans_gadai::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
        ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai.idCustomer')
        ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'trans_gadai.idGadai')
        ->join('tran_fapg','tran_fapg.idFAPG','=','trans_gadai.idFAPG')
        ->join("tran_taksiran as taksirawal", function($join){
            $join->on('taksirawal.idFAPG','=','trans_gadai.idFAPG');
            $join->where(DB::raw("taksirawal.isFinal"), "=", 0);
        })
        ->join("tran_taksiran as taksirfinal", function($join){
            $join->on('taksirfinal.idFAPG','=','trans_gadai.idFAPG');
            $join->where(DB::raw("taksirfinal.isFinal"), "=", 1);
        })
        ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, 
        CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, 
        trans_gadai.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, 
        tran_fapg.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, 
        trans_gadai.created_at AS timeInputGadai, trans_gadai.tglApprovalKaunit, trans_gadai.tglApprovalKacab, 
        trans_gadai.tglApprovalKawil, trans_gadai.tglApprovalDirektur, trans_gadai.tglApprovalDirut, 
        trans_gadai.tglCetakKwitansi AS timeCetakKwitansi")
        ->where('trans_payctrl.tanggalPencairan', '>=', $request->tgl_awal)
        ->where('trans_payctrl.tanggalPencairan', '<=', $request->tgl_sampai)
        ->orderBy('trans_payctrl.tanggalPencairan');
        if ($request->nama_wilayah) {
            $data = $query->where('tblcabang.idWilayah', $request->nama_wilayah);
        }
        $data = $query->paginate($limit);
        return view('login.reporttransaksi.waktutransaksikonvensional.populate',[
            "nama_wilayah"   => $request->nama_wilayah, 
            "tgl_awal"      => $request->tgl_awal, 
            "tgl_sampai"    => $request->tgl_sampai, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('payctrl', 'payctrl.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, payctrl.jmlHari, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaTerhutang, payctrl.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.laporanborrower.outstandingpinjaman.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    // public function ajax(Request $request) {
	// 	switch ($request->type) {
	// 		case 'branch':
	// 			$idCompany = $request->idCompany;
	// 			$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
	// 			$result = "<option value=''>== Pilih Branch ==</option>";
	// 			if ($branchs) {
	// 				foreach ($branchs as $branch) {
	// 					$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
	// 				}
	// 			}
	// 			echo $result;
	// 			break;
	// 	}
	// }

    public function excel(Request $request){
        return Excel::download(new WaktuTransaksiKonvensionalExcel($request->nama_wilayah, $request->tgl_awal, $request->tgl_sampai), 'WaktuTransaksiKonvensional.xlsx');
    }
}
