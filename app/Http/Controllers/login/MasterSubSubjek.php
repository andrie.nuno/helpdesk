<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\master_kategori;
use App\model\master_task;
use App\model\master_subjek;
use App\model\master_sub_subjek;
use Illuminate\Support\Facades\Hash;

class MasterSubSubjek extends Controller
{
    public function index()
    {
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.mastersubsubjek.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = master_sub_subjek::orderBy('master_sub_subjek.namaSubSubjek');
        $data = $query
            ->join('master_kategori', 'master_kategori.idKategori', '=', 'master_sub_subjek.idKategori')
            ->join('master_task', 'master_task.idTask', '=', 'master_sub_subjek.idTask')
            ->join('master_subjek', 'master_subjek.idSubjek', '=', 'master_sub_subjek.idSubjek');
            // ->where('master_sub_subjek.isActive', '=', 1);
        if ($request->status_subsubjek != "") {
            if ($request->status_subsubjek == 1) {
                $data = $query->where('master_sub_subjek.isActive', 1);
            } elseif ($request->status_subsubjek != 1) {
                $data = $query->where('master_sub_subjek.isActive', 0);
            }
        }
        if ($request->nama_subsubjek) {
            $data = $query->where('master_sub_subjek.namaSubSubjek', 'like', '%'.$request->nama_subsubjek.'%');
        }
        $data = $query->selectRaw('master_sub_subjek.*, master_kategori.namaKategori, master_task.namaTask,master_subjek.namaSubjek');
        $data = $query->paginate($limit);
        return view('login.mastersubsubjek.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $kategori = master_kategori::where('isActive', 1)->get();
                $task = master_task::where('isActive', 1)->get();
                $subjek = master_subjek::where('isActive', 1)->get();
                return view('login.mastersubsubjek.create', [
                    "kategoris" => $kategori,
                    "tasks" => $task,
                    "subjeks" => $subjek,
                ]);
				break;
            case 'edit':
                $kategori = master_kategori::where('isActive', 1)->get();
                $task = master_task::where('isActive', 1)->get();
                $subjek = master_subjek::where('isActive', 1)->get();
                $data = master_sub_subjek::find($request->id);
                return view('login.mastersubsubjek.edit', [
                    "kategoris"      => $kategori,
                    "tasks"      => $task,
                    "subjeks" => $subjek,
                    "data"      => $data,
                ]);
                break;
            case 'delete':
                $data = master_sub_subjek::find($request->id);
                if ($data) {
                    $data->delete();

                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'task':
                $idKategori = $request->idKategori;
                $tasks = master_task::where('idKategori',$idKategori)->get();
                // dd($tasks);
                $result = "<option value=''>== Pilih Task ==</option>";
                if ($tasks) {
                    foreach ($tasks as $task) {
                        $result .= "<option value='".$task->idTask."'>".$task->namaTask."</option>";
                    }
                }
                echo $result;
            break;
            case 'subjek':
                $idTask = $request->idTask;
                $subjeks = master_subjek::where('idTask',$idTask)->get();
                // dd($subjeks);
                $result2 = "<option value=''>== Pilih Subjek ==</option>";
                if ($subjeks) {
                    foreach ($subjeks as $subjek) {
                        $result2 .= "<option value='".$subjek->idSubjek."'>".$subjek->namaSubjek."</option>";
                    }
                }
                echo $result2;
            break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'idKategori'      => 'required',
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                'namaSubSubjek'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Nama Kategori wajib diisi.',
                'idTask.required'         => 'Task wajib diisi.',
                'idSubjek.required'         => 'Subjek wajib diisi.',
                'namaSubSubjek.required'         => 'Nama Sub Subjek wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'idKategori'      => 'required',
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                'namaSubSubjek'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Kategori wajib diisi.',
                'idTask.required'         => 'Task wajib diisi.',
                'idSubjek.required'         => 'Subjek wajib diisi.',
                'namaSubSubjek.required'         => 'Nama Subjek wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        // dd($request);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = master_sub_subjek::firstOrNew(array('idSubSubjek' => $request->idSubSubjek));
                // dd($request);
                $data->idKategori = $request->idKategori;
                $data->idTask = $request->idTask;
                $data->idSubjek = $request->idSubjek;
                $data->namaSubSubjek = $request->namaSubSubjek;
                $data->isActive = $request->isActive;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
