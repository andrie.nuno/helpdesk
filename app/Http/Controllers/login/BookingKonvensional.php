<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\Http\Exports\BookingKonvensionalExcel;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\trans_gadai;
// use App\model\RegisterJaminan;
// use App\model\tblsuperlender;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\exportOutstandingJob;
use App\Http\Exports\OutstandingPinjamanExcel;
use Illuminate\Support\Facades\Storage;
use App\model\report_harian;

class BookingKonvensional extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::orderBy('idWilayah')->where('isActive', 1)->get();
        return view('login.reportpromas.bookingkonvensional.index',[
            "wilayahs"      => $wilayah,
            // "idCompany"     => Auth::user()->idCompany, 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $tanggal = $request->tanggal;
        $query = report_harian::where('tanggal', '>=', $request->tanggalAwal)
        ->where('tanggal', '<=', $request->tanggalAkhir)
        ->where('jenisReport', 'Booking');
        $data = $query->paginate($limit);
        // $query = trans_gadai::
        // join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        // ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        // ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        // ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        // ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        // ->leftJoin('tblkelurahan','tblkelurahan.idKelurahan','=','tblcustomer.idKelurahanKtp')
        // ->leftJoin('tblkelurahan AS domisili','domisili.idKelurahan','=','tblcustomer.idKelurahanDomisili')
        // ->leftJoin('tblpekerjaan','tblpekerjaan.idPekerjaan','=','tblcustomer.idPekerjaan')
        // ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        // ->join('tran_taksiran','tran_taksiran.idTaksiran','=','trans_gadai.idTaksiran')
        // ->join('tran_fapg','tran_fapg.idFAPG','=','trans_gadai.idFAPG')
        // ->leftJoin("tran_taksiran as taksir", function($join){
        //     $join->on('taksir.idFAPG','=','trans_gadai.idFAPG');
        //     $join->where(DB::raw("taksir.isFinal"), "=", 0);
            
        // })
        // ->leftJoin('tbltujuantransaksi','tbltujuantransaksi.idTujuanTransaksi','=','trans_gadai.idTujuanTransaksi')
        // ->leftJoin('tblsektorekonomi','tblsektorekonomi.idSektorEkonomi','=','trans_gadai.idSektorEkonomi')
        // ->leftJoin('tbljenisreferensi','tbljenisreferensi.idJenisReferensi','=','trans_gadai.idJenisReferensi')
        // ->leftJoin('tblcustomer AS cgc','cgc.cif','=','trans_gadai.referensiCif')
        // ->leftJoin('tblasaljaminan','tblasaljaminan.idAsalJaminan','=','trans_gadai.idAsalJaminan')
        // ->leftJoin('tbljenisreferensi AS refCust','refCust.idJenisReferensi','=','tblcustomer.idJenisReferensi')
        // ->leftJoin('tblcustomer AS cgcNasabah','cgcNasabah.cif','=','tblcustomer.referensiCif')
        // ->leftJoin('trans_gadai AS sbgLama','sbgLama.idGadai','=','tran_fapg.idGadai')
        // ->leftJoin('trans_payctrl AS payctrlLama','payctrlLama.idGadai','=','tran_fapg.idGadai')
        // ->leftJoin('trans_gadaijf','trans_gadaijf.idGadai','=','trans_gadai.idGadai')
        // ->leftJoin('tbluser','tbluser.idUser','=','tran_taksiran.idUser')
        // ->leftJoin('tblkaryawaninternal','tblkaryawaninternal.idKaryawan','=','tbluser.idKaryawan')
        // ->leftJoin("tblkaryawaninternal as refInternal", function($join){
        //     $join->on('refInternal.npk', '=', 'trans_gadai.referensiNpk');
        //     $join->where(DB::raw("refInternal.isActive"), "=", 1);
            
        // })
        // ->leftJoin("tblkaryawaneksternal as refEksternal", function($join){
        //     $join->on('refEksternal.kodeAgent', '=', 'trans_gadai.referensiNpk');
        //     $join->where(DB::raw("refEksternal.isActive"), "=", 1);
            
        // })
        // ->leftJoin('tbljabatankaryawan as jabInternal','jabInternal.idJabatanKaryawan','=','refInternal.idJabatanKaryawan')
        // ->leftJoin('tbljabatankaryawan as jabeksternal','jabeksternal.idJabatanKaryawan','=','refEksternal.idJabatanKaryawan')
        // ->leftJoin('tblprogram','tblprogram.idProgram','=','trans_gadai.idProgram')
        // ->leftJoin("trans_fundingdetail", function($join){
        //     $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
        //     $join->whereNull('trans_fundingdetail.tglUnpledging');
            
        // })
        // ->leftJoin("tblsettingjf", function($join){
        //     $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
        //     $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            
        // })
        // ->leftJoin('trans_funding','trans_funding.idFunding','=','trans_fundingdetail.idFunding')
        // ->leftJoin('tblpartner','tblpartner.idPartner','=','trans_funding.idPartner')
        // ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet,
        // CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, tblcustomer.cif, 
        // tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
        // REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
        // tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
        // tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
        // IF(tblcustomer.alamatDomisili != '', 
        //     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
        //     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
        // ) AS alamatDomisili, 
        // IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
        // IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
        // tblcustomer.hp AS noHp, tblpekerjaan.namaPekerjaan AS pekerjaan, 
        // tblcustomer.tglRegister, DATE(trans_gadai.tglCair) AS tglCair, trans_payctrl.tanggalPelunasan, trans_gadai.lamaPinjaman AS tenor, trans_payctrl.ovd, 
        // trans_gadai.transKe AS ke, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, 
        // trans_gadai.nilaiPinjaman AS pokokAwal, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS pinjamAwal, 
        // trans_gadai.nilaiPinjaman AS saldoPokok, trans_gadai.biayaPenyimpanan AS saldoBunga, 
        // (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS saldoPinjam, 
        // trans_gadai.biayaAdmin, trans_gadai.biayaAdminAwal, trans_gadai.diskonAdmin, taksir.totalTaksiran AS taksir1, 
        // taksir.avgKarat AS karat1, taksir.sumBeratBersih AS berat1, tran_taksiran.totalTaksiran AS taksir, tran_taksiran.avgKarat AS karat, 
        // tran_taksiran.sumBeratBersih AS berat, tran_fapg.stleAntam AS stle, tbltujuantransaksi.namaTujuanTransaksi, 
        // tblsektorekonomi.namaSektorEkonomi, tbljenisreferensi.namaJenisReferensi AS asalAplikasi, trans_gadai.referensiNpk AS kodeReferensiAplikasi, 
        // trans_gadai.referensiNama AS namaReferensiAplikasi, 
        // IF(refInternal.idKaryawan != '', jabInternal.namaJabatanKaryawan, jabeksternal.namaJabatanKaryawan) AS jabatan, 
        // CONCAT(cgc.cif, '-', cgc.namaCustomer) AS namaCgcAplikasi, tblprogram.namaProgram, trans_gadai.statusAplikasi, tblasaljaminan.namaAsalJaminan, 
        // refCust.namaJenisReferensi AS referensiNasabah, tblcustomer.referensiNpk AS kodeReferensiNasabah, tblcustomer.referensiNama AS namaReferensiNasabah, 
        // tblcustomer.referensiCif AS cifReferensiNasabah, cgcNasabah.namaCustomer AS namaCgcReferensiNasabah, sbgLama.noSbg AS noSbgLama, 
        // payctrlLama.ovd AS ovdLama, (trans_gadai.transKe - 1) AS perpanjanganKe, trans_gadai.jenisPembayaran, tblcustomer.statusOcrKtp, 
        // tblpartner.namaPartner AS bankFunding, tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.rate AS RatePendana, 
        // tblsettingjf.namaJf AS bankPendana, tblsettingjf.porsi AS porsiBank, tblkaryawaninternal.namaKaryawan AS namaPenaksir, 
        // trans_payctrl.created_at AS tanggalJam, tran_fapg.imgJaminan AS foto, trans_gadai.approvalFinal, 
        // IF(trans_gadai.statusAplikasi = 'NEW ORDER', tblcustomer.kodeVoucher, '') AS kodeVoucher")
        //     ->where('trans_payctrl.tanggalPencairan', '>=', substr($tanggal,0,7)."-01")
        //     ->where('trans_payctrl.tanggalPencairan', '<', $tanggal)
        //     // ->where(DB::raw("(trans_gadai.tanggalpelunasan > $tanggal or trans_gadai.tanggalpelunasan is null)"))
        //     ;
        // if ($request->nama_wilayah) {
        //     $data = $query->where('tblcabang.idWilayah', $request->nama_wilayah);
        // }
        // if ($request->nama_cabang) {
        //     $data = $query->where('tblcabang.idCabang', $request->nama_cabang);
        // }
        // if ($request->nama_unit) {
        //     $data = $query->where('tblcabang.headCabang', $request->nama_unit);
        // }
        // $data = $query->paginate($limit);

        // $dd = dd($query->toSql());
        // $dd2 = dd($request);
        return view('login.reportpromas.bookingkonvensional.populate',[
            "tanggalAwal"   => $request->tanggalAwal, 
            "tanggalAkhir"  => $request->tanggalAkhir, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('trans_gadai', 'trans_gadai.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, trans_gadai.jmlHari, trans_gadai.bungaHarian, trans_gadai.jasaMitraHarian, trans_gadai.bungaTerhutang, trans_gadai.jasaMitraTerhutang, trans_gadai.dendaTerhutang, trans_gadai.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.reportpromas.bookingkonvensional.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
                // if ($request->idWilayah != 0){
                    $cabangs = tblcabang::where('idWilayah', '=', $idWilayah)->where('idJenisCabang', '=', 4)->orderBy('namaCabang')->get();
                // }
                $result = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
						$result .= "<option value='".$cabang->idCabang."'>".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
            case 'unit':
                $idHead = $request->idHead;
                // if ($request->idHead != 0){
                    $units = tblcabang::where('headCabang', '=', $idHead)->get();
                // }
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($units) {
                    foreach ($units as $unit) {
                        $result2 .= "<option value='".$unit->idCabang."'>".$unit->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
		}
	}

    public function download(Request $request) {
        $data = report_harian::find($request->id);
        $url = $data->urlFile;
        $fileName = "Booking_".str_replace('-', '', $data->tanggal).".txt";
        // Fetch the content from the URL
        $fileContent = file_get_contents($url);
        // Set the headers for force download
        $headers = [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="'.$fileName.'"',
        ];
        // Return the response with the file content and headers
        return response($fileContent, 200, $headers);
    }

    public function excel(Request $request) {
        return Excel::download(new BookingKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'BookingKonvensional.xlsx');

        // (new BookingKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->queue('BookingKonvensional.xlsx');
        // return back()->withSuccess('Export started!');

        // (new BookingKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->store('BookingKonvensional.xlsx');

        // exportOutstandingJob::dispatch($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit);
        // return Storage::download('public/AzKonvensional.xlsx');
    }
}
