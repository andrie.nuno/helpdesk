<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblcabang;
use App\model\tblcoatemplate;
use App\model\tblcoausedfor;
use App\model\tblcashflow;
use App\model\tblwilayah;
use App\model\updateinfomodel;
use Illuminate\Support\HtmlString;
use Maatwebsite\Excel\Facades\Excel;
// use App\Http\Exports\ReportKonfirmasiExcel;

class Dashboard extends Controller
{
    public function index()
    {
        $usedfor = tblcoausedfor::where('isActive', 1)->get();
        $cahflow = tblcashflow::where('isActive', 1)->get();
        // \dd($cahflows, $usedfors);
        return view('login.akunting.menucoa.index',[
            "usedfors"          => $usedfor,
            "cashflows"         => $cahflow,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = tblcoatemplate::leftJoin('tblcoausedfor', 'tblcoausedfor.idUsedfor', '=', 'tblcoatemplate.idUsedfor')
        ->leftJoin('tblcashflow', 'tblcashflow.idCashflow', '=', 'tblcoatemplate.idCashflow')
        ->selectRaw("tblcoatemplate.idCoa, tblcoatemplate.coa, tblcoatemplate.coaOracle, tblcoatemplate.description, tblcoatemplate.headAccount,
        tblcoausedfor.namaUsedfor AS usedFor, CONCAT(tblcashflow.kodeCashflow, '-', tblcashflow.namaCashflow) AS cashFlow,
        tblcoatemplate.accountType, tblcoatemplate.qualifiers,
        IF(tblcoatemplate.isActive = 1, 'Konven & Umum', IF(tblcoatemplate.isActiveSyariah = 1, 'Syariah', '')) AS jenisCoa,
        IF(tblcoatemplate.statusRekonBank = 0, 'COA Transaksi', IF(tblcoatemplate.statusRekonBank = 1, 'Bank Konven', 'Bank Syariah')) AS rekonBank");
        if ($request->coa_promas) {
            $query->where('tblcoatemplate.coa', $request->coa_promas);
        }
        if ($request->coa_oracle) {
            $query->where('tblcoatemplate.coaOracle', $request->coa_oracle);
        }
        if ($request->desk_coa) {
            $query->where('tblcoatemplate.description', $request->desk_coa);
        }
        if ($request->used_for) {
            $query->where('tblcoatemplate.idUsedfor', $request->used_for);
        }
        if ($request->cash_flow) {
            $query->where('tblcoatemplate.idCashflow', $request->cash_flow);
        }
        $data = $query->paginate($limit);
        return view('login.akunting.menucoa.populate',[
            "data"          => $data,
            "limit"         => $limit,
            "page"          => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $usedfor = tblcoausedfor::where('isActive', 1)->get();
                $cahflow = tblcashflow::where('isActive', 1)->get();
                return view('login.akunting.menucoa.create',[
                    "usedfors"          => $usedfor,
                    "cashflows"         => $cahflow,
                ]);
				break;
            case 'edit':
                $data = tblcoatemplate::find($request->id);
                $usedfor = tblcoausedfor::where('isActive', 1)->get();
                $cahflow = tblcashflow::where('isActive', 1)->get();
                return view('login.akunting.menucoa.edit', [
                    "data"              => $data,
                    "usedfors"          => $usedfor,
                    "cashflows"         => $cahflow,
                ]);
                break;
            case 'delete':
                $data = tblcoatemplate::firstOrNew(array('id' => $request->id));
                $data->isActive = 0;
                $data->save();
                echo '<script>closeMultiModal(1);doSearch("Populate");swal("Sukses!", "Proses Berhasil", "success")</script>';
                break;
		}
    }

    // public function ajax(Request $request) {
	// 	switch ($request->type) {
	// 		case 'cabang':
	// 			$idWilayah = $request->idWilayah;
	// 			$cabang = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
    //                 ->where('idWilayah', $idWilayah)
    //                 ->where('idJenisCabang', 4)
    //                 ->where('isActive', 1)
    //                 ->whereNull('tglNonAktif')
    //                 ->orderBy('kodeCabang')
    //                 ->get();
	// 			$result1 = "<option value=''>== Pilih Cabang ==</option>";
	// 			if ($cabang) {
	// 				foreach ($cabang as $branch) {
	// 					$result1 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
	// 				}
	// 			}
	// 			echo $result1;
	// 			break;
    //         case 'unit':
    //             $idCabang = $request->idCabang;
    //             $branchs = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
    //                 ->where('headCabang', $idCabang)
    //                 ->where('isActive', 1)
    //                 ->whereNull('tglNonAktif')
    //                 ->orderBy('kodeCabang')->get();
    //             $result2 = "<option value=''>== Pilih Unit ==</option>";
    //             if ($branchs) {
    //                 foreach ($branchs as $branch) {
    //                     $result2 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
    //                 }
    //             }
    //             echo $result2;
    //             break;
	// 	}
	// }

    public function proses(Request $request) {
        $rules = [
            'coa'  => 'required',
            'coaOracle'     => 'required',
            'headAccount'  => 'required',
        ];
        $messages = [
            'coa.required'     => 'COA Promas wajib diisi.',
            'coaOracle.required'        => 'Coa Oracle wajib diisi.',
            'headAccount.required'     => 'Head Account wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                // \dd($request);
                DB::beginTransaction();
                $data = tblcoatemplate::firstOrNew(array('idCoa' => $request->idCoa));
                $data->coa = $request->coa;
                $data->coaOracle = $request->coaOracle;
                $data->description = $request->description;
                $data->headAccount = $request->headAccount;
                $data->idUsedFor = $request->usedFor;
                $data->idCashFlow = $request->cashFlow;
                $data->accountType = $request->accountType;
                $data->qualifiers = $request->qualifiers;
                $data->rekonBank = $request->rekonBank;
                $data->idUser = Auth::id();
                if ($request->jenisCoa != 0) {
                    $data->isActive = 0;
                    $data->isActiveSyariah = 1;
                } else {
                    $data->isActive = 1;
                    $data->isActiveSyariah = 0;
                }
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}

    // public function excel(Request $request){
    //     return Excel::download(new ReportKonfirmasiExcel($request->idUpdate, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'ReportKonfirmasi.xlsx');
    // }
}
