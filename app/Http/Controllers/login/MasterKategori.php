<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\mitra_membercompany;
use App\model\mitra_memberbranch;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_limit;
use App\model\mitra_stle;
use App\model\master_kategori;
use Illuminate\Support\Facades\Hash;

class MasterKategori extends Controller
{
    public function index()
    {
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.masterkategori.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = master_kategori::orderBy('master_kategori.namaKategori');
        // $data = $query
        //     ->where('master_kategori.isActive', '=', 1);
        if ($request->status_kategori != "") {
            if ($request->status_kategori == 1) {
                $data = $query->where('master_kategori.isActive', 1);
            } elseif ($request->status_kategori != 1) {
                $data = $query->where('master_kategori.isActive', 0);
            }
        }
        if ($request->nama_kategori) {
            $data = $query->where('master_kategori.namaKategori', 'like', '%'.$request->nama_kategori.'%');
        }
        $data = $query->selectRaw('master_kategori.*');
        $data = $query->paginate($limit);
        return view('login.masterkategori.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.masterkategori.create');
				break;
            case 'edit':
                $data = master_kategori::find($request->id);
                return view('login.masterkategori.edit', [
                    "data"      => $data,
                ]);
                break;
            case 'delete':
                $data = master_kategori::find($request->id);
                if ($data) {
                    $data->delete();

                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'branch':
				$idCompany = $request->idCompany;
				$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
				$result = "<option value=''>== Pilih Branch ==</option>";
				if ($branchs) {
					foreach ($branchs as $branch) {
						$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
					}
				}
				echo $result;
				break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'namaKategori'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'namaKategori.required'         => 'Nama User wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'namaKategori'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'namaKategori.required'         => 'Nama User wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = master_kategori::firstOrNew(array('idKategori' => $request->idKategori));
                // dd($request);
                $data->namaKategori = $request->namaKategori;
                $data->isActive = $request->isActive;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
