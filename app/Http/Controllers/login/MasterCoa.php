<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblcoatemplate;
use App\model\tblbank;
use App\model\tblproduk;
use App\model\member_jamMulai;
use App\model\member_branch;
use App\model\tbllogit;

class MasterCoa extends Controller
{
    public function index()
    {
        return view('login.parameterfinance.mastercoa.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $data = tblcoatemplate::paginate($limit);
        // $data = tblcoatemplate::all();
        return view('login.parameterfinance.mastercoa.populate',[
            "data"          => $data, 
            // "date"          => "test", 
            // "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $bank = tblbank::all();
                return view('login.parameterfinance.mastercoa.create', [
                    "banks" => $bank,
                ]);				
				break;
            case 'edit':
                $bank = tblbank::all();
                $data = tblcoatemplate::where('idCoa', '=', $request->id)
                    ->first();
                return view('login.parameterfinance.mastercoa.edit', [
                    "banks" => $bank,
                    "data"      => $data
                ]);
                break;
            // case 'detail':
            //     $data = tblcoatemplate::Join('tblproduk', 'tblproduk.namaBatch', '=', 'tblcoatemplate.namaBatch')
            //     ->Join('member_jamMulai', 'member_jamMulai.idjamMulai', '=', 'tblcoatemplate.idjamMulai')
            //     ->Join('member_branch', 'member_branch.idBranch', '=', 'tblcoatemplate.idBranch')
            //     ->selectRaw('tblcoatemplate.*, tblproduk.keterangan, member_jamMulai.namajamMulai, member_branch.namaBranch')
            //     ->where('tblproduk.isActive', '=', 1)
            //     ->orderBY('tblcoatemplate.namaBatch', 'ASC')
            //     ->orderBY('tblcoatemplate.idjamMulai', 'ASC')
            //         ->where('tblcoatemplate.idCoa', '=', $request->id)
            //         ->first();
            //     return view('login.parameterfinance.mastercoa.detail', [
            //         "data"  => $data
            //     ]);
            //     break;
		}
    }

    public function proses(Request $request) {
        $rules = [
            'coa'          => 'required',
        ]; 
        $messages = [
            'coa.required'         => 'COA wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = tblcoatemplate::firstOrNew(['idCoa' => $request->idCoa]);
                $data->idUsedFor = $request->idUsedFor;
                $data->coa = $request->coa;
                $data->headAccount = $request->headAccount;
                $data->description = $request->description;
                $data->additionalDesc = $request->additionalDesc;
                $data->transactionType = $request->transactionType;
                $data->pettyCash = $request->pettyCash;
                $data->rekonBank = $request->rekonBank;
                $data->paymentRequest = $request->paymentRequest;
                $data->coaOracle = $request->coaOracle;
                $data->accountType = $request->accountType;
                $data->qualifiers = $request->qualifiers;
                $data->beginBalance = $request->beginBalance;
                $data->isActive = $request->isActive;
                $data->isActiveSyariah = $request->isActiveSyariah;
                $data->isDefault = $request->isDefault;
                $data->idBank = $request->idBank;
                $data->idUser = Auth::id();
                $data->isBuffer = $request->isBuffer;
                $data->isKasBesar = $request->isKasBesar;
                $data->idCashflow = $request->idCashflow;
                $data->kasMarketing = $request->kasMarketing;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
