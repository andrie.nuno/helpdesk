<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\PelaporanExcel;
use App\model\master_subjek;
use App\model\master_sub_subjek;
use App\model\master_task;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\master_user;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_limit;
use App\model\mitra_stle;
use App\model\task_pelaporan;
use Illuminate\Support\Facades\Hash;

class Pelaporan extends Controller
{
    public function index()
    {
        // $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        $pic = master_user::where('isActive', '=', 1)->get();
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.pelaporan.index',[
            // "wilayahs"       => $wilayah,
            "pics"       => $pic,
        ]);
    }

    public function populate(Request $request) {
        $user = Auth::user();
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = task_pelaporan::orderBy('task_pelaporan.idTaskPelaporan');
        $data = $query
        ->leftJoin('master_task','master_task.idTask','=','task_pelaporan.idTask')
        ->leftJoin('master_subjek','master_subjek.idSubjek','=','task_pelaporan.idSubjek')
        ->leftJoin('master_user','master_user.idUser','=','task_pelaporan.idPic');
        if ($user->idLevel == 2) {
            $data = $query->where('master_user.idUser', '=', $user->idUser);
        } elseif ($request->id_pic) {
            $data = $query->where('task_pelaporan.idPic', '=', $request->id_pic);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('task_pelaporan.startDate', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('task_pelaporan.dueDate', '<=', $request->tanggalAkhir);
        }
        if ($request->nama_task) {
            $data = $query->where('master_task.namaTask', 'like', '%'.$request->nama_task.'%');
        }
        $data = $query->selectRaw('task_pelaporan.*, master_task.namaTask, master_subjek.namaSubjek, master_user.namaUser,
        CASE
            WHEN task_pelaporan.status != 3 AND task_pelaporan.dueDate < NOW() + INTERVAL 7 HOUR THEN
                CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_pelaporan.dueDate - INTERVAL 24 HOUR) / 24)
            WHEN task_pelaporan.status = 3 AND task_pelaporan.doneDate > task_pelaporan.dueDate THEN
                CEIL(TIMESTAMPDIFF(HOUR, task_pelaporan.dueDate, task_pelaporan.doneDate) / 24)
            ELSE NULL
        END AS overdueDays');
    if ($request->dash){
        $query->whereRaw('CASE WHEN task_pelaporan.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_pelaporan.dueDate) ELSE NULL END > 0')
        ->where('task_pelaporan.status', '!=', 3);
    }
        $data = $query->paginate($limit);
        return view('login.pelaporan.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
            "tanggal_awal" => $request->tanggalAwal,
            "tanggal_akhir" => $request->tanggalAkhir,
            "pic" => $request->id_pic,
            "dash" => $request->dash,
            "nama_task" => $request->nama_task,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $task = master_task::where('isActive', '=', 1)->where('idKategori', '=', 5)->get();
                $subjek = master_subjek::where('isActive', '=', 1)->get();
                $pic = master_user::where('isActive', '=', 1)->get();
                return view('login.pelaporan.create', [
                    "tasks" => $task,
                    "subjeks" => $subjek,
                    "pics" => $pic,
                ]);
				break;
            case 'edit':
                $task = master_task::where('isActive', '=', 1)->where('idKategori', '=', 5)->get();
                $subjek = master_subjek::where('isActive', '=', 1)->get();
                $pic = master_user::where('isActive', '=', 1)->get();
                $data = task_pelaporan::find($request->id);
                return view('login.pelaporan.edit', [
                    "tasks" => $task,
                    "subjeks" => $subjek,
                    "pics" => $pic,
                    "data" => $data,
                ]);
                break;
            case 'detail':
                $data = task_pelaporan::join('master_task','master_task.idTask','=','task_pelaporan.idTask')
                ->join('master_subjek','master_subjek.idSubjek','=','task_pelaporan.idSubjek')
                ->join('master_user','master_user.idUser','=','task_pelaporan.idPic')
                ->selectRaw('task_pelaporan.*, master_task.namaTask, master_subjek.namaSubjek, master_user.namaUser,
        CASE
            WHEN task_pelaporan.status != 3 AND task_pelaporan.dueDate < NOW() + INTERVAL 7 HOUR THEN
                CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_pelaporan.dueDate - INTERVAL 24 HOUR) / 24)
            WHEN task_pelaporan.status = 3 AND task_pelaporan.doneDate > task_pelaporan.dueDate THEN
                CEIL(TIMESTAMPDIFF(HOUR, task_pelaporan.dueDate, task_pelaporan.doneDate) / 24)
            ELSE NULL
        END AS overdueDays')
                ->where('task_pelaporan.idTaskPelaporan', '=', $request->id)
                ->first();
                // dd($data);
                return view('login.pelaporan.detail',[
                    // "idTaskPelaporan"         => $request->id,
                    "data"                  => $data,
                ]);
                break;
            case 'delete':
                $data = task_pelaporan::firstOrNew(array('id' => $request->id));
                $data->status = 0;
                $data->isTampil = 0;
                $data->save();
                echo '<script>closeMultiModal(1);doSearch("Populate");swal("Sukses!", "Proses Berhasil", "success")</script>';
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'subjek':
                $idTask = $request->idTask;
                $subjeks = master_subjek::where('idTask',$idTask)->where('isActive', 1)->get();
                // dd($subjeks);
                $result3 = "<option value=''>== Pilih Subjek ==</option>";
                if ($subjeks) {
                    foreach ($subjeks as $subjek) {
                        $result3 .= "<option value='".$subjek->idSubjek."'>".$subjek->namaSubjek."</option>";
                    }
                }
                echo $result3;
            break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                'startDate'      => 'required',
                'dueDate'      => 'required|after:startDate',
                'idPic'      => 'required',
                'status'      => 'required',
            ];
            $messages = [
                'idTask.required'         => 'Task wajib diisi.',
                'idSubjek.required'      => 'Subjek wajib diisi.',
                'idPic.required'      => 'PIC wajib diisi.',
                'status.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                'startDate'      => 'required',
                'dueDate'      => 'required|after:startDate',
                'idPic'      => 'required',
                'status'      => 'required',
            ];
            $messages = [
                'idTask.required'         => 'Task wajib diisi.',
                'idSubjek.required'      => 'Subjek wajib diisi.',
                'idPic.required'      => 'PIC wajib diisi.',
                'status.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();

                $data = task_pelaporan::firstOrNew(array('idTaskPelaporan' => $request->idTaskPelaporan));
                // dd($request);
                $data->idTask = $request->idTask;
                $data->idSubjek = $request->idSubjek;
                $data->idPic = $request->idPic;
                $data->startDate = $request->startDate;
                $data->dueDate = $request->dueDate;
                if ($request->status == 3) {
                    $data->doneDate = now();
                }
                $data->keterangan = $request->keterangan;
                $data->addUser = Auth::id();
                $data->status = $request->status;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}

    public function excel(Request $request) {

        // return Excel::store(new WorkspaceExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'excel/AzKonvensional.xlsx', 'local');

        return Excel::download(new PelaporanExcel($request->tanggal_awal, $request->tanggal_akhir, $request->pic, $request->dash), 'TaskPelaporan.xlsx');
    }
}
