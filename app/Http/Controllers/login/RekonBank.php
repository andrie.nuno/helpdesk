<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\FaRekonBank;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class RekonBank extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Rekon Bank');
        return view('login.rekonbank.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = FaRekonBank::join('tblcabang', 'tblcabang.idCabang', '=', 'fa_rekonbank.idCabang')
            ->join('tblbank', 'tblbank.idBank', '=', 'fa_rekonbank.idBank')
            ->join('report_banktransaksi', 'report_banktransaksi.kodeTrans', '=', 'fa_rekonbank.noVoucher')
            ->selectRaw('fa_rekonbank.idRekonBank, fa_rekonbank.tanggal, fa_rekonbank.noVoucher, fa_rekonbank.total, fa_rekonbank.isActive, fa_rekonbank.isReversal, fa_rekonbank.statusApproval, fa_rekonbank.dateReversal, fa_rekonbank.isJurnal, fa_rekonbank.isJurnalReversal, tblcabang.kodeCabang, tblcabang.namaCabang, tblbank.namaBank')
            ->orderBy('fa_rekonbank.tanggal', 'ASC')
            ->orderBy('fa_rekonbank.idRekonBank', 'ASC')
            ->groupBy('fa_rekonbank.idRekonBank')
            ->where('fa_rekonbank.isActive', '=', 1)
            ->where('fa_rekonbank.statusApproval', '=', 1)
            ->where('fa_rekonbank.fromOracle', '=', 0);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('fa_rekonbank.tanggal', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('fa_rekonbank.tanggal', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('fa_rekonbank.isJurnal', '=', $request->statusJurnal);
        }
        // dd($query->toSql());
        $data = $query->paginate($limit);
        return view('login.rekonbank.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'   => 1,
                    'dateJurnal' => date('Y-m-d H:i:s'),
                ];
                FaRekonBank::where('idRekonBank', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.rekonbank.createjurnal',[]);
				break;
            case 'jurnalreversal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnalReversal'   => 1,
                    'dateJurnalReversal' => date('Y-m-d H:i:s'),
                ];
                FaRekonBank::where('idRekonBank', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.rekonbank.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idRekonBank', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.rekonbank.jurnal',[
                    "idGadai"               => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
