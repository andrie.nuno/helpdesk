<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\ClosingAcrue;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class Acrue extends Controller
{
    public function index()
    {
        Session::put('breadcrumb', 'Acrue');
        return view('login.acrue.index',[]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = ClosingAcrue::join('trans_gadai', 'trans_gadai.idGadai', '=', 'closing_acrue.idGadai')
            ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'closing_acrue.idGadai')
            ->leftJoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'closing_acrue.idGadai')
            ->leftJoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
            ->selectRaw('closing_acrue.idGadai, closing_acrue.tanggalClosing, closing_acrue.tanggalPencairan, closing_acrue.jumlahHari, closing_acrue.ovd, closing_acrue.pokokAwal, closing_acrue.selisihAcrue, closing_acrue.selisihAcrueBungaMurni, closing_acrue.selisihAcrueBungaSelisih, closing_acrue.isJurnal, trans_gadai.noSbg, trans_payctrl.tanggalPelunasan, trans_gadaijf.idJf, tblsettingjf.namaJf')
            ->orderBy('closing_acrue.idGadai', 'ASC')
            ->where(function ($query2) {
                $query2->where('closing_acrue.selisihAcrue', '>', 0)
                ->orWhere('closing_acrue.selisihAcrueBungaMurni', '>', 0);
            });
        if ($request->idJf) {
            $query = $query->where('trans_gadaijf.idJf', '=', $request->idJf);
        }
        if ($request->statusJurnal) {
            $query = $query->where('closing_acrue.isJurnal', '=', $request->statusJurnal);
        }
        $data = $query->paginate($limit);
        return view('login.acrue.populate',[
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'   => 1,
                    'dateJurnal' => date('Y-m-d H:i:s'),
                ];
                ClosingAcrue::where('idGadai', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.acrue.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.acrue.jurnal',[
                    "idGadai"               => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
