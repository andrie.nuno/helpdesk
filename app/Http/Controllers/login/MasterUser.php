<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\mitra_membercompany;
use App\model\mitra_memberbranch;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_limit;
use App\model\mitra_stle;
use App\model\master_user;
use App\model\task_problem;
use App\model\task_incident;
use App\model\task_pelaporan;
use Illuminate\Support\Facades\Hash;

class MasterUser extends Controller
{
    public function index()
    {
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.masteruser.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = master_user::orderBy('master_user.namaUser');
        // $data = $query->where('master_user.isActive', '=', 1);
        if ($request->nama_user) {
            $data = $query->where('master_user.namaUser', 'like', '%'.$request->nama_user.'%');
        }
        if ($request->status_pic != "") {
            if ($request->status_pic == 1) {
                $data = $query->where('master_user.isActive', 1);
            } elseif ($request->status_pic != 1) {
                $data = $query->where('master_user.isActive', 0);
            }
        }
        $data = $query->selectRaw('master_user.*');
        $data = $query->paginate($limit);
        return view('login.masteruser.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.masteruser.create');
				break;
            case 'edit':
                $data = master_user::find($request->id);
                return view('login.masteruser.edit', [
                    "data"      => $data,
                ]);
                break;
                case 'delete':
                    $data = master_user::find($request->id);
                    if ($data) {
                        $idUser = $data->idUser;
                        $data->delete();

                        $problem = task_problem::where("idPic", $idUser);
                        $incident = task_incident::where("idPic", $idUser);
                        $pelaporan = task_pelaporan::where("idPic", $idUser);
                        $problem->delete();
                        $incident->delete();
                        $pelaporan->delete();

                        return response()->json(['success' => true]);
                    } else {
                        return response()->json(['success' => false]);
                    }
                    break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'branch':
				$idCompany = $request->idCompany;
				$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
				$result = "<option value=''>== Pilih Branch ==</option>";
				if ($branchs) {
					foreach ($branchs as $branch) {
						$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
					}
				}
				echo $result;
				break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'emailUser' => 'required|email|unique:master_user,emailUser',
                'namaUser'      => 'required',
                'password'      => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()_+]+$/',
                'idLevel'       => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'emailUser.required'        => 'Email User wajib diisi.',
                'emailUser.unique'          => 'Email User sudah digunakan oleh user lain.',
                'namaUser.required'         => 'Nama User wajib diisi.',
                'password.required'         => 'Password wajib diisi.',
                'password.string'       => 'Password harus berupa string',
                'password.min'          => 'Password harus minimal 8 karakter',
                'password.regex'          => 'Password harus minimal 8 karakter, mengunakan huruf, dan angka',
                'idLevel.required'          => 'Kelompok Akses wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'emailUser'     => 'required|email|unique:master_user,emailUser,'.$request->idUser.',idUser',
                'namaUser'      => 'required',
                'password' => 'sometimes|nullable|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()_+]+$/',
                'idLevel'       => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'emailUser.required'            => 'Email User wajib diisi.',
                'emailUser.unique'              => 'Email User sudah digunakan oleh user lain.',
                'namaUser.required'         => 'Nama User wajib diisi.',
                'password.string'       => 'Password harus berupa string',
                'password.min'          => 'Password harus minimal 8 karakter',
                'password.regex'          => 'Password harus minimal 8 karakter, mengunakan huruf, dan angka',
                'idLevel.required'          => 'Kelompok Akses wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = master_user::firstOrNew(array('idUser' => $request->idUser));
                // dd($request);
                $data->emailUser = $request->emailUser;
                // if ($data->exists) { // user already exists and was pulled from database.
                //     if ($request->has('passcheck')) {
                //         $data->password = Hash::make("Mulia1D**");
                //     }
                // }
                // if (empty($data->exists)) {
                //     // user created from 'new'; does not exist in database.
                //     $data->password = Hash::make("Mulia1D**");
                // }
                if ($request->password) {
                    $data->password = Hash::make($request->password);
                }
                $data->namaUser = $request->namaUser;
                $data->idLevel = $request->idLevel;
                $data->isActive = $request->isActive;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            if ($request->_action == "Add") {
                echo '<div class="alert alert-success text-center mb-0">Data Berhasil Disimpan ! <br>
            Email : '.$request->emailUser.' <br>
            Password : '.$request->password.'</div>';
            } else {
                echo '<div class="alert alert-success text-center mb-0">Data Berhasil Disimpan !</div>';
            }

            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
