<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\master_kategori;
use App\model\master_task;
use App\model\master_subjek;
use Illuminate\Support\Facades\Hash;

class MasterSubjek extends Controller
{
    public function index()
    {
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.mastersubjek.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = master_subjek::orderBy('master_subjek.namaSubjek');
        $data = $query
            ->join('master_kategori', 'master_kategori.idKategori', '=', 'master_subjek.idKategori')
            ->join('master_task', 'master_task.idTask', '=', 'master_subjek.idTask');
            // ->where('master_subjek.isActive', '=', 1);
        if ($request->status_subjek != "") {
            if ($request->status_subjek == 1) {
                $data = $query->where('master_subjek.isActive', 1);
            } elseif ($request->status_subjek != 1) {
                $data = $query->where('master_subjek.isActive', 0);
            }
        }
        if ($request->nama_subjek) {
            $data = $query->where('master_subjek.namaSubjek', 'like', '%'.$request->nama_subjek.'%');
        }
        $data = $query->selectRaw('master_subjek.*, master_kategori.namaKategori, master_task.namaTask');
        $data = $query->paginate($limit);
        return view('login.mastersubjek.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $kategori = master_kategori::where('isActive', 1)->get();
                $task = master_task::where('isActive', 1)->get();
                return view('login.mastersubjek.create', [
                    "kategoris" => $kategori,
                    "tasks" => $task,
                ]);
				break;
            case 'edit':
                $kategori = master_kategori::where('isActive', 1)->get();
                $task = master_task::where('isActive', 1)->get();
                $data = master_subjek::find($request->id);
                return view('login.mastersubjek.edit', [
                    "kategoris"      => $kategori,
                    "tasks"      => $task,
                    "data"      => $data,
                ]);
                break;
            case 'delete':
                $data = master_subjek::find($request->id);
                if ($data) {
                    $data->delete();

                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'task':
                $idKategori = $request->idKategori;
                $tasks = master_task::where('idKategori',$idKategori)->get();
                // dd($tasks);
                $result = "<option value=''>== Pilih Task ==</option>";
                if ($tasks) {
                    foreach ($tasks as $task) {
                        $result .= "<option value='".$task->idTask."'>".$task->namaTask."</option>";
                    }
                }
                echo $result;
            break;
		}
	}

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'idKategori'      => 'required',
                'idTask'      => 'required',
                'namaSubjek'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Nama Kategori wajib diisi.',
                'idTask.required'         => 'Task wajib diisi.',
                'namaSubjek.required'         => 'Nama Subjek wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'idKategori'      => 'required',
                'idTask'      => 'required',
                'namaSubjek'      => 'required',
                'isActive'      => 'required',
            ];
            $messages = [
                'idKategori.required'         => 'Kategori wajib diisi.',
                'idTask.required'         => 'Task wajib diisi.',
                'namaSubjek.required'         => 'Nama Subjek wajib diisi.',
                'isActive.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        // dd($request);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = master_subjek::firstOrNew(array('idSubjek' => $request->idSubjek));
                // dd($request);
                $data->idKategori = $request->idKategori;
                $data->idTask = $request->idTask;
                $data->namaSubjek = $request->namaSubjek;
                $data->isActive = $request->isActive;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
