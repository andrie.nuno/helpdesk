<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\master_subjek;
use App\model\master_sub_subjek;
use App\model\master_task;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\master_user;
use App\model\task_problem;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\ProblemExcel;
use App\Http\Exports\TaskExcel;
use Carbon\Carbon;

class Problem extends Controller
{

    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        $pic = master_user::where('isActive', '=', 1)->get();
        // $company = mitra_membercompany::orderBy('idMitra')->orderBy('idProvinsi')->get();
        return view('login.problem.index',[
            "wilayahs"       => $wilayah,
            "pics"       => $pic,
        ]);
    }

    public function populate(Request $request) {
        $user = Auth::user();

        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = task_problem::orderBy('task_problem.idTaskProblem');
        $data = $query
        ->leftJoin('tblwilayah','tblwilayah.idWilayah','=','task_problem.idWilayah')
        ->leftJoin('tblcabang','tblcabang.idCabang','=','task_problem.idCabang')
        ->leftJoin("tblcabang AS unit", function($join){
            $join->on('unit.idCabang', '=', 'task_problem.idUnit');
            $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
        })
        ->leftJoin('master_task','master_task.idTask','=','task_problem.idTask')
        ->leftJoin('master_subjek','master_subjek.idSubjek','=','task_problem.idSubjek')
        ->leftJoin('master_sub_subjek','master_sub_subjek.idSubSubjek','=','task_problem.idSubSubjek')
        ->leftJoin('master_user','master_user.idUser','=','task_problem.idPic');
            if ($user->idLevel == 2) {
                $data = $query->where('master_user.idUser', '=', $user->idUser);
            } elseif ($request->id_pic) {
                $data = $query->where('task_problem.idPic', '=', $request->id_pic);
            }
            if ($request->nama_wilayah) {
                $data = $query->where('task_problem.idWilayah', '=', $request->nama_wilayah);
            }
            if ($request->nama_cabang) {
                $data = $query->where('task_problem.idCabang', '=', $request->nama_cabang);
            }
            if ($request->nama_unit) {
                $data = $query->where('task_problem.idUnit', '=', $request->nama_unit);
            }
            if ($request->tanggalAwal) {
                $query = $query->where('task_problem.startDate', '>=', $request->tanggalAwal);
            }
            if ($request->tanggalAkhir) {
                $query = $query->where('task_problem.dueDate', '<=', $request->tanggalAkhir);
            }
            if ($request->nama_task) {
                $data = $query->where('master_task.namaTask', 'like', '%'.$request->nama_task.'%');
            }

            $data = $query->selectRaw('task_problem.*,
            tblwilayah.namaWilayah,
            tblcabang.namaCabang,
            unit.namaCabang as namaUnit,
            master_task.namaTask,
            master_subjek.namaSubjek,
            master_sub_subjek.namaSubSubjek,
            master_user.namaUser,
            CASE
                WHEN task_problem.status != 3 AND task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN
                    CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_problem.dueDate - INTERVAL 24 HOUR) / 24)
                WHEN task_problem.status = 3 AND task_problem.doneDate > task_problem.dueDate THEN
                    CEIL(TIMESTAMPDIFF(HOUR, task_problem.dueDate, task_problem.doneDate) / 24)
                ELSE NULL
            END AS overdueDays');

        if ($request->dash){
            $query->whereRaw('CASE WHEN task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN DATEDIFF(NOW() + INTERVAL 7 HOUR, task_problem.dueDate) ELSE NULL END > 0')
            ->where('task_problem.status', '!=', 3);
        }
        // $query->where('overdueDays', '>', 0);
        // dd($data->toSql());
        $data = $query->paginate($limit);
        // dd($request);
        return view('login.problem.populate',[
            "data"  => $data,
            "limit" => $limit,
            "page"  => $page,
            "tanggal_awal" => $request->tanggalAwal,
            "tanggal_akhir" => $request->tanggalAkhir,
            "pic" => $request->id_pic,
            "nama_wilayah" => $request->nama_wilayah,
            "nama_cabang" => $request->nama_cabang,
            "nama_unit" => $request->nama_unit,
            "nama_task" => $request->nama_task,
            "dash" => $request->dash
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $wilayah = tblwilayah::where('isActive', '=', 1)->orderBy('namaWilayah')->get();
                $cabang = tblcabang::where('isActive', '=', 1)->get();
                $unit = tblcabang::where('idJenisCabang', 5)->where('isActive', '=', 1)->orderBy('namaCabang')->get();
                $task = master_task::where('isActive', '=', 1)->where('idKategori', '=', 3)->orderBy('namaTask')->get();
                $subjek = master_subjek::where('isActive', '=', 1)->orderBy('namaSubjek')->get();
                $subsubjek = master_sub_subjek::where('isActive', '=', 1)->orderBy('namaSubSubjek')->get();
                $pic = master_user::where('isActive', '=', 1)->orderBy('namaUser')->get();
                return view('login.problem.create', [
                    "wilayahs" => $wilayah,
                    "cabangs" => $cabang,
                    "units" => $unit,
                    "tasks" => $task,
                    "subjeks" => $subjek,
                    "subsubjeks" => $subsubjek,
                    "pics" => $pic,
                ]);
				break;
            case 'edit':
                $data = task_problem::find($request->id);
                $wilayah = tblwilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
                $cabang = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                ->where('idWilayah', $data->idWilayah)
                ->where('idJenisCabang', 4)
                ->where('isActive', 1)
                ->whereNull('tglNonAktif')
                ->orderBy('namaCabang')->get();
                $unit = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                ->where('headCabang', $data->idCabang)
                ->where('idJenisCabang', 5)
                ->where('isActive', 1)
                ->whereNull('tglNonAktif')
                ->orderBy('namaCabang')->get();
                $task = master_task::where('isActive', '=', 1)->where('idKategori', '=', 3)->orderBy('namaTask')->get();
                $subjek = master_subjek::where('isActive', '=', 1)->where('idTask', '=', $data->idTask)->orderBy('namaSubjek')->get();
                $subsubjek = master_sub_subjek::where('isActive', '=', 1)->where('idSubjek', '=', $data->idSubjek)->orderBy('namaSubSubjek')->get();
                $pic = master_user::where('isActive', '=', 1)->orderBy('namaUser')->get();
                return view('login.problem.edit', [
                    "data"      => $data,
                    "wilayahs" => $wilayah,
                    "cabangs" => $cabang,
                    "units" => $unit,
                    "tasks" => $task,
                    "subjeks" => $subjek,
                    "subsubjeks" => $subsubjek,
                    "pics" => $pic,
                ]);
                break;
            case 'detail':
                $data = task_problem::join('tblwilayah','tblwilayah.idWilayah','=','task_problem.idWilayah')
                ->join('tblcabang','tblcabang.idCabang','=','task_problem.idCabang')
                ->join("tblcabang AS unit", function($join){
                    $join->on('unit.idCabang', '=', 'task_problem.idUnit');
                    $join->where(DB::raw("unit.idJenisCabang"), "=", 5);
                })
                ->join('master_task','master_task.idTask','=','task_problem.idTask')
                ->join('master_subjek','master_subjek.idSubjek','=','task_problem.idSubjek')
                ->join('master_sub_subjek','master_sub_subjek.idSubSubjek','=','task_problem.idSubSubjek')
                ->join('master_user','master_user.idUser','=','task_problem.idPic')
                ->selectRaw('task_problem.*,
            tblwilayah.namaWilayah,
            tblcabang.namaCabang,
            unit.namaCabang as namaUnit,
            master_task.namaTask,
            master_subjek.namaSubjek,
            master_sub_subjek.namaSubSubjek,
            master_user.namaUser,
            CASE
                WHEN task_problem.status != 3 AND task_problem.dueDate < NOW() + INTERVAL 7 HOUR THEN
                    CEIL(TIMESTAMPDIFF(HOUR, NOW() + INTERVAL 7 HOUR, task_problem.dueDate - INTERVAL 24 HOUR) / 24)
                WHEN task_problem.status = 3 AND task_problem.doneDate > task_problem.dueDate THEN
                    CEIL(TIMESTAMPDIFF(HOUR, task_problem.dueDate, task_problem.doneDate) / 24)
                ELSE NULL
            END AS overdueDays')
                ->where('task_problem.idTaskProblem', '=', $request->id)
                ->first();
                // dd($data);
                return view('login.problem.detail',[
                    // "idTaskProblem"         => $request->id,
                    "data"                  => $data,
                ]);
                break;
                case 'dokumen':
                    return view('login.problem.dokumen',[
                        // "idTaskProblem"         => $request->id,
                        "data"                  => $request->id,
                    ]);
                    break;
            case 'delete':
                $data = task_problem::firstOrNew(array('id' => $request->id));
                $data->status = 0;
                $data->isTampil = 0;
                $data->save();
                echo '<script>closeMultiModal(1);doSearch("Populate");swal("Sukses!", "Proses Berhasil", "success")</script>';
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
				$cabang = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('idWilayah', $idWilayah)
                    ->where('idJenisCabang', 4)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('namaCabang')
                    ->get();
				$result1 = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabang) {
					foreach ($cabang as $branch) {
						$result1 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
					}
				}
				echo $result1;
				break;
            case 'unit':
                $idCabang = $request->idCabang;
                $branchs = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('headCabang', $idCabang)
                    ->where('idJenisCabang', 5)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('namaCabang')->get();
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($branchs) {
                    foreach ($branchs as $branch) {
                        $result2 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
            case 'subjek':
                $idTask = $request->idTask;
                $subjeks = master_subjek::where('idTask',$idTask)->where('isActive', 1)->get();
                // dd($subjeks);
                $result3 = "<option value=''>== Pilih Subjek ==</option>";
                if ($subjeks) {
                    foreach ($subjeks as $subjek) {
                        $result3 .= "<option value='".$subjek->idSubjek."'>".$subjek->namaSubjek."</option>";
                    }
                }
                echo $result3;
            break;
            case 'subsubjek':
                $idSubjek = $request->idSubjek;
                $subsubjeks = master_sub_subjek::where('idSubjek',$idSubjek)->where('isActive', 1)->get();
                // dd($subsubjeks);
                $result4 = "<option value=''>== Pilih Sub Subjek ==</option>";
                if ($subsubjeks) {
                    foreach ($subsubjeks as $subsubjek) {
                        $result4 .= "<option value='".$subsubjek->idSubSubjek."'>".$subsubjek->namaSubSubjek."</option>";
                    }
                }
                echo $result4;
            break;
		}
	}

    public function proses(Request $request) {
        // dd($request);
        if ($request->_action == "Add") {
            $rules = [
                'idWilayah'      => 'required',
                'idCabang'      => 'required',
                'idUnit'      => 'required',
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                // 'idSubSubjek'      => 'required',
                'idPic'      => 'required',
                'startDate'      => 'required',
                'dueDate'      => 'required|after:startDate',
                'dokumen' => 'mimes:jpg,jpeg,png,pdf',
                'status'      => 'required',
            ];
            $messages = [
                'idWilayah.required'         => 'Wilayah wajib diisi.',
                'idCabang.required'      => 'Cabang wajib diisi.',
                'idUnit.required'      => 'Unit wajib diisi.',
                'idTask.required'      => 'Task wajib diisi.',
                'idSubjek.required'      => 'Subjek wajib diisi.',
                // 'idSubSubjek.required'      => 'Sub Subjek wajib diisi.',
                'idPic.required'      => 'PIC wajib diisi.',
                'startDate.required'      => 'Start Date wajib diisi.',
                'dueDate.required'      => 'Due Date wajib diisi.',
                'status.required'         => 'Status wajib diisi.',
            ];
        } else {
            $rules = [
                'idWilayah'      => 'required',
                'idCabang'      => 'required',
                'idUnit'      => 'required',
                'idTask'      => 'required',
                'idSubjek'      => 'required',
                // 'idSubSubjek'      => 'required',
                'idPic'      => 'required',
                'startDate'      => 'required',
                'dueDate'      => 'required|after:startDate',
                'dokumen' => 'mimes:jpg,jpeg,png,pdf',
                'status'      => 'required',
            ];
            $messages = [
                'idWilayah.required'         => 'Wilayah wajib diisi.',
                'idCabang.required'      => 'Cabang wajib diisi.',
                'idUnit.required'      => 'Unit wajib diisi.',
                'idTask.required'      => 'Task wajib diisi.',
                'idSubjek.required'      => 'Subjek wajib diisi.',
                // 'idSubSubjek.required'      => 'Sub Subjek wajib diisi.',
                'idPic.required'      => 'PIC wajib diisi.',
                'startDate.required'      => 'Start Date wajib diisi.',
                'dueDate.required'      => 'Due Date wajib diisi.',
                'status.required'         => 'Status wajib diisi.',
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();

                if ($request->dokumen != "") {
                    $image = "data:image/".$request->dokumen->getClientOriginalExtension().";base64,".base64_encode(file_get_contents($request->dokumen));
                    $urlImage_S3 = helpers::saveimgS3($image,'/uploads/','/image-asset/',$request->dokumen->getClientOriginalExtension());
                }

                $data = task_problem::firstOrNew(array('idTaskProblem' => $request->idTaskProblem));
                // dd($request);
                $data->idWilayah = $request->idWilayah;
                $data->idCabang = $request->idCabang;
                $data->idUnit = $request->idUnit;
                $data->idTask = $request->idTask;
                $data->idSubjek = $request->idSubjek;
                $data->idSubSubjek = $request->idSubSubjek;
                $data->idPic = $request->idPic;
                $data->startDate = $request->startDate;
                $data->dueDate = $request->dueDate;
                $data->keterangan = $request->keterangan;
                if ($request->dokumen != "") {
                    $data->dokumen = $urlImage_S3;
                }
                if ($request->status == 3) {
                    $data->doneDate = now();
                }
                $data->addUser = Auth::id();
                $data->status = $request->status;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}

    public function excel(Request $request) {

        // return Excel::store(new WorkspaceExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'excel/AzKonvensional.xlsx', 'local');

        return Excel::download(new ProblemExcel($request->tanggal_awal, $request->tanggal_akhir, $request->pic, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit, $request->dash), 'TaskProblem.xlsx');
    }

    public function taskexcel(Request $request) {

        return Excel::download(new TaskExcel($request->tanggal_awal, $request->tanggal_akhir, $request->pic, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit, $request->nama_task, $request->dash), 'TaskExcel.xlsx');
    }
}
