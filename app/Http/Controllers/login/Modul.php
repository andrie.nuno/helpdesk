<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\model\menuads;
use App\model\menuakses;
use App\model\mitra_level;
use DB;
use Exception;
use Session;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\CustomClass\helpers;

class Modul extends Controller {
    
    public function index() {
        return view('login.modul.index');
    }

    public function populate() {
        $data = mitra_level::All();
        return view('login.modul.populate',[
            "data"  => $data
        ]);
    }
    
    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.modul.create');				
				break;	
            case 'edit':
                $data = mitra_level::find($request->id);
                return view('login.modul.edit', [
                    "data"  => $data
                ]);
                break;
            case 'hakakses':
                $modul = mitra_level::find($request->id);
                $menu = menuads::headerAkses($request->id);
                return view('login.modul.createakses', [
                    "modul"     => $modul, 
                    "menus"     => $menu
                ]);
                break;
		}
    }
    
    public function proses(Request $request) {
        $rules = [
            'namaLevel'     => 'required',
            'isActive'      => 'required',
        ]; 
        $messages = [
            'namaLevel.required'    => 'Nama Modul wajib diisi.',
            'isActive.required'     => 'Status wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = mitra_level::firstOrNew(array('idLevel' => $request->idLevel));
                $data->namaLevel = $request->namaLevel;
                $data->isActive = $request->isActive;
                $data->addUser = Auth::id();
                $data->save();           
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Modul");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Modul");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
    }
    
    public function prosesakses(Request $request) {
        try{
            DB::beginTransaction();
            // hapus data
            $menu = menuakses::where('idLevel', $request->idLevel)
                    ->first();
            if ($menu != null) {
                $hapus = menuakses::find($request->idLevel);
                $hapus->delete();
            }
            // proses form input
            $akses = $request->menuId;
            $jumlahakses = count($akses);
            for($i=0; $i<$jumlahakses; $i++) {
                $data = array(
                    'idLevel'   => $request->idLevel, 
                    'menuId'    => $akses[$i]
                );
                menuakses::create($data);
            }
            // if ($menu != null) {
            //     $hapus = ads_modulakses::find($request->idModul);
            //     $hapus->delete();
            // }
            // $akses = $request->idMenu;
            // $jumlahakses = count($akses);
            // for($i=0; $i<$jumlahakses; $i++) {
            //     $data = array(
            //         'idModul'   => $request->idModul, 
            //         'idMenu'    => $akses[$i]
            //     );
            //     ads_modulakses::create($data);
            // }
        } catch (\Exception  $e) {
            DB::rollback();
            echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
            return FALSE;
        } catch (\Throwable  $e) {
            DB::rollback();
            echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
            return FALSE;
        }
        DB::commit();
        echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
        echo '<script>doSearch("Modul");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
    }
}
