<?php

// ResetPasswordController.php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Hash;


class ResetPasswordController extends Controller
{
    public function showResetForm($token)
    {
       // Retrieve the email associated with the given token
        $passwordReset = \DB::table('password_resets')
        ->where('token', $token)
        ->first();

        if (!$passwordReset) {
        // Handle the case where the token is not found
        abort(404);
        }

        $email = $passwordReset->email;

        return view('auth.passwords.reset', [
        'token' => $token,
        'emailUser' => $email,
        ]);
    }


    public function reset(Request $request)
    {

        $messages = [
            'emailUser.required' => 'Email is required.',
            'emailUser.email' => 'Email must be a valid email address.',
            'password.required' => 'Password is required.',
            'password.confirmed' => 'Konfirmasi password tidak sesuai',
            'password.min' => 'Minimal 8 karakter, menggunakan huruf dan angka',
            'password.regex' => 'Minimal 8 karakter, menggunakan huruf dan angka',
            'token.required' => 'Token is required.'
        ];
        // Validate the request
        $request->validate([
            'emailUser' => 'required|email',
            'password' => 'required|confirmed|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d!@#$%^&*()_+]+$/',
            'password_confirmation' => 'required',
            'token' => 'required'
        ], $messages);

        // Attempt to reset the password
        $response = Password::reset($request->only(
            'emailUser', 'password', 'password_confirmation', 'token'
        ), function ($user, $password) {
            // Update the user's password

            $user->password = Hash::make($password);
            $user->save();
        });

        // Check the response from the password broker
        if ($response == Password::PASSWORD_RESET) {
            // Password reset successful
            return response()->json(['status' => 'success']);
            return redirect()->route('login');
        } else {
            // Password reset failed
            // dd($request);
            Session::flash('error', 'Password reset gagal, silakan coba lagi!');
            return back()->withInput($request->only('emailUser'));


        }
    }
}
