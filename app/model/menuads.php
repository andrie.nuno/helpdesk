<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class menuads extends Model
{
    protected $table = "ads_menu";
    protected $primaryKey = 'menuId';
    protected $fillable = [
        'menuNama', 'menuSegmen', 'menuUrl', 'menuNav', 'menuUrut', 'headerId', 'isActive', 'addUser'
    ];

    static function header() {
        $data = menuads::where('menuNav','Header')
                ->orderBy('menuUrut', 'asc')
                ->get();
        $headers = $data;
        $i=0;
        foreach($headers as $header) {
            $headers[$i]->sub = menuads::child($header->menuId);
            $i++;
        }
        return $headers;
    }

	static function child($id) {
        $data = menuads::where('headerId',$id)
                ->orderBy('menuUrut', 'asc')
                ->get();
        $childs = $data;
        return $childs;
    }

    static function headerAktif($idLevel) {
        $data = menuads::select('ads_menu.*')
                ->join('ads_menu_akses', 'ads_menu_akses.menuId', '=', 'ads_menu.menuId')
                ->where('ads_menu.menuNav','Header')
                ->where('ads_menu.isActive',1)
                ->where('ads_menu_akses.idLevel',$idLevel)
                ->orderBy('ads_menu.menuUrut', 'asc')
                ->get();
        $headers = $data;
        $i=0;
        foreach($headers as $header) {
            $headers[$i]->sub = menuads::childAktif($header->menuId, $idLevel);
            $i++;
        }
        return $headers;
    }

	static function childAktif($id, $idLevel) {
        $data = menuads::select('ads_menu.*')
                ->join('ads_menu_akses', 'ads_menu_akses.menuId', '=', 'ads_menu.menuId')
                ->where('ads_menu.headerId',$id)
                ->where('ads_menu.isActive',1)
                ->where('ads_menu_akses.idLevel',$idLevel)
                ->orderBy('ads_menu.menuUrut', 'asc')
                ->get();
        $childs = $data;
        return $childs;
    }

    static function headerAkses($idLevel) {
        $data = menuads::select('ads_menu.*','ads_menu_akses.menuId AS akses')
                ->leftJoin('ads_menu_akses', function($join) use ($idLevel) {
                    $join->on('ads_menu.menuId', '=', 'ads_menu_akses.menuId')
                        ->where('ads_menu_akses.idLevel', '=', $idLevel);
                })
                ->where('ads_menu.menuNav','Header')
                ->orderBy('ads_menu.menuUrut', 'asc')
                ->get();
        $headers = $data;
        $i=0;
        foreach($headers as $header) {
            $headers[$i]->sub = menuads::childAkses($header->menuId, $idLevel);
            $i++;
        }
        return $headers;
    }

	static function childAkses($id, $idLevel) {
        $data = menuads::select('ads_menu.*','ads_menu_akses.menuId AS akses')
                ->leftJoin('ads_menu_akses', function($join) use ($idLevel) {
                    $join->on('ads_menu.menuId', '=', 'ads_menu_akses.menuId')
                        ->where('ads_menu_akses.idLevel', '=', $idLevel);
                })
                ->where('ads_menu.headerId',$id)
                ->orderBy('ads_menu.menuUrut', 'asc')
                ->get();
        $childs = $data;
        return $childs;
    }

    static function getNameByIdMenu($id){
        $data=menuads::where('menuId',$id)
        ->first();
        return $data->menuNama;
    }

    static function getNameBySegmenMenu($segmen){
        $data=menuads::where('menuSegmen',$segmen)
        ->first();
        return $data->menuNama;
    }
}
