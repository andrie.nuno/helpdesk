<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FaRekonBank extends Model
{
    protected $table = "fa_rekonbank";
	protected $primaryKey = 'idRekonBank';
    protected $fillable = [
        'idRekonBank', 
        'idCabang', 
        'idBank', 
        'tanggal', 
	    'noVoucher', 
        'tglCreated', 
	    'keterangan', 
	    'total', 
        'keteranganApproval', 
        'idApproval', 
        'statusApproval', 
        'isActive', 
        'isReversal', 
        'dateReversal', 
        'keteranganReversal', 
        'fromOracle', 
        'coaOracle', 
        'isJurnal', 
        'dateJurnal', 
        'isJurnalReversal', 
        'dateJurnalReversal', 
    ];
}