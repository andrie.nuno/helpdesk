<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_pekerjaan extends Model
{
    protected $table = "master_pekerjaan";
    protected $primaryKey = 'idPekerjaan';
    protected $keyType = 'string';
    protected $fillable = [
        'namaPekerjaan', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_pekerjaan::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_pekerjaan::where('idPekerjaan',$id)
        ->first();
        return $data;
    }
}