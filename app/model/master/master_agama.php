<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_agama extends Model
{
    protected $table = "master_agama";
    protected $primaryKey = 'idAgama';
    protected $fillable = [
        'namaAgama', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_agama::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_agama::where('idAgama',$id)
        ->first();
        return $data;
    }
}