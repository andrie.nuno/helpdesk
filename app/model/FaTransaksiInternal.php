<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FaTransaksiInternal extends Model
{
    protected $table = "fa_transaksiinternal";
	protected $primaryKey = 'idTransaksiInternal';
    protected $fillable = [
        'idTransaksiInternal',  
        'idCabang', 
        'idCabangKeluar', 
	    'idJenisTransaksi', 
	    'idBankKeluar', 
	    'idBankMasuk', 
	    'tanggal', 
        'tahun', 
        'kodeTrans', 
        'urutVoucher', 
        'noVoucher', 
        'saldoBankMasuk', 
        'saldoBankKeluar', 
        'nominalTransaksi', 
        'keteranganTransaksi', 
        'idCreated', 
        'dateCreated', 
        'denomA', // 'Denominasi 100.000'
        'denomB', // 'Denominasi 50.000'
        'denomC', // 'Denominasi 20.000'
        'denomD', // 'Denominasi 10.000'
        'denomE', // 'Denominasi 5.000'
        'denomF', // 'Denominasi 2.000'
        'denomG', // 'Denominasi 1.000'
        'denomH', // 'Denominasi 500'
        'denomI', // 'Denominasi 200'
        'denomJ', // 'Denominasi 100'
        'denomK', // 'Denominasi 50'
        'keteranganApproval', 
        'imageApproval', 
        'selfieApproval', 
        'idApproval', 
        'dateApproval', 
        'created_at', 
        'updated_at', 
        'statusApproval', 
        'fileKwitansi', 
        'nomorCek', 
        'prosesTransaksi', 
        'denomL', 
        'idCashflow', 
        'fromOracle', 
        'idGiroCek', 
        'isJurnal', 
        'dateJurnal', 
    ];
}