<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class member_mitra extends Model
{
    protected $connection = 'mysql';
    protected $table = "ads_member_mitra";
    protected $primaryKey = 'idMitra';
    protected $fillable = [
        'kodeMitra', 'namaMitra', 'alamatMitra', 'idKota', 'idProvinsi', 'kodeposMitra', 'jenisMitra', 'isActive'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=member_mitra::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=member_mitra::where('idMitra',$id)
        ->first();
        return $data;
    }

    public function updateData($id,$update){
        $data = member_mitra::where('idMitra',$id)->first();

        $data->kodeMitra= $update['kodeMitra'];
        $data->namaMitra=$update['namaMitra'];
        $data->alamatMitra=$update['alamatMitra'];
        $data->idKota=$update['idKota'];
        $data->idProvinsi=$update['idProvinsi'];
        $data->kodeposMitra=$update['kodeposMitra'];
        $data->jenisMitra=$update['jenisMitra'];
        $data->isActive=$update['isActive'];
        
        $data->save();
    }

    public function insertData($query){
        $data= member_mitra::newInstance($query);
        $data->save();
        return $data;
    }
}