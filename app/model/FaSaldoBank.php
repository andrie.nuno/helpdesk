<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FaSaldoBank extends Model
{
    protected $table = "fa_saldobank";
	protected $primaryKey = 'idSaldoBank';
    protected $fillable = [
        'idSaldoBank', 
        'idBank', 
        'idCabang', 
        'tanggal', 
	    'saldoAwal', 
	    'saldoMasuk', 
	    'saldoKeluar', 
	    'saldoAkhir', 
    ];
}