<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class task_incident extends Model
{
    protected $connection = 'mysql';
    protected $table = "task_incident";
    protected $primaryKey = 'idTaskIncident';
    protected $fillable = [
        'idTaskIncident', 'idWilayah', 'idCabang', 'idUnit', 'idTask', 'idSubjek', 'idPic', 'startDate', 'dueDate', 'keterangan', 'status', 'addUser'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getAllData(){
    //     $data=task_incident::
    //     // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
    //     // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
    //     get();
    //     return $data;
    // }

    // public function getData($id){
    //     $data=task_incident::where('id',$id)
    //     ->first();
    //     return $data;
    // }

    // public function updateData($id,$update){
    //     $data = task_incident::where('id',$id)->first();
    //     if($update['password']!=null){
    //         $data->password= Hash::make($update['password']);
    //     }
    //     $data->email= $update['email'];
    //     $data->namaUser=$update['namaUser'];
    //     $data->hp=$update['hp'];
    //     $data->idMitra=$update['idMitra'];
    //     $data->idCompany=$update['idCompany'];
    //     $data->idBranch=$update['idBranch'];
    //     $data->idLevel=$update['idLevel'];
    //     $data->isActive=$update['isActive'];
    //     $data->addUser=$update['addUser'];
    //     $data->save();
    // }

    // public function insertData($query){
    //     $data= task_incident::newInstance($query);
    //     $data->save();
    //     return $data;
    // }
}
