<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class master_task extends Model
{
    protected $connection = 'mysql';
    protected $table = "master_task";
    protected $primaryKey = 'idTask';
    protected $fillable = [
        'idTask', 'idKategori', 'namaTask', 'isUpload', 'isActive'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getAllData(){
    //     $data=master_task::
    //     // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
    //     // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
    //     get();
    //     return $data;
    // }

    // public function getData($id){
    //     $data=master_task::where('id',$id)
    //     ->first();
    //     return $data;
    // }

    // public function updateData($id,$update){
    //     $data = master_task::where('id',$id)->first();
    //     if($update['password']!=null){
    //         $data->password= Hash::make($update['password']);
    //     }
    //     $data->email= $update['email'];
    //     $data->namaUser=$update['namaUser'];
    //     $data->hp=$update['hp'];
    //     $data->idMitra=$update['idMitra'];
    //     $data->idCompany=$update['idCompany'];
    //     $data->idBranch=$update['idBranch'];
    //     $data->idLevel=$update['idLevel'];
    //     $data->isActive=$update['isActive'];
    //     $data->addUser=$update['addUser'];
    //     $data->save();
    // }

    // public function insertData($query){
    //     $data= master_task::newInstance($query);
    //     $data->save();
    //     return $data;
    // }
}
