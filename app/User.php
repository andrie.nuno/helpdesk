<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = "tbluser";
    // protected $primaryKey = 'idUser';
    // protected $fillable = [
    //     'idUser', 'email', 'userName', 'password', 'idModul', 'isActive','userEdit','hp'
    // ];
    protected $table = "master_user";
    protected $primaryKey = 'idUser';
    protected $fillable = [
        'idUser', 'idLevel', 'namaUser', 'emailUser', 'password', 'google2fa_secret', 'isActive'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'google2fa_secret'
    ];

    /**
     * Get the email address for password reset.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->emailUser;
    }

    /**
     * Route notifications for the Email channel.
     *
     * @return string
     */
    public function routeNotificationForEmail()
    {
        return $this->emailUser;
    }

}
