<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\AzKonvensionalExcel;

class AzKonvensionalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tanggal = date('Y-m-d');
        $fileName = "AzKonvensional";
        $extension = "xlsx";

        $fileNameWithExtension = "excel/$fileName-$tanggal.$extension";
        Excel::store(new AzKonvensionalExcel($tanggal, '', '', ''), $fileNameWithExtension, 'local');
    }
}
