<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Http\Exports\OutstandingPinjamanExcel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class exportOutstandingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $tanggal;
    protected $nama_company;
    protected $nama_branch;
    protected $rate_pendana;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tanggal, $nama_company, $nama_branch, $rate_pendana)
    {
        $this->tanggal = $tanggal;
        $this->nama_company = $nama_company;
        $this->nama_branch = $nama_branch;
        $this->rate_pendana = $rate_pendana;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new OutstandingPinjamanExcel($this->tanggal, $this->nama_company, $this->nama_branch, $this->rate_pendana))->store('public/OutstandingPinjaman.xlsx');
    }
}
