<?php
namespace App\CustomClass;
use Ramsey\Uuid\Uuid;
use App\model\menuads;
use App\model\agreement_peminjam;
use App\model\master\master_bank;
use App\model\master\master_pendidikan;
use App\model\master\master_pekerjaan;
use App\model\master\master_statusperkawinan;
use App\model\master\master_agama;
use App\model\master\master_jabatan;
use App\model\master\master_jeniskelamin;
use App\model\master\master_kabupaten;
use App\model\master\master_provinsi;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_user;
use App\model\payctrl;
use App\model\kasbank;
use App\model\master\master_ovd;
use App\model\payctrl_log;
use App\model\tblmenu;
use App\model\tblmodul_list;
use App\model\tblvendor;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class helpers
{
    static function leftMenu() {
        $data = menuads::headerAktif(Auth::user()->idLevel);
        return $data;
    }

    static function getModulList(){
        $data = tblmodul_list::getModul_listById(Auth::user()->idModul);
        return empty($data) ? null : $data;
    }
    static function getNameMenu($id){
        $data = tblmenu::getNameByIdMenu($id);
        return empty($data) ? null : $data;
    }
    static function getSegmenNameMenu($id){
        $data = menuads::getNameBySegmenMenu($id);
        return empty($data) ? null : $data;
    }
    static function getModulList2($idMenu){
        $data = tblmodul_list::getModul_listById2(Auth::user()->idModul,$idMenu);
        return empty($data) ? null : $data;
    }

    static function getmasterbank($id){
        $master_bank = new master_bank;
        $data = $master_bank->getdata($id);
        return isset($data) ? $data->keterangan : $id;
    }

    static function getmasterpendidikan($id){
        $master_pendidikan = new master_pendidikan;
        $data = $master_pendidikan->getdata($id);
        return isset($data) ? $data->namaPendidikan : $id;
    }

    static function getmasterpekerjaan($id){
        $master_pekerjaan = new master_pekerjaan;
        $data = $master_pekerjaan->getdata($id);
        return isset($data) ? $data->namaPekerjaan : $id;
    }

    static function getmasterstatusperkawinan($id){
        $master_statusperkawinan = new master_statusperkawinan;
        $data = $master_statusperkawinan->getdata($id);
        return isset($data) ? $data->namaStatusPerkawinan : $id;
    }

    static function getmasteragama($id){
        $master_agama = new master_agama;
        $data = $master_agama->getdata($id);
        return isset($data) ? $data->namaAgama : $id;
    }

    static function getmasterjabatan($id){
        $master_jabatan = new master_jabatan;
        $data = $master_jabatan->getdata($id);
        return isset($data) ? $data->namaJabatan : $id;
    }

    static function getmasterjeniskelamin($id){
        $master_jeniskelamin = new master_jeniskelamin;
        $data = $master_jeniskelamin->getdata($id);
        return isset($data) ? $data->namaJenisKelamin : $id;
    }

    static function getmasterkota($id){
        $master_kabupaten = new master_kabupaten();
        $data = $master_kabupaten->getdata($id);
        return isset($data) ? $data->namaKabupaten : $id;
    }

    static function getmasterprovinsi($id){
        $master_provinsi = new master_provinsi();
        $data = $master_provinsi->getdata($id);
        return isset($data) ? $data->namaProvinsi : $id;
    }

    static function getusermitra($id){
        $mitra_user = new mitra_user;
        $data = $mitra_user->getdata($id);
        return isset($data) ? $data->namaUser : $id;
    }

    static function getstatussp($ovd){
        $master_ovd = new master_ovd;
        $status = 'Overdue';
        $data = $master_ovd->getAllData();
        foreach($data as $d){
            if($d->hariOvd<$ovd){
                $status = $d->keteranganOvd;
            }else{
                break;
            }
        }
        return $status;
    }

    static function getpayctrl_log($id){
        $data = new payctrl_log;
        $data = $data->getData($id);
        return $data;
    }

    static function getmembermitra($id){
        $member_mitra = new member_mitra();
        $data = $member_mitra->getdata($id);
        return isset($data) ? $data->namaMitra : $id;
    }

    static function getvendor($id){
        $tblvendor = new tblvendor();
        $data = $tblvendor->getdata($id);
        return isset($data) ? $data->namaVendor : $id;
    }

    static function getmembercompany($id){
        $member_company = new member_company();
        $data = $member_company->getdata($id);
        return isset($data) ? $data->namaCompany : $id;
    }

    static function getmemberbranch($id){
        $member_branch = new member_branch();
        $data = $member_branch->getdata($id);
        return isset($data) ? $data->namaBranch : $id;
    }

    static function getmitralevel($id){
        $mitra_level = new mitra_level();
        $data = $mitra_level->getdata($id);
        return isset($data) ? $data->namaLevel : $id;
    }

    static function getpayctrl($id){
        $payctrl = new payctrl();
        $data = $payctrl->getborrowerpayctrl($id);
        return isset($data) ? $data->namaBorrower : $id;
    }

    static function getTotalDepositLender($id){
        $data = kasbank::where('idUserClient', '=', $id)
            ->whereIn('kdTrans', array('DEP', 'OTH1'))
            ->selectRaw('SUM(amount) AS deposit')
            ->groupBy('idUserClient')
            ->first();
        return isset($data) ? $data->deposit : 0;
    }

    static function getTotalVoucherLender($id){
        $data = kasbank::where('idUserClient', '=', $id)
            ->where('kdTrans', '=', 'VCH')
            ->selectRaw('SUM(amount) AS voucher')
            ->groupBy('idUserClient')
            ->first();
        return isset($data) ? $data->voucher : 0;
    }

    // PAGINATION START
    static function pagination($totalData, $limit, $page) {
        return view('layouts.paging',[
            "totalData"     => $totalData,
            "limit"         => $limit,
            "page"          => $page,
        ]);
    }

    static function paginationModal($totalData, $limit, $page) {
        return view('layouts.pagingmodal',[
            "totalData"     => $totalData,
            "limit"         => $limit,
            "page"          => $page,
        ]);
    }

    static function get_no($limit, $page){
        return (($limit * $page) - $limit) + 1;
    }
    // PAGINATION END

    static function saveimgresizeS3($img, $path, $s3){
        try {
            $uuid4          = Uuid::uuid4();
            $uuidfresh      = date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            // list($width, $height) = getimagesize($img);
            // if ($width >= $height) {
            //     $newwidth = 900;
            //     $newheight = 600;
            // } else {
            //     $newwidth = 600;
            //     $newheight = 900;
            // }
            $storagePath    = Storage::disk('publicupload')->putFileAs($path, $img, $uuidfresh.'.jpg');
            $pathfile       = base_path('public'.$path.$uuidfresh.'.jpg');
            // $image          = Image::make($pathfile)->resize($newwidth, $newheight);
            // $result         = $image->save($pathfile);
            // upload ktp to S3
            // jalankan dulu : composer require league/flysystem-aws-s3-v3
            // atau ini : composer require league/flysystem-aws-s3-v3 "~1.0"
            $file           = base_path('public'.$path.$uuidfresh.'.jpg');
            $s3Path         = $s3.$uuidfresh.'.jpg';
            $storageS3      = Storage::disk('s3upload')->put($s3Path, file_get_contents($file), 'public');
            $s3Link         = "http://nos.jkt-1.neo.id/appsdanain-borrower".$s3Path;
            // delete local file
            if(Storage::disk('publicupload')->exists($path.$uuidfresh.'.jpg')){
                Storage::disk('publicupload')->delete($path.$uuidfresh.'.jpg');
            }
            return $s3Link;
            // return $s3Link;
        }
        catch (\Exception $e){
            return $e;
        }
    }

    static function saveimgS3($img, $path, $s3, $extension){
        try {
            $uuid4          = Uuid::uuid4();
            $uuidfresh      = date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            $storagePath    = Storage::disk('publicupload')->putFileAs($path, $img, $uuidfresh.'.'.$extension);
            $pathfile       = base_path('public'.$path.$uuidfresh.'.'.$extension);
            $file           = base_path('public'.$path.$uuidfresh.'.'.$extension);
            $s3Path         = $s3.$uuidfresh.'.'.$extension;
            $storageS3      = Storage::disk('s3upload')->put($s3Path, file_get_contents($file), 'public');
            $s3Link         = "https://nos.jkt-1.neo.id/appsdanain-borrower".$s3Path;
            // delete local file
            if(Storage::disk('publicupload')->exists($path.$uuidfresh.'.'.$extension)){
                Storage::disk('publicupload')->delete($path.$uuidfresh.'.'.$extension);
            }
            return $s3Link;
        }
        catch (\Exception $e){
            return $e;
        }
    }

    public static function namaHari($hari)
    {
        switch ($hari) {
            case 'Sunday':
              return 'Minggu';
            case 'Monday':
              return 'Senin';
            case 'Tuesday':
              return 'Selasa';
            case 'Wednesday':
              return 'Rabu';
            case 'Thursday':
              return 'Kamis';
            case 'Friday':
              return 'Jumat';
            case 'Saturday':
              return 'Sabtu';
            default:
              return 'hari tidak valid';
        }
    }

    public static function namaBulan($bulan)
    {
        switch ($bulan) {
            case 1:
              return 'Januari';
            case 2:
              return 'Februari';
            case 3:
              return 'Maret';
            case 4:
              return 'April';
            case 5:
              return 'Mei';
            case 6:
              return 'Juni';
            case 7:
              return 'Juli';
            case 8:
              return 'Agustus';
            case 9:
              return 'September';
            case 10:
              return 'Oktober';
            case 11:
              return 'November';
            case 12:
              return 'Desember';
            default:
              return 'bulan tidak valid';
        }
    }

    public static function tglLongIndo($tanggal)
    {
        $tgl = explode("-",$tanggal);
        return $tgl[2]." ".helpers::namaBulan($tgl[1])." ".$tgl[0];
    }

    public static function sembilanAngka($angka){
      if ($angka < 10) {
          return '00000000'.$angka;
      } else if ($angka < 100) {
          return '0000000'.$angka;
      } else if ($angka < 1000) {
          return '000000'.$angka;
      } else if ($angka < 10000) {
          return '00000'.$angka;
      } else if ($angka < 100000) {
          return '0000'.$angka;
      } else if ($angka < 1000000) {
          return '000'.$angka;
      } else if ($angka < 10000000) {
          return '00'.$angka;
      } else if ($angka < 100000000) {
          return '0'.$angka;
      } else {
          return $angka;
      }
  }
}
