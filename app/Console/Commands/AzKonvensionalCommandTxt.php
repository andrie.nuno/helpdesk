<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\model\trans_payctrl;
use DB;

class AzKonvensionalCommandTxt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportaz:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export AzKonvensional data to txt, upload to S3, and log file URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $tanggal = date("Y-m-d", strtotime("yesterday"));
            
            $data = $this->getData($tanggal);

            $exportedData = $this->formatData($data);

            $filename = "export-azkonvensional/exported_data_az_".$tanggal.".txt";
            Storage::put($filename, $exportedData);
            $contentType = 'application/json';

            $s3Url = Storage::disk('s3')->put($filename, $exportedData, 'public', ['Content-Type' => $contentType]);
            $url = "https://gadaimas.nos.wjv-1.neo.id/".$filename;

            $this->logFileUrl($s3Url, $url, $tanggal);
        } catch (\Exception $e) {
            $this->error("An error occurred: {$e->getMessage()}");
        }
    }

    protected function getData($tanggal)
    {
        $query = trans_payctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_payctrl.idCustomer')
            ->leftjoin('tblkelurahan', 'tblkelurahan.idKelurahan', '=', 'tblcustomer.idKelurahanKtp')
            ->leftjoin('tblkelurahan as domisili', 'domisili.idKelurahan', '=', 'tblcustomer.idKelurahanDomisili')
            ->leftjoin('tblpekerjaan', 'tblpekerjaan.idPekerjaan', '=', 'tblcustomer.idPekerjaan')
            ->leftjoin('tblsektorekonomi', 'tblsektorekonomi.idSektorEkonomi', '=', 'tblcustomer.idSektorEkonomi')
            ->join('tran_taksiran', 'tran_taksiran.idTaksiran', '=', 'trans_gadai.idTaksiran')
            ->leftjoin('tran_fapg', 'tran_fapg.idFAPG', '=', 'trans_gadai.idFAPG')
            ->leftJoin('trans_payment_schedule', function ($join) use ($tanggal) {
                $join->on('trans_payment_schedule.idGadai', '=', 'trans_payctrl.idGadai')
                ->where(function ($query) use ($tanggal) {
                    $query->whereNull('trans_payment_schedule.tglLunas')
                    // $dd2 = dd($tanggal);
                        ->orWhere('trans_payment_schedule.tglLunas', '>', $tanggal);
                });
                })
            ->leftjoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai')
            ->leftJoin("tblsettingjf", function($join){
                $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
                $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            })
            ->leftjoin('tbltujuantransaksi', 'tbltujuantransaksi.idTujuanTransaksi', '=', 'trans_gadai.idTujuanTransaksi')
            ->leftJoin("trans_fundingdetail", function($join){
                $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
                $join->whereNull('trans_fundingdetail.tglUnpledging');
            })
            ->leftjoin('trans_funding', 'trans_funding.idFunding', '=', 'trans_fundingdetail.idFunding')
            ->leftjoin('tblpartner', 'tblpartner.idPartner', '=', 'trans_funding.idPartner')
            ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, cabang.kodeCabang, cabang.namaCabang, tblwilayah.kd_wilayah AS kodeWilayah, tblwilayah.namaWilayah, trans_gadai.noSbg, tblcustomer.cif AS noCif, tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, tblcustomer.jenisKelamin, tblcustomer.statusPerkawinan AS statusPernikahan, tblpekerjaan.namaPekerjaan AS pekerjaan, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, tblkelurahan.namaProvinsi, tblkelurahan.kodepos, IF(tblcustomer.alamatDomisili != '', REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')) AS alamatDomisili, IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, tblcustomer.hp AS noHp, tblsektorekonomi.namaSektorEkonomi, trans_payctrl.tanggalPencairan AS tanggalCair, trans_gadai.tglJatuhTempo, IF(tblproduk.idJenisProduk = 2, trans_payctrl.tanggalJtCicilan, trans_gadai.tglJatuhTempo) AS tglJatuhTempoCicilan, IF(tblproduk.idJenisProduk = 2, trans_payctrl.angsuranKe, 1) AS angsuranKe, IF(tblproduk.idJenisProduk = 2, DATEDIFF($tanggal, trans_payctrl.tanggalJtCicilan), DATEDIFF($tanggal, trans_gadai.tglJatuhTempo)) AS ovd,trans_gadai.lamaPinjaman AS tenor, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, trans_gadai.nilaiPinjaman AS pokokAwal, IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.pokokAngsuran), trans_gadai.nilaiPinjaman) AS saldoPokok, trans_payctrl.bungaFull AS saldoSimpan, IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.bungaAngsuran), trans_payctrl.bungaFull) AS saldoSimpanOutstanding, trans_gadai.biayaAdmin, tblpartner.namaPartner AS bankFunding, trans_gadaijf.idJf, tblsettingjf.namaPendana, trans_gadaijf.rateJf, trans_gadaijf.porsiJf, trans_payctrl.statusPinjaman, tran_taksiran.totalTaksiran, tran_taksiran.sumBeratBersih, tran_taksiran.avgKarat, tran_fapg.stleAntam AS stle, tbltujuantransaksi.namaTujuanTransaksi, trans_gadai.statusAplikasi, trans_gadai.idAsalJaminan")
            ->where('trans_payctrl.statusPinjaman', '!=', 'WO')
            ->where('trans_payctrl.tanggalPencairan', '<=', $tanggal)
            ->where('trans_payctrl.tanggalpelunasan', '>', $tanggal)
            ->orWhereNull('trans_payctrl.tanggalpelunasan')
            ->groupBy('trans_payctrl.idGadai')
            ->get()
            ->toArray();
        return $query;
    }

    protected function formatData($data)
    {
        $exportedData = '';

        // Check if there is any data
        if (empty($data)) {
            $this->info('No data found for export.');
            return $exportedData;
        }

        // Get the headers
        $headers = array_keys($data[0]);

        // Set the delimiter
        $delimiter = '|';

        // Add headers to the exported data
        $exportedData .= implode($delimiter, $headers) . PHP_EOL;

        foreach ($data as $row) {
            $rowArray = [];
            foreach ($row as $column) {
                // Convert objects and arrays to JSON
                $value = is_array($column) || is_object($column) ? json_encode($column) : $column;
                $rowArray[] = str_replace(["\r", "\n", '|', "\t"], ' ', $value);
            }
            $exportedData .= implode($delimiter, $rowArray) . PHP_EOL;
        }

        return $exportedData;
    }

    protected function logFileUrl($s3Url, $url, $tanggal)
    {
        $insertedId = DB::table('report_harian')->insert([
            'urlFile' => $url,
            'tanggal' => $tanggal,
            'jenisReport' => 'AZ'
        ]);

        if ($insertedId) {
            $this->info("File uploaded to S3. URL: {$url}. Log ID: {$insertedId}");
        } else {
            $this->error("Failed to insert log record for URL: {$url}");
        }
    }

}
