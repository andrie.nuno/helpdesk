<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\model\trans_payctrl;
use DB;

class JaminanKonvensionalCommandTxt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportjaminan:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export JaminanKonvensional data to txt, upload to S3, and log file URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $tanggal = date("Y-m-d", strtotime("yesterday"));
            
            $data = $this->getData($tanggal);

            $exportedData = $this->formatData($data);

            $filename = "export-jaminankonvensional/exported_data_jaminan_$tanggal.txt";
            Storage::put($filename, $exportedData);
            $contentType = 'application/json';

            $s3Url = Storage::disk('s3')->put($filename, $exportedData, 'public', ['Content-Type' => $contentType]);
            $url = "https://gadaimas.nos.wjv-1.neo.id/$filename";

            $this->logFileUrl($s3Url, $url, $tanggal);
        } catch (\Exception $e) {
            $this->error("An error occurred: {$e->getMessage()}");
        }
    }

    protected function getData($tanggal)
    {
        DB::select(DB::raw("SET @tanggalNext := ?"), [$tanggal]);

        DB::select(DB::raw("SET @row_number := 0"));

        $query = DB::select(DB::raw("
        SELECT tblproduk.kodeProduk, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, 
        tblcustomer.cif, tblcustomer.noKtp, tblcustomer.namaCustomer, trans_payctrl.tanggalPencairan, trans_gadai.lamaPinjaman, 
        tran_taksirandetail.ltv, tran_taksirandetail.hargaStle, trans_gadai.transKe, trans_gadai.tglJatuhTempo, trans_payctrl.ovd, 
        trans_payctrl.totalHutang, trans_gadai.rateFlat, trans_payctrl.pokokAwal, trans_payctrl.pokokSaldo, trans_gadai.biayaAdmin, 
        '' AS kodeMitra, '' AS namaMitra, 
        @row_number:=CASE
            WHEN @idTaksiran = tran_taksirandetail.idTaksiran 
                THEN @row_number + 1
            ELSE 1 
        END AS noJaminan, 
        @idTaksiran:=tran_taksirandetail.idTaksiran AS idTaksiran, 
        REPLACE(REPLACE(REPLACE(tran_taksirandetail.keterangan, CHAR(13), ' '), CHAR(10), ' '), '|', ' ') AS model, tran_taksirandetail.taksiran, 
        tran_taksirandetail.karat, tran_taksirandetail.beratBersih, tran_taksirandetail.jumlah, tblbarangemas.kodeBarangEmas, 
        tblbarangemas.namaBarangEmas FROM trans_payctrl 
        INNER JOIN trans_gadai ON trans_gadai.idGadai = trans_payctrl.idGadai 
        INNER JOIN tblproduk ON tblproduk.idProduk = trans_gadai.idProduk 
        INNER JOIN tblcabang ON tblcabang.idCabang = trans_gadai.idCabang 
        INNER JOIN tblwilayah ON tblwilayah.idWilayah = tblcabang.idWilayah 
        INNER JOIN tblcustomer ON tblcustomer.idCustomer = trans_gadai.idCustomer 
        INNER JOIN tran_taksiran ON tran_taksiran.idTaksiran = trans_gadai.idTaksiran 
        INNER JOIN tran_taksirandetail ON tran_taksirandetail.idTaksiran = trans_gadai.idTaksiran 
        INNER JOIN tblbarangemas ON tblbarangemas.idBarangEmas = tran_taksirandetail.idBarangEmas 
        WHERE trans_payctrl.statusPinjaman != 'WO' 
        AND trans_payctrl.tanggalPencairan <= @tanggalNext 
        AND (trans_payctrl.tanggalPelunasan > @tanggalNext OR trans_payctrl.tanggalPelunasan IS NULL) 
        ORDER BY tran_taksirandetail.idTaksiran, tran_taksirandetail.idTaksiranDetail;"));

        return $query;
        // dd($query);
    }

    protected function formatData($data)
{
    $exportedData = '';

    // Check if there is any data
    if (empty($data)) {
        $this->info('No data found for export.');
        return $exportedData;
    }

    // Get the headers
    $headers = array_keys(get_object_vars($data[0]));

    // Set the delimiter
    $delimiter = '|';

    // Add headers to the exported data
    $exportedData .= implode($delimiter, $headers) . PHP_EOL;

    foreach ($data as $row) {
        $rowArray = [];
        foreach ($row as $column) {
            // Convert objects and arrays to JSON
            $value = is_array($column) || is_object($column) ? json_encode($column) : $column;
            $rowArray[] = str_replace(["\r", "\n", '|', "\t"], ' ', $value);
        }
        $exportedData .= implode($delimiter, $rowArray) . PHP_EOL;
    }

    return $exportedData;
}


    protected function logFileUrl($s3Url, $url, $tanggal)
    {
        $insertedId = DB::table('report_harian')->insert([
            'urlFile' => $url,
            'tanggal' => $tanggal,
            'jenisReport' => 'Jaminan'
        ]);

        if ($insertedId) {
            $this->info("File uploaded to S3. URL: {$url}. Log ID: {$insertedId}");
        } else {
            $this->error("Failed to insert log record for URL: {$url}");
        }
    }

}
