<?php

namespace App\Services;

use GuzzleHttp\Client;

class SendGridMail
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function send($to, $subject, $htmlContent)
    {
        $client = new Client();

        try {
            $response = $client->post("https://api.sendgrid.com/v3/mail/send", [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->apiKey,
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'personalizations' => [
                        [
                            'to' => [
                                [
                                    'email' => $to,
                                ],
                            ],
                            'subject' => $subject,
                        ],
                    ],
                    'from' => [
                        'name' => "Danain",
                        'email' => "notifikasi@danain.co.id",
                    ],
                    'content' => [
                        [
                            'type' => 'text/html', // Set the content type to HTML
                            'value' => $htmlContent, // Set the HTML content
                        ],
                    ],
                ],
            ]);

            $statusCode = $response->getStatusCode();
            $header = json_encode($response->getHeaders());
            $body = $response->getBody()->getContents();
            // $responseData = json_decode($body, true);

        // Log the entire response for debugging
        \Log::info('SendGrid API Response: ' . $statusCode . ' - ' . $header . ' - ' . $body);

            return $body;
        } catch (\Exception $e) {
            \Log::error('Error sending email: ' . $e->getMessage());
            return false;
        }
    }
}
