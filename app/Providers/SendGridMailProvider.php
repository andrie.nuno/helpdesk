<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;

class SendGridMailProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Services\SendGridMail', function () {
            return new \App\Services\SendGridMail(config('services.sendgrid.api_key'));
        });
    }
}
